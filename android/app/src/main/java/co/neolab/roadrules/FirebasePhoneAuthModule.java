package co.neolab.roadrules;

import android.net.Uri;
import android.util.Log;

import android.support.annotation.NonNull;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirebasePhoneAuthModule extends ReactContextBaseJavaModule {
    private static final String TAG = "FirebasePhoneAuthModule";
    public static final String ON_VERIFICATION_COMPLETED = "onFirebasePhoneVerificationCompleted";
    public static final String ON_VERIFICATION_FAILED = "onFirebasePhoneVerificationFailed";
    public static final String ON_CODE_SENT = "onFirebasePhoneCodeSent";
    public static final String ON_CODE_AUTO_RETRIEVAL_TIMEOUT = "onFirebasePhoneCodeAutoRetrievalTimeOut";

    public FirebasePhoneAuthModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "FirebasePhoneAuth";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("ON_VERIFICATION_COMPLETED", ON_VERIFICATION_COMPLETED);
        constants.put("ON_VERIFICATION_FAILED", ON_VERIFICATION_FAILED);
        constants.put("ON_CODE_SENT", ON_CODE_SENT);
        constants.put("ON_CODE_AUTO_RETRIEVAL_TIMEOUT", ON_CODE_AUTO_RETRIEVAL_TIMEOUT);
        return constants;
    }

    private void sendEvent(String eventName, Object params) {
        getReactApplicationContext()
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void verifyPhoneNumber(final String userInput, final Promise promise) {
        FirebaseApp firebaseApp = FirebaseApp.getInstance();
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance(firebaseApp);

        PhoneAuthProvider.getInstance(firebaseAuth).verifyPhoneNumber(userInput, 60, TimeUnit.SECONDS,
                getCurrentActivity(), new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential credential) {
                        sendEvent(ON_VERIFICATION_COMPLETED, credential.getSmsCode());
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        sendEvent(ON_VERIFICATION_FAILED, e.getMessage());
                        promiseRejectAuthException(promise, e);
                    }

                    @Override
                    public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
                        sendEvent(ON_CODE_SENT, verificationId);
                        promise.resolve(verificationId);
                    }

                    @Override
                    public void onCodeAutoRetrievalTimeOut(String verificationId) {
                        super.onCodeAutoRetrievalTimeOut(verificationId);
                        sendEvent(ON_CODE_AUTO_RETRIEVAL_TIMEOUT, verificationId);
                    }
                });
    }

    @ReactMethod
    public void signInWithCredential(final String userInput, final String verificationID, final Promise promise) {
        Log.d(TAG, "signInWithCredential");
        FirebaseApp firebaseApp = FirebaseApp.getInstance();
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance(firebaseApp);
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationID, userInput);


        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            promiseWithUser(user, promise);
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                promiseRejectAuthException(promise, task.getException());
                            } else {
                                promise.reject("firebase_auth_phone_verification_failed", "Firebase authentication error");
                            }
                        }
                    }
                });
    }

  /* ------------------
   * INTERNAL HELPERS
   * ---------------- */

    /**
     * Resolves or rejects an auth method promise without a user (user was missing)
     *
     * @param promise
     * @param isError
     */
    private void promiseNoUser(Promise promise, Boolean isError) {
        if (isError) {
            promise.reject("auth/no-current-user", "No user currently signed in.");
        } else {
            promise.resolve(null);
        }
    }

    /**
     * promiseWithUser
     *
     * @param user
     * @param promise
     */
    private void promiseWithUser(final FirebaseUser user, final Promise promise) {
        if (user != null) {
            WritableMap userMap = firebaseUserToMap(user);
            promise.resolve(userMap);
        } else {
            promiseNoUser(promise, true);
        }
    }

    /**
     * promiseRejectAuthException
     *
     * @param promise
     * @param exception
     */
    private void promiseRejectAuthException(Promise promise, Exception exception) {
        String code = "UNKNOWN";
        String message = exception.getMessage();
        String invalidPhoneNumber = "The phone number you have given is badly formatted.";
        String invalidEmail = "The email address is badly formatted.";

        Log.d(TAG, "PROMISE REJECT WITH AUTH exception");
        Log.d(TAG, Log.getStackTraceString(exception));
        try {
            FirebaseAuthException authException = (FirebaseAuthException) exception;
            code = authException.getErrorCode();
            message = authException.getMessage();
            Log.d("ROADRULES_ERROR", authException.getErrorCode());
            Log.d("ROADRULES_ERROR", authException.getMessage());
        } catch (Exception e) {
            Matcher matcher = Pattern.compile("\\[(.*):.*\\]").matcher(message);
            if (matcher.find()) {
                code = matcher.group(1).trim();
            }
        }


        switch (code) {
            case "INVALID_CUSTOM_TOKEN":
                message = "The custom token format is incorrect. Please check the documentation.";
                break;
            case "CUSTOM_TOKEN_MISMATCH":
                message = "The custom token corresponds to a different audience.";
                break;
            case "INVALID_CREDENTIAL":
                message = "The supplied auth credential is malformed or has expired.";
                break;
            case "INVALID_EMAIL":
                message = invalidEmail;
                break;
            case "WRONG_PASSWORD":
                message = "The password is invalid or the user does not have a password.";
                break;
            case "USER_MISMATCH":
                message = "The supplied credentials do not correspond to the previously signed in user.";
                break;
            case "REQUIRES_RECENT_LOGIN":
                message = "This operation is sensitive and requires recent authentication. Log in again before retrying this request.";
                break;
            case "ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL":
                message = "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.";
                break;
            case "EMAIL_ALREADY_IN_USE":
                message = "The email address is already in use by another account.";
                break;
            case "CREDENTIAL_ALREADY_IN_USE":
                message = "This credential is already associated with a different user account.";
                break;
            case "USER_DISABLED":
                message = "The user account has been disabled by an administrator.";
                break;
            case "USER_TOKEN_EXPIRED":
                message = "The user\'s credential is no longer valid. The user must sign in again.";
                break;
            case "USER_NOT_FOUND":
                message = "There is no user record corresponding to this identifier. The user may have been deleted.";
                break;
            case "INVALID_USER_TOKEN":
                message = "The user\'s credential is no longer valid. The user must sign in again.";
                break;
            case "WEAK_PASSWORD":
                message = "The given password is invalid.";
                break;
            case "OPERATION_NOT_ALLOWED":
                message = "This operation is not allowed. You must enable this service in the console.";
                break;




            // phone auth codes
            case "MISSING_PHONE_NUMBER":
                code = "auth/missing-phone-number";
                message = "Please provide a phone number.";
            case "INVALID_PHONE_NUMBER":
                code = "auth/invalid-phone-number";
                message = "Please enter a valid phone number in the correct international format, e.g. +263718384668";
                break;
            case "MISSING_VERIFICATION_CODE":
                code = "auth/missing-verification-code";
                message = "Please enter the verification code that was sent to you via SMS.";
                break;
            case "INVALID_VERIFICATION_CODE":
                code = "auth/invalid-verification-code";
                message = "You have entered the wrong verification code, please re-check the SMS sent to you and enter the correct code & press Verify. If the problem continues get a new code by pressing Resend Code.";
                break;
            case "MISSING_VERIFICATION_ID":
                code = "auth/missing-verification-id";
                message = "Please provide a verification ID.";
                break;
            case "INVALID_VERIFICATION_ID":
                code = "auth/invalid-verification-id";
                message = "Please provide a valid verification ID.";
                break;
            case  "MISSING_APP_CREDENTIAL":
                code = "auth/missing-app-credential";
                message = "Please provide an app credential.";
                break;
            case  "INVALID_APP_CREDENTIAL":
                code = "auth/invalid-app-credential";
                message = "Please provide a valid app credential.";
                break;
            case  "SESSION_EXPIRED":
                code = "auth/session-expired";
                message = "SMS code has expired. Please click Resend Code to get a new verification code.";
                break;
            case  "QUOTA_EXCEEDED":
                code = "auth/quota-exceeded";
                message = "SMS quota has been exceeded, please contact admin.";
                break;
            default:
                break;
        }

        if (code.equals("UNKNOWN") && exception instanceof FirebaseAuthInvalidCredentialsException) {
            code = "INVALID_PHONE_NUMBER";
            message = invalidPhoneNumber;
        }

        code = "auth/" + code.toLowerCase().replace("error_", "").replace('_', '-');

        promise.reject(code, message, exception);
    }


    /**
     * Converts a List of UserInfo instances into the correct format to match the web sdk
     *
     * @param providerData List<UserInfo> user.getProviderData()
     * @return WritableArray array
     */
    private WritableArray convertProviderData(List<? extends UserInfo> providerData) {
        WritableArray output = Arguments.createArray();
        for (UserInfo userInfo : providerData) {
            WritableMap userInfoMap = Arguments.createMap();
            userInfoMap.putString("providerId", userInfo.getProviderId());
            userInfoMap.putString("uid", userInfo.getUid());
            userInfoMap.putString("displayName", userInfo.getDisplayName());

            final Uri photoUrl = userInfo.getPhotoUrl();

            if (photoUrl != null) {
                userInfoMap.putString("photoURL", photoUrl.toString());
            } else {
                userInfoMap.putNull("photoURL");
            }

            userInfoMap.putString("email", userInfo.getEmail());

            output.pushMap(userInfoMap);
        }

        return output;
    }

    /**
     * firebaseUserToMap
     *
     * @param user
     * @return
     */
    private WritableMap firebaseUserToMap(FirebaseUser user) {
        WritableMap userMap = Arguments.createMap();

        final String email = user.getEmail();
        final String uid = user.getUid();
        final String provider = user.getProviderId();
        final String name = user.getDisplayName();
        final Boolean verified = user.isEmailVerified();
        final Uri photoUrl = user.getPhotoUrl();

        userMap.putString("uid", uid);
        userMap.putString("providerId", provider);
        userMap.putBoolean("emailVerified", verified);
        userMap.putBoolean("isAnonymous", user.isAnonymous());

        if (email != null) {
            userMap.putString("email", email);
        } else {
            userMap.putNull("email");
        }

        if (name != null) {
            userMap.putString("displayName", name);
        } else {
            userMap.putNull("displayName");
        }

        if (photoUrl != null) {
            userMap.putString("photoURL", photoUrl.toString());
        } else {
            userMap.putNull("photoURL");
        }

        userMap.putArray("providerData", convertProviderData(user.getProviderData()));

        return userMap;
    }
}
