import { takeLatest } from 'redux-saga/effects'
import { actionTypes as ReactReduxFirebaseTypes } from 'react-redux-firebase'
import Firebase from '../Services/Firebase'
import * as Facebook from '../Services/Facebook'
import PushNotifications from '../Services/PushNotifications'
import Search from '../Services/Search'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { FacebookTypes } from '../Redux/FacebookRedux'
import { ProfileTypes } from '../Redux/ProfileRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { appInvite } from './FacebookSagas'
import { login, logout, verify, signInAnonymously, setProfile, updateProfile, updateProfilePicture, updateToken, refreshToken, getToken, onMessage, logoutRedirect } from './ProfileSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
// const api = DebugConfig.useFixtures ? FixtureAPI : API.create()
const firebase = Firebase.create()
const pushNotifications = PushNotifications.create()
const search = Search.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield [
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(ProfileTypes.LOGIN_REQUEST_ACTION, login, firebase),
    takeLatest(ProfileTypes.LOGOUT_REQUEST_ACTION, logout, firebase),
    takeLatest(ProfileTypes.VERIFY_REQUEST_ACTION, verify, firebase),
    takeLatest(ProfileTypes.SIGN_IN_ANONYMOUSLY_ACTION, signInAnonymously, firebase),
    takeLatest(ProfileTypes.UPDATE_PROFILE_REQUEST_ACTION, updateProfile, firebase),
    takeLatest(ProfileTypes.UPDATE_PROFILE_PICTURE_ACTION, updateProfilePicture, firebase),
    takeLatest(ProfileTypes.UPDATE_TOKEN_ACTION, updateToken, firebase),
    takeLatest(ReactReduxFirebaseTypes.SET_PROFILE, setProfile, firebase),
    takeLatest(ProfileTypes.REFRESH_PROFILE_ACTION, refreshToken, firebase),
    takeLatest(ProfileTypes.REFRESH_PROFILE_ACTION, getToken, firebase),
    takeLatest(ProfileTypes.REFRESH_PROFILE_ACTION, onMessage, firebase),
  ]
}
