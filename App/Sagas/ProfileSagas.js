import { put, select, call, take } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import { populate } from 'react-redux-firebase'
import Actions from '../Redux/ProfileRedux'
import ReactReduxFirebaseConfig from '../Config/ReactReduxFirebaseConfig'

export const selectAuth = (state) => state.firebase.auth

export const selectProfile = (state) => state.profile

export const selectFirebaseProfile = ({ firebase }) => {
  const profile = populate(firebase, 'profile', ReactReduxFirebaseConfig.profileParamsToPopulate)
  try {
    if (typeof profile.profile_pic === 'string') {
      profile.profile_pic = firebase.data.images.profile_pics[profile.profile_pic]
    }
    return profile
  } catch (e) {
    return profile
  }
}

export const getFormData = (fields, form) => {
  const data = {}
  const errors = fields.reduce((accumulator, field) => {
    const {key, title, validate} = field
    const value = form[key] && typeof form[key] === 'object' ? form[key].value : ''
    if (field.required && !value) {
      return {...accumulator, [key]: `${title} field is required`}
    }
    if (validate) {
      const error = validate(value)
      if (error) {
        return {...accumulator, [key]: error}
      }
    }
    data[key] = value
    return accumulator
  }, {})
  return {data, errors}
}

export function * login (firebase, {fields, form}) {
  const {data, errors} = getFormData(fields, form)
  if (Object.keys(errors).length) {
    return yield put(Actions.fieldErrorsAction(fields, errors))
  }

  try {
    const verificationId = yield call(firebase.signInWithPhoneNumber, data.phoneNumber)
    return yield put(Actions.loginSuccessAction(verificationId))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.loginFailureAction(e.message))
  }
}

export function * signInAnonymously (firebase) {
  try {
    const user = yield call(firebase.signInAnonymously)
    alert("sign in anonymously success " + JSON.stringify(user))
    return yield put(Actions.verifySuccessAction(user))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
  alert("sign in anonymously fail " + e.message)
    return yield put(Actions.verifyFailureAction(e.message))
  }
}

export function * verify (firebase, {fields, form}) {
  const {data, errors} = getFormData(fields, form)
  const { verificationId } = yield select(selectProfile)
  if (!verificationId) {
    return yield put(Actions.verifyFailureAction('Verification code not found, please click Resend Verification code'))
  }
  if (Object.keys(errors).length) {
    return yield put(Actions.fieldErrorsAction(fields, errors))
  }

  try {
    const user = yield call(firebase.verify, data.verificationCode, verificationId)
    return yield put(Actions.verifySuccessAction(user))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.verifyFailureAction(e.message))
  }
}

export function * setProfile (action) {
  const profile = yield select(selectFirebaseProfile)
  yield put(Actions.setProfileAction(profile))
}

export function * updateProfile (firebase, {fields, form}) {
  const {data, errors} = getFormData(fields, form)
  const { uid: id } = yield select(selectAuth)
  data.id = id

  if (Object.keys(errors).length) {
    return yield put(Actions.fieldErrorsAction(fields, errors))
  }
  try {
    const response = yield call(firebase.updateProfile, id, data)
    return yield put(Actions.updateProfileSuccessAction(data, response))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.updateProfileFailureAction(e.message))
  }
}

export function * updateToken (firebase, { token }) {
  const { data: { id: uid } } = yield select(selectProfile)

  if (!uid) {
    return
  }

  try {
    const response = yield call(firebase.updateToken, uid, token)
    return yield put(Actions.updateTokenSuccessAction(token, response))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.updateTokenFailAction(token, e.message))
  }
}

export function * updateProfilePicture (firebase, { image }) {
  const { uid } = yield select(selectAuth)
  console.log("THIS IS WHERE WE ARE HERE  to check ID:", uid)

  if (!uid) {
    return
  }
  console.log("THIS IS WHERE WE ARE HERE  ID: ", uid)

  try {
    const channel = yield call(progressSagaHelper, firebase.updateProfilePicture, uid, image)
    try {
      // take(END) will cause the saga to terminate by jumping to the finally block
      while (true) {
        // Remember, our helper only emits actions
        // Thus we can directly "put" them
        const action = yield take(channel)
        yield put(action)
      }
    } catch (error) {
    } finally {
      // Optional
    }
  } catch (e) {
    return yield put(Actions.updateProfilePictureFailureAction(e.message))
  }
}

export function * logout (firebase, action) {
  try {
    const result = yield call(firebase.logout)
    return yield put(Actions.logoutSuccessAction(result))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.logoutFailureAction(e.message))
  }
}

export function * getToken (firebase, { id }) {
  try {
    const token = yield call(firebase.getToken)
    return yield put(Actions.updateTokenAction(token))
  } catch (e) {
    // const { code, framesToPop, nativeStackIOS, nativeStackAndroid, domain, userInfo } = e
    // const { error_name, NSLocalizedDescription } = userInfo
    return yield put(Actions.getTokenFailAction(e.message))
  }
}

export function * refreshToken (firebase, { id: uid }) {
  if (!uid) {
    return
  }

  try {
    const channel = yield call(progressSagaHelper, firebase.setOnTokenRefresh)
    try {
      // take(END) will cause the saga to terminate by jumping to the finally block
      while (true) {
        // Remember, our helper only emits actions
        // Thus we can directly "put" them
        const action = yield take(channel)
        // yield put(action)
      }
    } catch (error) {
    } finally {
      // Optional
    }
  } catch (e) {
    // return yield put(Actions.updateProfilePictureFailureAction(e.message))
  }
}

export function * onMessage (firebase, { id: uid }) {
  if (!uid) {
    return
  }

  try {
    const channel = yield call(progressSagaHelper, firebase.setOnMessage)
    try {
      // take(END) will cause the saga to terminate by jumping to the finally block
      while (true) {
        // Remember, our helper only emits actions
        // Thus we can directly "put" them
        const message = yield take(channel)
        yield put(Actions.onMessageAction(message))
      }
    } catch (error) {
    } finally {
      // Optional
    }
  } catch (e) {
    // return yield put(Actions.updateProfilePictureFailureAction(e.message))
  }
}

function progressSagaHelper (func, ...args) {
  // An event channel will let you send an infinite number of events
  // It provides you with an emitter to send these events
  // These events can then be picked up by a saga through a "take" method
  return eventChannel(emitter => {
    const task = func(emitter, ...args)

    // The returned method can be called to cancel the channel
    return () => {
      task.cancel()
    }
  })
}
