import { StackNavigator } from 'react-navigation'
import DrivingSchoolScreen from '../Containers/DrivingSchoolScreen'
import NotesScreen from '../Containers/NotesScreen'
import Progress from '../Containers/Progress'
import DetailedRevisionScreen from '../Containers/DetailedRevisionScreen'
import QuickRevisionScreen from '../Containers/QuickRevisionScreen'
import ResultsScreen from '../Containers/ResultsScreen'
import QuizScreen from '../Containers/QuizScreen'
import DrawerContainer from '../Containers/DrawerContainer'
import HomeScreen from '../Containers/HomeScreen'
import DrivingSchoolsScreen from '../Containers/DrivingSchoolsScreen'
import LearnScreen from '../Containers/LearnScreen'
import DonutScreen from '../Containers/DonutScreen'
import ProfileScreen from '../Containers/ProfileScreen'
import AuthScreen from '../Containers/AuthScreen'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  DrivingSchoolScreen: { screen: DrivingSchoolScreen },
  NotesScreen: { screen: NotesScreen },
  Progress: { screen: Progress },
  DetailedRevisionScreen: { screen: DetailedRevisionScreen },
  QuickRevisionScreen: { screen: QuickRevisionScreen },
  ResultsScreen: { screen: ResultsScreen },
  QuizScreen: { screen: QuizScreen },
  DrawerContainer: { screen: DrawerContainer },
  HomeScreen: { screen: HomeScreen },
  DrivingSchoolsScreen: { screen: DrivingSchoolsScreen },
  LearnScreen: { screen: LearnScreen },
  DonutScreen: { screen: DonutScreen },
  ProfileScreen: { screen: ProfileScreen },
  AuthScreen: { screen: AuthScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
