export const fields = [
  {
    title: 'location',
    key: 'location',
    type: 'select',
    options: [
      {
        value: 'bulawayo',
        label: 'Bulawayo',

      },
      {
        value: 'harare',
        label: 'Harare',

      },
      {
        value: 'manicaland',
        label: 'Manicaland',

      },
      {
        value: 'mash_central',
        label: 'Mash Central',

      },
      {
        value: 'mash_east',
        label: 'Mash East',

      },
      {
        value: 'mash_west',
        label: 'Mash West',

      },
      {
        value: 'masvingo',
        label: 'Masvingo',

      },
      {
        value: 'mat_north',
        label: 'Mat North',

      },
      {
        value: 'mat_south',
        label: 'Mat South',

      },
      {
        value: 'midlands',
        label: 'Midlands',

      },
    ],
    tableEditable: true,
    formEditable: true,
    required: true
  },
  {
    title: 'Name',
    key: 'name',
    type: 'text',
    tableEditable: true,
    formEditable: true,
    required: true
  }
]
