// leave off @2x/@3x
const images = {
  icon: require('../Images/icon.png'),
  logo: require('../Images/logo.png'),
  splash: require('../Images/splash.png'),
  backgroundHarare: require('../Images/harare.jpg'),
  backgroundTrafficFines: require('../Images/traffic_fines.jpg'),
  backgroundEmergencyServices: require('../Images/emergency_services.jpg'),
  backgroundDrivingSchools: require('../Images/driving_schools.jpg'),
  backgroundChat: require('../Images/chat.jpg'),
  whatsapp: require('../Images/whatsapp.png'),
  facebook: require('../Images/facebook.png'),
  call: require('../Images/call.png'),
  stop: require('../Images/splash_stop.png'),
  // profile: require('../Images/1234')
}

export default images
