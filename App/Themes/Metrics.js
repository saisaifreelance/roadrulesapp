import {Dimensions, Platform} from 'react-native'

const { width, height } = Dimensions.get('window')
const maxDonutWidth = 500
const maxModalWidth = 550
const maxAllowableDonutWidth = width - 32

// Used via Metrics.baseMargin
const metrics = {
  marginHorizontal: 10,
  marginVertical: 10,
  section: 25,
  baseMargin: 10,
  doubleBaseMargin: 20,
  smallMargin: 5,
  doubleSection: 50,
  horizontalLineHeight: 1,
  searchBarHeight: 30,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  donutWidth: maxDonutWidth < maxAllowableDonutWidth ? maxDonutWidth : maxAllowableDonutWidth,
  modalWidth: maxModalWidth < maxAllowableDonutWidth ? maxModalWidth : maxAllowableDonutWidth,
  drawerLogoHeight: 200,
  buttonRadius: 4,
  parallaxHeaderHeight: 220,
  toolbarPaddingTop: (Platform.OS === 'ios') ? 20 : 0,
  navBarHeight: (Platform.OS === 'ios') ? 44 : 54,
  toolbarHeight: (Platform.OS === 'ios') ? 64 : 54,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 200
  }
}

export default metrics
