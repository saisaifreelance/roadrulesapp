import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    mainContainer: {
      flex: 1,
      backgroundColor: Colors.transparent
    },
    container: {
      flex: 1,
      backgroundColor: Colors.transparent
    },
    containerCentered: {
      flex: 1,
      backgroundColor: Colors.transparent,
      alignItems: 'center',
      justifyContent: 'center'
    },
    backgroundImage: {
      position: 'absolute',
      top: 0,
      // left: 0,
      bottom: 0,
      // right: 0
    },
    backgroundOverlay: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.snow,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.snow,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text
    }
  },
  searchModal: {
    container: {
      flex: 1,
      backgroundColor: Colors.amber,
    },
    toolbarTextContainer: {
      marginRight: Metrics.doubleBaseMargin
    },
    toolbarText: {
      ...Fonts.style.normal,
      fontFamily: Fonts.type.bold,
      color: Colors.snow,
      fontWeight: 'bold',
    },
    titleContainer: {
      paddingBottom: Metrics.navBarHeight,
      paddingHorizontal: Metrics.doubleBaseMargin,
      borderBottomColor: Colors.snow,
      borderBottomWidth: 1,
    },
    title: {
      ...Fonts.style.h4,
      color: Colors.snow,
    },
  },
  form: {
    form: {
      marginVertical: Metrics.baseMargin
    },
    formError: {
      ...Fonts.style.h6,
      textAlign: 'center',
      // margin: Metrics.baseMargin,
      color: Colors.fire,
    },
    formLegend: {
      ...Fonts.style.h5,
      textAlign: 'center',
      margin: Metrics.baseMargin
    },
    formField: {
      borderColor: Colors.steel,
      borderWidth: 1,
      borderRadius: Metrics.smallMargin,
      padding: Metrics.baseMargin,
      marginBottom: Metrics.baseMargin,
    },
    textInput: {
      height: 32,
      ...Fonts.style.h4,
    },
    errorText: {
      color: Colors.fire,
      marginTop: Metrics.smallMargin
    }
  },
  logo: {
    logoContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      // backgroundColor: Colors.transparent,
      flex: 1,
    },
    logoCenter: {
      flex: 1,
      marginHorizontal: 16,
      backgroundColor: 'transparent'
    },
    logoFlank: {
      height: 50,
      width: 20,
      backgroundColor: 'black'
    },
  },
  modal: {
    modalContainer: {
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      alignItems: 'center',
      justifyContent: 'center'
    },
    container: {
      width: Metrics.modalWidth,
      borderRadius: Metrics.smallMargin,
      backgroundColor: Colors.snow,
    },
    section: {
      marginHorizontal: Metrics.baseMargin,
      marginVertical: Metrics.smallMargin,
    },
    title: {
      ...Fonts.style.h5,
    },
    titleError: {
      ...Fonts.style.h5,
      color: Colors.fire
    },
    modalControlsContainer: {
      margin: Metrics.smallMargin,
      flexDirection: 'row',
      justifyContent: 'flex-end'
    },
    optionContainer: {
      padding: Metrics.smallMargin,
    },
    option: {
      ...Fonts.style.h6,
    },
    description: {
      ...Fonts.style.normal,
    },
  },
  button: {
    button: {
      borderRadius: Metrics.smallMargin,
      padding: Metrics.baseMargin,
      backgroundColor: Colors.amber,
      alignItems: 'center',
      flexDirection: 'row',
      marginVertical: 8,
    },
    buttonText: {
      ...Fonts.style.h4,
      textAlign: 'center',
      color: Colors.snow,
      flex: 1
    },
    buttonTextIcon: {
      ...Fonts.style.h4,
      textAlign: 'left',
      color: Colors.snow,
      flex: 1
    },
    buttonIcon: {
      height: Metrics.icons.medium,
      width: Metrics.icons.medium,
      marginRight: Metrics.baseMargin
    }
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.snow
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  }
}

export default ApplicationStyles
