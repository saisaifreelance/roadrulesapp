import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image } from 'react-native'
// import { CachedImage } from 'react-native-img-cache'
import styles from './Styles/ProfilePictureEditStyle'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconButton from './IconButton'
import { Colors, Metrics, Fonts } from '../Themes'
import ImageType from '../Types/Image'


const CachedImage = Image

export default class ProfilePictureEdit extends Component {
  // Prop type warnings
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    image: PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      path: PropTypes.string,
      downloadUrl: PropTypes.string
    }),
    profile_pic: ImageType
  }

  // Defaults for props
  static defaultProps = {
    title: false
  }

  constructor (props) {
    super(props)
    this.onLayout = this.onLayout.bind(this)
    this.state = {
      height: null,
      width: null
    }
  }

  onLayout ({ nativeEvent: { layout: { width, height, x, y } } }) {
    this.setState({ width, height })
  }

  render () {
    let image = null
    let title = null
    const {height} = this.state
    let size = 200
    const style = {
      image: {}
    }
    let displacementY = size - (Fonts.size.h2 * 3 / 2)
    let displacementX = displacementY
    if (height && height < 200) {
      size = height
      style.image = {width: size, height: size, borderRadius: size / 2}
      style.icon = {top: 0, left: 0}
    } else if (height && height > 200) {
      displacementY = (((height - size) / 2) + size) - (Fonts.size.h2 * 3 / 2)
    }
    if (false && height && height > 250) {
      title = (<Text style={styles.title}>Profile Picture</Text>)
    }
    style.icon = {
      top: displacementY,
      left: displacementX
    }
    if (this.props.profile_pic && !this.props.image) {
      image = (
        <View style={styles.imageOuter}>
          <CachedImage source={{uri: this.props.profile_pic.downloadUrl}} style={[styles.image, style.image]} />
        </View>
      )
    } else if (this.props.image) {
      image = (
        <View style={styles.imageOuter}>
          <Image source={{uri: this.props.image.path}} style={[styles.image, style.image]} />
        </View>
      )
    } else {
      image = (
        <View style={styles.imageOuter}>
          <Icon name='account-circle' size={size} color={Colors.steel} />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={[styles.imageContainer]} onLayout={this.onLayout}>
          {image}
          <IconButton
            onPress={this.props.onPress}
            style={[styles.icon, style.icon]}
            size={Fonts.size.h2}
            backgroundColor={Colors.amber}
            color={Colors.charcoal}
            icon='camera'
          />
        </View>
        {title}
      </View>
    )
  }
}
