import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import IconButton from './IconButton'
import styles from './Styles/ModalResolveStyle'

export default class ModalResolve extends Component {
  // // Prop type warnings
  static propTypes = {
    actions: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    close: PropTypes.func
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.onSelect = this.onSelect.bind(this)
    this.close = this.close.bind(this)
  }

  onSelect (index) {
    this.props.actions[index].onPress()
  }

  close () {
    if (this.props.close) {
      return this.props.close()
    }
    this.props.navigation.goBack()
  }

  render () {
    const actions = this.props.actions.map((action, index) => {
      return (
        <TouchableOpacity onPress={this.onSelect.bind(this, index)} key={index}>
          <View style={styles.optionContainer}>
            <Text style={styles.option}>{action.title}</Text>
          </View>
        </TouchableOpacity>
      )
    })
    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            <View style={styles.container}>
              <View style={styles.modalControlsContainer}>
                <IconButton icon='close' onPress={this.close}/>
              </View>
              <View style={styles.section}>
                <Text style={styles.title}>{this.props.title}</Text>
              </View>
              <View style={styles.section}>
                {actions}
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}
