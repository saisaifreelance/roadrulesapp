import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { StackNavigator } from 'react-navigation'
import IconButton from './IconButton'
import ModalConnect from './ModalConnect'
import ModalPay from './ModalPay'
import ModalNotification from './ModalNotification'
import ModalRate from './ModalRate'
import ModalResolve from './ModalResolve'
import { Colors } from '../Themes'
import styles from './Styles/ModalMenuStyle'

export class ModalMenu extends Component {
  // // Prop type warnings
  static propTypes = {
    actions: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      onPress: PropTypes.func.isRequired,
    })),
    title: PropTypes.string,
    close: PropTypes.func
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.onSelect = this.onSelect.bind(this)
  }

  onSelect (index) {
    this.props.actions[index].onPress()
  }

  render () {
    const actions = Array.isArray(this.props.actions) && this.props.actions.map((action, index) => {
      return (
        <TouchableOpacity onPress={this.onSelect.bind(this, index)} key={index}>
          <View style={styles.optionContainer}>
            <Text style={styles.option}>{action.title}</Text>
          </View>
        </TouchableOpacity>
      )
    })
    return (
      <TouchableWithoutFeedback onPress={this.props.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            <View style={styles.container}>
              <View style={styles.modalControlsContainer}>
                <IconButton
                  icon='close'
                  onPress={this.props.close}
                />
              </View>
              <View style={styles.section}>
                <Text style={styles.title}>{this.props.title}</Text>
              </View>
              <View style={styles.section}>
                {actions}
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export default StackNavigator({
  ModalMenu: {
    screen: (props) => (<ModalMenu {...props} {...props.navigation.state.params} />)
  },
  ModalConnect: {
    screen: (props) => (<ModalConnect {...props} {...props.navigation.state.params} />)
  },
  ModalPay: {
    screen: (props) => (<ModalPay {...props} {...props.navigation.state.params} />)
  },
  ModalNotification: {
    screen: (props) => (<ModalNotification {...props} {...props.navigation.state.params} />)
  },
  ModalRate: {
    screen: (props) => (<ModalRate {...props} {...props.navigation.state.params} />)
  },
  ModalResolve: {
    screen: (props) => (<ModalResolve {...props} {...props.navigation.state.params} />)
  }
}, {
  cardStyle: {
    opacity: 1,
    backgroundColor: 'black'
  },
  initialRouteName: 'ModalMenu',
  headerMode: 'none',
  // Keeping this here for future when we can make
  navigationOptions: {
    header: {
      left: (
        <IconButton
          icon='close'
          onPress={() => window.alert('pop')}
        />
      ),
      style: {
        backgroundColor: 'green'
      }
    }
  }
})
