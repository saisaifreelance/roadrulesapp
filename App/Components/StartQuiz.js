import React, { Component, PropTypes } from 'react'
import {
  View,
  TextInput,
  TouchableOpacity,
  Text
} from 'react-native'
import Button from './Button'
import { Colors } from '../Themes'
import Mascot from '../Components/Mascot'
import styles from './Styles/StartQuizStyles'

class StartQuiz extends Component {
  static defaultProps = {
    max: 25,
    min: 3,
    maxMessage: 'The maximum number of questions is [25]',
  }
  constructor (props) {
    super(props)
    this.state = {
      questions: props.max
    }
  }

  render () {
    const increment = this.props.practice ? (
      <View style={{
        marginVertical: 8,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
      }}>

        <TouchableOpacity
          onPress={() => {
            if (parseInt(this.state.questions) > this.props.min) {
              this.setState({questions: parseInt(this.state.questions) - 1})
            }
          }}
          style={{
            marginHorizontal: 8,
            backgroundColor: Colors.amber,
            height: 56,
            width: 56,
            borderRadius: 28,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <Text style={{fontSize: 24, color: 'white'}}>-</Text>
        </TouchableOpacity>

        <TextInput
          style={{fontSize: 18, width: 80, height: 56, textAlign: 'center'}}
          value={this.state.questions + ''}
          onChangeText={
            (questions) => {
              if (questions && isNaN(parseInt(questions))) {
                return this.setState({questions: this.props.max})
              }
              if (questions && parseInt(questions) > this.props.max) {
                alert(this.props.maxMessage)
                return
              }
              this.setState({questions: parseInt(questions)})
            }
          }
        />
        <TouchableOpacity
          onPress={() => {
            if (parseInt(this.state.questions) < this.props.max) {
              this.setState({questions: parseInt(this.state.questions) + 1})
            } else {
              alert(this.props.maxMessage)
            }
          }}
          style={{
            marginHorizontal: 8,
            backgroundColor: Colors.amber,
            height: 56,
            width: 56,
            borderRadius: 28,
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Text style={{fontSize: 24, color: 'white'}}>+</Text>
        </TouchableOpacity>
      </View>

    ) : null

    return (
      <View style={styles.container}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            marginVertical: 8,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row'
          }}>
            <Mascot />
          </View>
          <Text style={styles.instruction}>{this.props.instruction}</Text>
          {increment}
        </View>

        <View style={styles.buttons}>
          <Button
            style={styles.button}
            onPress={() => this.props.startQuiz(this.state.questions)}>{this.props.message}</Button>
        </View>
      </View>
    )
  }
}

StartQuiz.propTypes = {
  startQuiz: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  instruction: PropTypes.string.isRequired,
  practice: PropTypes.bool.isRequired,
  max: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
}

StartQuiz.defaultProps = {
  practice: true
}

export default StartQuiz
