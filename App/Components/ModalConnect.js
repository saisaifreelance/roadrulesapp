import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableWithoutFeedback } from 'react-native'
import { StackNavigator } from 'react-navigation'
import Communication from '../Services/Communication'
import IconButton from './IconButton'
import Button from './Button'
import styles from './Styles/ModalConnectStyle'
import { Colors, Metrics, Images } from '../Themes'
import { Contact } from '../Types'

const communication = Communication.create()

export default class ModalConnect extends Component {
  // // Prop type warnings
  static propTypes = {
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    contact: Contact.isRequired,
    close: PropTypes.func
  }

  // Defaults for props
  static defaultProps = {
    title: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
    message: 'Hi, I\'m a Road Rules App user, requesting assistance',
  }

  constructor (props) {
    super(props)
    this.onContactPhoneCall = this.onContactPhoneCall.bind(this)
    this.onContactFacebook = this.onContactFacebook.bind(this)
    this.onContactWhatsApp = this.onContactWhatsApp.bind(this)
    this.close = this.close.bind(this)
  }

  onContactPhoneCall () {
    const numberKey = Object.keys(this.props.contact.phone)[0]
    const number = this.props.contact.phone[numberKey].value
    communication
      .call({
        number,
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  onContactFacebook () {
    const messengerKey = Object.keys(this.props.contact.messenger)[0]
    const messenger = this.props.contact.messenger[messengerKey].value
    communication
      .messenger({
        messenger,
        message: this.props.message,
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  onContactWhatsApp () {
    const whatsappKey = Object.keys(this.props.contact.whatsapp)[0]
    const whatsapp = this.props.contact.phone[whatsappKey].value
    communication
      .whatsapp({
        whatsapp,
        message: this.props.message,
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  close () {
    if (this.props.close) {
      return this.props.close()
    }
    this.props.navigation.goBack()
  }

  render () {
    return (
      <TouchableWithoutFeedback onPress={this.close}>
        <View style={styles.modalContainer}>
          <TouchableWithoutFeedback enabled={false}>
            <View style={styles.container}>
              <View style={styles.modalControlsContainer}>
                <IconButton icon='close' onPress={this.close} />
              </View>
              <View style={styles.section}>
                <Text style={styles.title}>{this.props.title}</Text>
              </View>
              <View style={styles.section}>
                <Button
                  icon={Images.call}
                  color={Colors.snow}
                  backgroundColor={Colors.bloodOrange}
                  style={{marginTop: Metrics.baseMargin}}
                  onPress={this.onContactPhoneCall}>Phonecall</Button>

                <Button
                  icon={Images.whatsapp}
                  color={Colors.snow}
                  backgroundColor={Colors.whatsapp}
                  style={{marginTop: Metrics.baseMargin}}
                  onPress={this.onContactWhatsApp}>WhatsApp</Button>

                <Button
                  icon={Images.facebook}
                  color={Colors.snow}
                  backgroundColor={Colors.facebook}
                  style={{marginTop: Metrics.baseMargin}}
                  onPress={this.onContactFacebook}>Facebook</Button>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export const ModalNavigator = StackNavigator({
  ModalConnect: {
    screen: (props) => (<ModalConnect {...props} {...props.navigation.state.params} />)
  }
}, {
  cardStyle: {
    opacity: 1,
    backgroundColor: 'black'
  },
  initialRouteName: 'ModalConnect',
  headerMode: 'none',
  // Keeping this here for future when we can make
  navigationOptions: {
    header: {
      left: (
        <IconButton
          icon='close'
          onPress={() => window.alert('pop')}
        />
      ),
      style: {
        backgroundColor: 'green'
      }
    }
  }
})
