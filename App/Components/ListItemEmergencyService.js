import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/ListItemEmergencyServiceStyle'
import {Colors, Metrics} from '../Themes'
import { EmergencyService } from '../Types'
import MarkdownText from './MarkdownText'
import NetworkImage from './NetworkImage'

export default class ListItemEmergencyService extends Component {
  static propTypes = {
    emergencyService: EmergencyService.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.cardIndex, this.props.emergencyService.id)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex, this.props.emergencyService.id)
  }

  render () {
    const props = {
      style: [styles.container],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }
    let description = null
    let image = null
    if (this.props.emergencyService.description) {
      description = (
        <Text style={styles.description}>{this.props.emergencyService.description}</Text>
      )
    }
    if (this.props.emergencyService.image) {
      image = (
        <NetworkImage
          url={'logo.png'}
          containerStyle={{alignItems: 'center', padding: Metrics.smallMargin}}
          style={{width: 300, height: 200}}
        />
      )
    }
    return (
      <TouchableOpacity {...props}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.emergencyService.title}</Text>
          {description}
        </View>
        {image}
        <MarkdownText>{this.props.emergencyService.text}</MarkdownText>
      </TouchableOpacity>
    )
  }
}
