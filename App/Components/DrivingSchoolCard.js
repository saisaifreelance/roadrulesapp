import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/DrivingSchoolCardStyle'
import { Images, Fonts, Colors } from '../Themes'


export default class DrivingSchoolCard extends Component {
  // Prop type warnings
  static propTypes = {
    description: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.number.isRequired,
    rating: PropTypes.number.isRequired,
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    onPress: PropTypes.func.isRequired
  }

  // Defaults for props
  static defaultProps = {

  }

  render () {
    const stars = []
    for (let i = 0; i < 5; i += 1) {
      if (this.props.rating > i) {
        stars.push(<Icon key={i} name="star" size={Fonts.size.small} color={Colors.fire} style={styles.star}  />)
      } else {
        stars.push(<Icon key={i} name="star-border" size={Fonts.size.small} color={Colors.fire} style={styles.star}  />)
      }
    }
    return (
      <TouchableOpacity onPress={this.props.onPress} style={[styles.container, this.props.style]}>
        <View style={styles.imageContainer}>
          <Image source={this.props.image} resizeMode='contain' style={styles.image} />
        </View>
        <Text style={styles.location}>{this.props.location}</Text>
        <Text style={styles.title} numberOfLines={2}>{this.props.title}</Text>
        <Text style={styles.description}>{`US$${this.props.description} per lesson`}</Text>
        <View style={styles.stars}>
          {stars}
        </View>
      </TouchableOpacity>
    )
  }
}
