import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  FlatList,
  ScrollView,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import moment from 'moment'
import styles from './Styles/CalendarStyle'
import { Metrics } from '../Themes'
import Day from './Day'

const VIEW_INDEX = 2

function getNumberOfWeeks (month, weekStart) {
  const firstDay = moment(month).startOf('month').day()
  const offset = (firstDay - weekStart + 7) % 7
  const days = moment(month).daysInMonth()
  return Math.ceil((offset + days) / 7)
}

export default class Calendar extends Component {
  constructor (props) {
    super(props)
    const currentMoment = moment(props.startDate)
    const selectedMoment = moment(props.selectedDate)
    let calendarDates = null
    let eventDatesMap = null
    if (!props.horizontal) {
      calendarDates = this.getStack(currentMoment).map((date, i) => ({date, key: i}))
      eventDatesMap = this.prepareEventDates(props.eventDates, props.events)

    }
    this.state = {
      currentMoment,
      selectedMoment,
      rowHeight: null,
      calendarDates,
      eventDatesMap
    }
    this.scollTimeout = null

    this.scrollEnd = this.scrollEnd.bind(this)
    this.momentumScrollEnd = this.momentumScrollEnd.bind(this)
    this.momentumScrollBegin = this.momentumScrollBegin.bind(this)
    this.scrollEndDrag = this.scrollEndDrag.bind(this)
    this.scrollBeginDrag = this.scrollBeginDrag.bind(this)
    this.onEndReached = this.onEndReached.bind(this)
  }

  static propTypes = {
    currentMonth: PropTypes.any,
    customStyle: PropTypes.object,
    horizontal: PropTypes.bool,
    dayHeadings: PropTypes.array,
    eventDates: PropTypes.array,
    monthNames: PropTypes.array,
    nextButtonText: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]),
    onDateSelect: PropTypes.func,
    onSwipeNext: PropTypes.func,
    onSwipePrev: PropTypes.func,
    onTouchNext: PropTypes.func,
    onTouchPrev: PropTypes.func,
    onTitlePress: PropTypes.func,
    prevButtonText: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]),
    removeClippedSubviews: PropTypes.bool,
    scrollEnabled: PropTypes.bool,
    selectedDate: PropTypes.any,
    showControls: PropTypes.bool,
    showEventIndicators: PropTypes.bool,
    startDate: PropTypes.any,
    titleFormat: PropTypes.string,
    today: PropTypes.any,
    weekStart: PropTypes.number,
    calendarFormat: PropTypes.string
  }

  static defaultProps = {
    customStyle: {},
    horizontal: true,
    width: Metrics.screenWidth,
    dayHeadings: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    eventDates: [],
    monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    nextButtonText: 'Next',
    prevButtonText: 'Prev',
    removeClippedSubviews: true,
    scrollEnabled: false,
    showControls: false,
    showEventIndicators: false,
    startDate: moment().format('YYYY-MM-DD'),
    titleFormat: 'MMMM YYYY',
    weekStart: 1,
    calendarFormat: 'monthly' // weekly or monthly
  }

  componentDidMount () {
    // fixes initial scrolling bug on Android
    setTimeout(() => this.scrollToItem(VIEW_INDEX), 0)
  }

  componentDidUpdate () {
    this.scrollToItem(VIEW_INDEX)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.selectedDate && this.props.selectedDate !== nextProps.selectedDate) {
      this.setState({selectedMoment: nextProps.selectedDate})
    }
    if (nextProps.currentMonth) {
      this.setState({currentMoment: moment(nextProps.currentMonth)})
    }
  }

  componentWillUnmount () {
    clearTimeout(this.scollTimeout)
  }

  getStack (currentMoment) {
    if (!this.props.horizontal) {
      const res = []

      for (let i = 0; i <= 5; i++) {
        if (this.props.calendarFormat === 'monthly') {
          res.push(moment(currentMoment).add(i, 'month'))
        } else {
          res.push(moment(currentMoment).add(i, 'week'))
        }
      }
      return res
    }
    if (this.props.scrollEnabled) {
      let i = -VIEW_INDEX
      const res = []

      for (i; i <= VIEW_INDEX; i++) {
        if (this.props.calendarFormat === 'monthly') {
          res.push(moment(currentMoment).add(i, 'month'))
        } else {
          res.push(moment(currentMoment).add(i, 'week'))
        }
      }
      return res
    }
    return [moment(currentMoment)]
  }

  prepareEventDates (eventDates, events) {
    const parsedDates = {}

    if (events) {
      events.forEach(event => {
        if (event.date) {
          parsedDates[event.date] = event
        }
      })
    } else {
      eventDates.forEach(event => {
        parsedDates[event] = {}
      })
    }

    return parsedDates
  }

  selectDate (date) {
    if (this.props.selectedDate === undefined) {
      this.setState({ selectedMoment: date,
        currentMoment: date})
    }
  }

  selectAndJumpToToday () {
    const today = new Date()
    const newMoment = moment(this.state.currentMonthMoment).set('month', today.getMonth())
    this.setState({
      selectedMoment: today,
      currentMonthMoment: newMoment
    })
  }

  onPrev = () => {
    const newMoment = this.props.calendarFormat === 'monthly' ?
      moment(this.state.currentMoment).subtract(1, 'month'):
      moment(this.state.currentMoment).subtract(1, 'week')
    this.setState({ currentMoment: newMoment })
    this.props.onTouchPrev && this.props.onTouchPrev(newMoment)
  }

  onNext = () => {
    const newMoment = this.props.calendarFormat === 'monthly' ?
      moment(this.state.currentMoment).add(1, 'month'):
      moment(this.state.currentMoment).add(1, 'week')
    this.setState({ currentMoment: newMoment })
    this.props.onTouchNext && this.props.onTouchNext(newMoment)
  }

  scrollToItem (itemIndex) {
    if (this.props.horizontal) {
      const scrollToX = itemIndex * this.props.width
      if (this.props.scrollEnabled && this._calendar) {
        this._calendar.scrollTo({ y: 0, x: scrollToX, animated: false })
      }
      return
    }

    // TODO ADD SCROLL TO Y
  }

  scrollBeginDrag (event) {
    clearTimeout(this.scollTimeout)
  }

  scrollEndDrag (event) {
    clearTimeout(this.scollTimeout)
    const { nativeEvent: { contentOffset: { x, y } } } = event
    if (!this.props.horizontal) {
      this.scollTimeout = setTimeout(() => { this.scrollEnd(x, y) }, 500)
    }
  }

  momentumScrollBegin (event) {
    clearTimeout(this.scollTimeout)
  }

  momentumScrollEnd (event) {
    clearTimeout(this.scollTimeout)
    const { nativeEvent: { contentOffset: { x, y } } } = event
    this.scrollEnd(x, y)
  }

  scrollEnd (x, y) {
    clearTimeout(this.scollTimeout)
    if (this.props.horizontal) {
      const position = x
      const currentPage = position / this.props.width
      const newMoment = this.props.calendarFormat === 'monthly' ? moment(this.state.currentMoment).add(currentPage - VIEW_INDEX, 'month') : moment(this.state.currentMoment).add(currentPage - VIEW_INDEX, 'week')
      this.setState({currentMoment: newMoment})
      if (currentPage < VIEW_INDEX) {
        this.props.onSwipePrev && this.props.onSwipePrev(newMoment)
      } else if (currentPage > VIEW_INDEX) {
        this.props.onSwipeNext && this.props.onSwipeNext(newMoment)
      }
      return
    }

    return
    const position = y
    const calendarDates = this.state.calendarDates
    let newMoment = null
    let height = 0
    for (let i = 0; !newMoment && i < calendarDates.length; i += 1) {
      const numOfWeeks = this.props.calendarFormat === 'weekly' ? 1 : getNumberOfWeeks(calendarDates[i].date, this.props.weekStart)
      height += (numOfWeeks * this.state.rowHeight) + 100

      if (height >= position) {
        newMoment = calendarDates[i]
        height -= (numOfWeeks * this.state.rowHeight) + 90
      }
    }
    if (newMoment) {
      this._calendar.scrollToOffset({ offset: height, animated: true })
    }
  }

  onEndReached () {
    const { calendarDates } = this.state
    const currentMoment = calendarDates[calendarDates.length - 1].date
    const newCalendarDates = [...calendarDates]
    for (let i = 0; i < 3; i += 1) {
      if (this.props.calendarFormat === 'monthly') {
        newCalendarDates.push({date: moment(currentMoment).add(i + 1, 'month'), key: i + calendarDates.length})
      } else {
        newCalendarDates.push({date: moment(currentMoment).add(i + 1, 'week'), key: i + calendarDates.length})
      }
    }
    this.setState({calendarDates: newCalendarDates})
  }

  onWeekRowLayout = (event) => {
    if (this.state.rowHeight !== event.nativeEvent.layout.height) {
      this.setState({ rowHeight: event.nativeEvent.layout.height })
    }
  }

  getStartMoment (calFormat, currMoment) {
    const weekStart = this.props.weekStart

    let res
    if (calFormat === 'monthly') {
      res = moment(currMoment).startOf('month')
    } else {
      // weekly
      let sundayMoment = moment(currMoment).startOf('week')
      if (weekStart > 0) {
        res = moment(currMoment).isoWeekday(weekStart)
        if (res.diff(currMoment) > 0) {
          res = moment(currMoment).subtract(7, 'day').isoWeekday(weekStart)
        }
      } else {
        res = sundayMoment
      }
    }
    return res
  }

  renderCalendarView (calFormat, argMoment, eventsMap) {
    let renderIndex = 0
    let weekRows = []
    let days = []

    const startOfArgMoment = this.getStartMoment(calFormat, argMoment)
    const selectedMoment = moment(this.state.selectedMoment)
    const weekStart = this.props.weekStart
    const todayMoment = moment(this.props.today)
    const todayIndex = todayMoment.date() - 1
    const argDaysCount = calFormat === 'monthly' ? argMoment.daysInMonth(): 7
    const offset = calFormat === 'monthly' ? (startOfArgMoment.isoWeekday() - weekStart + 7) % 7 : 0
    const selectedIndex = moment(selectedMoment).date() - 1

    do {
      const dayIndex = renderIndex - offset
      const isoWeekday = (renderIndex + weekStart) % 7
      const thisMoment = moment(startOfArgMoment).add(dayIndex, 'day')

      if (dayIndex >= 0 && dayIndex < argDaysCount) {
        days.push((
          this.renderDay({
            startOfMonth: startOfArgMoment,
            isWeekend: isoWeekday === 0 || isoWeekday === 6,
            key: renderIndex,
            onPress: () => {
              this.selectDate(thisMoment)
              this.props.onDateSelect && this.props.onDateSelect(thisMoment ? thisMoment.format() : null )
            },
            caption: thisMoment.format('D'),
            isToday: todayMoment.format('YYYY-MM-DD') === thisMoment.format('YYYY-MM-DD'),
            isSelected: selectedMoment.isSame(thisMoment),
            event: eventsMap[thisMoment.format('YYYY-MM-DD')] ||
            eventsMap[thisMoment.format('YYYYMMDD')],
            showEventIndicators: this.props.showEventIndicators,
            customStyle: this.props.customStyle
          })
        ))
      } else {
        days.push(
          this.renderDay({
            key: renderIndex,
            filler: true,
            customStyle: this.props.customStyle
          })
        )
      }
      if (renderIndex % 7 === 6) {
        weekRows.push(
          <View
            key={weekRows.length}
            onLayout={weekRows.length ? undefined : this.onWeekRowLayout}
            style={[styles.weekRow, this.props.customStyle.weekRow]}
          >
            {days}
          </View>)
        days = []
        if (dayIndex + 1 >= argDaysCount) {
          break
        }
      }
      renderIndex += 1
    } while (true)
    const containerStyle = [styles.monthContainer, this.props.customStyle.monthContainer]
    return (
      <View key={`${startOfArgMoment.format('YYYY-MM-DD')}-${calFormat}`} style={containerStyle}>
        {this.props.horizontal ? null : this.renderSectionTopBar(startOfArgMoment)}
        {weekRows}
      </View>
    )
  }

  renderDay (props) {
    if (this.props.renderDay) {
      return this.props.renderDay(props)
    }
    return <Day {...props} />
  }

  renderHeading () {
    let headings = []
    let i = 0

    for (i; i < 7; ++i) {
      const j = (i + this.props.weekStart) % 7
      headings.push(
        <Text
          key={i}
          style={j === 0 || j === 6 ?
            [styles.weekendHeading, this.props.customStyle.weekendHeading] :
            [styles.dayHeading, this.props.customStyle.dayHeading]}
        >
          {this.props.dayHeadings[j]}
        </Text>
      )
    }

    return (
      <View style={[styles.calendarHeading, this.props.customStyle.calendarHeading]}>
        {headings}
      </View>
    )
  }

  renderTopBar () {
    let localizedMonth = this.props.monthNames[this.state.currentMoment.month()]
    if (!this.props.horizontal) {
      return null
    }
    return this.props.showControls ? (
      <View style={[styles.calendarControls, this.props.customStyle.calendarControls]}>
        <TouchableOpacity
          style={[styles.controlButton, this.props.customStyle.controlButton]}
          onPress={this.onPrev}
        >
          <Text style={[styles.controlButtonText, this.props.customStyle.controlButtonText]}>
            {this.props.prevButtonText}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.title} onPress={() => this.props.onTitlePress && this.props.onTitlePress()}>
          <Text style={[styles.titleText, this.props.customStyle.title]}>
            {localizedMonth} {this.state.currentMoment.year()}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.controlButton, this.props.customStyle.controlButton]}
          onPress={this.onNext}
        >
          <Text style={[styles.controlButtonText, this.props.customStyle.controlButtonText]}>
            {this.props.nextButtonText}
          </Text>
        </TouchableOpacity>
      </View>
    ) : (
      <View style={[styles.calendarControls, this.props.customStyle.calendarControls]}>
        <Text style={[styles.title, this.props.customStyle.title]}>
          {this.state.currentMoment.format(this.props.titleFormat)}
        </Text>
      </View>
    )
  }

  renderSectionTopBar (currentMoment) {
    return (
      <View style={[styles.calendarControls, this.props.customStyle.calendarControls]}>
        <Text style={[styles.title, this.props.customStyle.title]}>
          {currentMoment.format(this.props.titleFormat)}
        </Text>
      </View>
    )
  }

  render () {
    let content = null
    if (!this.props.horizontal) {
      content = (
        <FlatList
          onEndReached={this.onEndReached}
          onEndReachedThreshold={200}
          data={this.state.calendarDates}
          renderItem={({item: {date}}) => this.renderCalendarView(this.props.calendarFormat, moment(date), this.state.eventDatesMap)}
          ref={calendar => { this._calendar = calendar }}
          scrollEnabled
          removeClippedSubviews={this.props.removeClippedSubviews}
          scrollEventThrottle={1000}
          showsVerticalScrollIndicator={false}
          automaticallyAdjustContentInsets={false}
          onScrollBeginDrag={(event) => this.scrollBeginDrag(event)}
          onScrollEndDrag={(event) => this.scrollEndDrag(event)}
          onMomentumScrollBegin={(event) => this.momentumScrollBegin(event)}
          onMomentumScrollEnd={(event) => this.momentumScrollEnd(event)}
          style={[{flex: 1}, this.props.customStyle.calendarScrollView]}
        />
      )
    } else if (this.props.scrollEnabled) {
      const calendarDates = this.getStack(this.state.currentMoment)
      const eventDatesMap = this.prepareEventDates(this.props.eventDates, this.props.events)
      const numOfWeeks = this.props.calendarFormat === 'weekly' ? 1 : getNumberOfWeeks(this.state.currentMonthMoment, this.props.weekStart)

      content = (
        <ScrollView
          ref={calendar => { this._calendar = calendar }}
          horizontal={this.props.horizontal}
          scrollEnabled
          pagingEnabled={this.props.horizontal}
          removeClippedSubviews={this.props.removeClippedSubviews}
          scrollEventThrottle={1000}
          showsHorizontalScrollIndicator={false}
          automaticallyAdjustContentInsets={false}
          onScrollBeginDrag={(event) => this.scrollBeginDrag(event)}
          onScrollEndDrag={(event) => this.scrollEndDrag(event)}
          onMomentumScrollBegin={(event) => this.momentumScrollBegin(event)}
          onMomentumScrollEnd={(event) => this.momentumScrollEnd(event)}
          style={[{
            height: this.state.rowHeight ? this.state.rowHeight * numOfWeeks : null,
          }, this.props.customStyle.calendarScrollView]}
        >
          {calendarDates.map((date) => this.renderCalendarView(this.props.calendarFormat, moment(date), eventDatesMap))}
        </ScrollView>
      )
    } else {
      const calendarDates = this.getStack(this.state.currentMoment)
      const eventDatesMap = this.prepareEventDates(this.props.eventDates, this.props.events)

      content = (
        <View ref={calendar => { this._calendar = calendar }}>
          {calendarDates.map((date) => this.renderCalendarView(this.props.calendarFormat, moment(date), eventDatesMap))}
        </View>
      )
    }

    return (
      <View style={[styles.calendarContainer, this.props.customStyle.calendarContainer]}>
        {this.renderTopBar()}
        {this.renderHeading(this.props.titleFormat)}
        { content }
      </View>
    )
  }
}
