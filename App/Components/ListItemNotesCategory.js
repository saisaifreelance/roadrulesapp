import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text, Image, Dimensions } from 'react-native'
import HTML from 'react-native-render-html'
import { Colors } from '../Themes'
import MarkdownText from './MarkdownText'
import styles from './Styles/ListItemNotesCategoryStyle'

export default class ListItemNotesCategory extends Component {
  // Prop type warnings
  static propTypes = {
    category: PropTypes.object.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.cardIndex)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex)
  }

  render () {
    const { category } = this.props
    const cardStyles = {}
    let hoverContent = null
    let faceContent = null
    let selectedContent = null
    let title = null
    let description = null
    let divider = null
    let text = null
    let props = {
      style: [styles.container, this.props.style, cardStyles],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    if (category.title) {
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{category.title}</Text>
        </View>
      )
    }

    if (this.props.selected === this.props.cardIndex) {
      props.disabled = true
      
      if (category.html) {
        // text = (<MarkdownText>{category.html}</MarkdownText>)
        text = (
        <HTML
          html={category.html} imagesMaxWidth={Dimensions.get('window').width}
          renderers={{
            img: (htmlAttribs) => {
              const period = htmlAttribs.src.indexOf(".")
              const uri = htmlAttribs.src.slice(0, period)
              return (
              <View
              style={{ width: Dimensions.get('window').width}}
              >
              <Image source={{uri}} style={{width: Dimensions.get('window').width - 20, height: 300, resizeMode: "contain"}} />
              </View>)
            }
      }}  
          />)
        title = null
      }
      selectedContent = (<View style={styles.selectedContent} />)
      faceContent = null
    }

    if (this.props.hovered === this.props.cardIndex) {
      text = (<MarkdownText>{category.description}</MarkdownText>)
      divider = <View style={styles.divider} />
    }

    if ((this.props.selected !== this.props.cardIndex) && typeof this.props.selected === 'number') {
      cardStyles.height = 0
      cardStyles.overflow = 'hidden'
      hoverContent = null
      faceContent = null
      selectedContent = null
      title = null
      text = null
      props = {
        disabled: true
      }
    }

    return (
      <View>
        <TouchableOpacity {...props}>
          {title}
          {divider}
          {text}
          {hoverContent}
          {faceContent}
        </TouchableOpacity>
      </View>
    )
  }
}
