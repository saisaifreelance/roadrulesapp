import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, View, LayoutAnimation, Text } from 'react-native'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import Header from './Header'
import calcHeight from '../Lib/CalcHeight'

const {Types, Properties} = LayoutAnimation
const ERROR_MESSAGE = 'CardStack component must have at least two child Card components. Please check the children of this CardStack instance.'
import styles from './Styles/ExpandableListStyle'
import { Images, Colors, Metrics } from '../Themes'

export default class ExpandableList extends Component {
  static propTypes = {
    height: PropTypes.number,
    width: PropTypes.number,
    backgroundColor: PropTypes.string,
    hoverOffset: PropTypes.number,
    transitionDuration: PropTypes.number,
  }

  static defaultProps = {
    height: 600,
    width: 350,
    backgroundColor: 'f8f8f8',
    hoverOffset: 30,
    transitionDuration: 300,
  }

  constructor (props) {
    super(props)
    const childrenLength = props.children && props.children.length || 1
    if (childrenLength <= 1) throw new Error(ERROR_MESSAGE)

    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)

    this.state = {
      selectedCardIndex: null,
      hoveredCardIndex: null
    }

    this._PRESET = LayoutAnimation.create(
      props.transitionDuration, Types.easeInEaseOut, Properties.opacity
    )
  }

  componentWillReceiveProps ({transitionDuration}) {
    if (this.props.transitionDuration !== transitionDuration) {
      this._PRESET = LayoutAnimation.create(
        transitionDuration, Types.easeInEaseOut, Properties.opacity
      )
    }
  }

  handlePress (cardId) {
    LayoutAnimation.configureNext(this._PRESET)
    const index = (this.state.selectedCardIndex === cardId) ? null : cardId
    this.setState({selectedCardIndex: index, hoveredCardIndex: null})
    if (this.props.onPress) this.props.onPress()
  }

  handleLongPress (cardId) {
    LayoutAnimation.configureNext(this._PRESET)
    this.setState({hoveredCardIndex: cardId})
  }

  renderCards () {
    const cloneCard = (child, cardIndex, children) => {
      const indexs = {
        selectedIndex: this.state.selectedCardIndex,
        hoveredIndex: this.state.hoveredCardIndex,
        cardIndex,
      }
      const height = calcHeight(
        indexs,
        this.props.height,
        this.props.hoverOffset,
        children.length
      )
      return React.cloneElement(child, {
        key: cardIndex,
        cardId: cardIndex,
        selected: cardIndex === this.state.selectedCardIndex,
        hovered: cardIndex === this.state.hoveredCardIndex,
        height,
        onPress: this.handlePress,
        onLongPress: this.handleLongPress
      })
    }
    return this.props.children.map(cloneCard)
  }

  render () {
    const stackStyles = {
      backgroundColor: Colors.steel,
      width: this.props.width,
    }
    const {selectedCardIndex: selectedIndex} = this.state
    const scrollable = (!selectedIndex && selectedIndex !== 0)

    return (
      <ParallaxScrollView
        contentContainerStyle={[this.props.style, stackStyles]}
        scrollEnabled={scrollable}
        bounces={scrollable}
        headerBackgroundColor={Colors.charcoal}
        contentBackgroundColor={Colors.steel}
        parallaxHeaderHeight={scrollable ? Metrics.parallaxHeaderHeight : 0}
        renderForeground={() => (
          <Header background={Images.backgroundTrafficFines} title='Traffic Fines'/>
        )}>
        {this.renderCards()}
      </ParallaxScrollView>
    )
  }
}
