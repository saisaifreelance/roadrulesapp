import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, Image } from 'react-native'
import styles from './Styles/HeaderStyle'

export default class Header extends Component {
  // // Prop type warnings
  static propTypes = {
    background: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    style: PropTypes.oneOfType([PropTypes.number, PropTypes.object])
  }

  // Defaults for props
  static defaultProps = {
    style: {}
  }

  render () {
    return (
      <View style={[styles.container, this.props.style]}>
        <Image style={styles.backgroundImage} resizeMode='cover' source={this.props.background} />
        <View style={styles.backgroundOverlay} />
        <View style={styles.content}>
          <Text style={styles.title}>{this.props.title}</Text>
        </View>
      </View>
    )
  }
}
