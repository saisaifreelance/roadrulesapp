import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text } from 'react-native'
import styles from './Styles/ListItemEmergencyServiceCategoryStyle'
import { Colors } from '../Themes'
import MarkdownText from './MarkdownText'
import { EmergencyServiceCategory } from '../Types'

export default class ListItemEmergencyServiceCategory extends Component {
  // Prop type warnings
  static propTypes = {
    category: EmergencyServiceCategory.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.cardIndex)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex)
  }

  render () {
    const { category } = this.props
    const cardStyles = {}
    let selectedContent = null
    let title = null
    let text = null
    let props = {
      style: [styles.container, this.props.style, cardStyles],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    if (category.title && (category.text || category.description)) {
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{category.title}</Text>
        </View>
      )
    }

    if (category.title && !(category.text || category.description)) {
      title = (
        <View style={[styles.titleContainer, {borderBottomColor: 'transparent', borderBottomWidth: 0}]}>
          <Text style={styles.title}>{category.title}</Text>
        </View>
      )
    }

    if (category.text) {
      text = (<MarkdownText>{category.text}</MarkdownText>)
    }

    if (this.props.selected === this.props.cardIndex) {
      props.disabled = true
      selectedContent = (<View style={styles.selectedContent}/>)
    }

    if (this.props.selected !== this.props.cardIndex) {
      if (category.text) {
        text = (<MarkdownText>{`${category.text.slice(0, 160)}...`}</MarkdownText>)
      }
    }

    if ((this.props.selected !== this.props.cardIndex) && typeof this.props.selected === 'number') {
      cardStyles.height = 0
      cardStyles.overflow = 'hidden'
      selectedContent = null
      title = null
      text = null
      props = {
        disabled: true
      }
    }

    return (
      <View>
        <TouchableOpacity {...props}>
          {title}
          {text}
        </TouchableOpacity>
        {selectedContent}
      </View>
    )
  }
}
