import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, View, Text } from 'react-native'
import styles from './Styles/MapListingsStyle'
import DrivingSchoolCard from '../Components/DrivingSchoolCard'
import { Images } from '../Themes'

export default class MapListings extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    const views = this.props.drivingSchools.map((drivingSchool, index) => {
      return (
        <View style={styles.drivingSchoolCard}>
          <DrivingSchoolCard
            onPress={() => {}}
            description={drivingSchool.cost}
            title={drivingSchool.title}
            image={{uri: 'image_22'}}
            location={drivingSchool.city}
            rating={drivingSchool.rating} />
          </View>

      )
    })
    return (
      <ScrollView
        horizontal
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        {views}
        </ScrollView>
    )
  }
}
