import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Animated, InteractionManager, Platform, View, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import TabBar from '../Components/TabBar'
import styles from './Styles/DrivingSchoolSearchStyle'
import { Colors, Fonts, Metrics } from '../Themes'

export default class DrivingSchoolSearch extends Component {
  // Prop type warnings
  static propTypes = {
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired,
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    this.onLayout = this.onLayout.bind(this)
    this.onIndexChange = this.onIndexChange.bind(this)
    this.state = {
      height: null,
      width: null,
      index: 0,
    }
  }

  onLayout ({ nativeEvent: { layout: { width, height, x, y } } }) {
    this.setState({ width, height })
    // alert(height)
  }

  onIndexChange = index => {
    this.setState({ index })
  }

  render () {
    let collapse = null
    let location = null
    let date = null
    let tub = null
    let tabs = null
    tabs = (
      <TabBar
        tabs={['ALL', 'PROVISIONAL', 'PRACTICAL']}
        onIndexChange={this.onIndexChange}
        containerWidth={Metrics.screenWidth}
        index={this.state.index}
        backgroundColor="transparent"
        activeTextColor={Colors.snow}
        inactiveTextColor={'rgba(255, 255, 255, 0.85)'}
        underlineStyle={{backgroundColor: Colors.snow}}
        style={{borderColor: Colors.transparent, borderBottomWidth: 0}}
      />
    )
    if (this.props.showSearch) {
      collapse = (
        <TouchableOpacity style={styles.top}>
          <Icon style={styles.buttonIcon} size={Fonts.size.h2} color={Colors.charcoal} name="keyboard-arrow-up" />
        </TouchableOpacity>
      )
      location = (
        <TouchableOpacity
          style={styles.button}
          onPress={() => { this.props.openModal('location') }}
          shadowColor={Colors.charcoal}
          shadowOffset={{width: 4, height: 4}}
          shadowOpacity={0.5}
          shadowRadius={4}>
          <Icon style={styles.buttonIcon} size={Fonts.size.h4} color={Colors.snow} name="location-on" />
          <Text style={styles.buttonText}>Anywhere</Text>
        </TouchableOpacity>
      )
      date = (
        <TouchableOpacity style={styles.button} onPress={() => { this.props.openModal('time') }}>
          <Icon style={styles.buttonIcon} size={Fonts.size.h4} color={Colors.charcoal} name="date-range" />
          <Text style={styles.buttonText}>Anytime</Text>
        </TouchableOpacity>
      )
      tub = (
        <TouchableOpacity style={styles.button} onPress={() => { this.props.openModal('tub') }}>
          <Icon style={styles.buttonIcon} size={Fonts.size.h4} color={Colors.charcoal} name="hot-tub" />
          <Text style={styles.buttonText}>1 guest</Text>
        </TouchableOpacity>
      )
    } else {
      location = (
        <TouchableOpacity style={styles.button} onPress={() => { this.props.openModal('location') }}>
          <Icon style={styles.buttonIcon} size={Fonts.size.h4} color={Colors.charcoal} name="location-on" />
          <Text style={styles.buttonText}>Anywhere</Text>
        </TouchableOpacity>
      )
    }
    return (
      <View style={[styles.container]} onLayout={this.props.onLayout}>
        {collapse}
        {location}
        {date}
        {tabs}
      </View>
    )
  }
}
