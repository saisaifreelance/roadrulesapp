import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text, TextInput, ScrollView } from 'react-native'
import styles from './Styles/ModalLocationStyle'
import NavBar from '../Components/NavBar'
import { Metrics, Colors } from '../Themes'
import ModalNavbarRightTextElement from './ModalNavbarRightTextElement'

const Location = ({ onPress, children }) => (
  <TouchableOpacity onPress={onPress} style={styles.locationContainer}>
    <Text style={styles.locationText}>{children}</Text>
  </TouchableOpacity>
)

export default class ModalLocation extends Component {
  // Prop type warnings
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.transparent,
            }
          }}
          leftElement='close'
          rightElement={(<ModalNavbarRightTextElement>Clear</ModalNavbarRightTextElement>)}
          onLeftElementPress={this.props.closeModal}
          onRightElementPress={this.props.closeModal}
        />
        <View style={styles.titleContainer}>
          <TextInput
            style={styles.title}
            value=""
            autoCorrect={false}
            placeholder={"Where?"}
            placeholderTextColor={'rgba(255, 255, 255, 0.75)'}
            onChangeText={() => {}}
            blurOnSubmit
            enablesReturnKeyAutomatically
            onSubmitEditing={() => {}}
            underlineColorAndroid='rgba(0,0,0,0)'
          />
        </View>
        <ScrollView style={styles.list} contentContainerStyle={styles.listContent}>
          <Location onPress={() => {}}>Harare</Location>
          <Location onPress={() => {}}>Bulawayo</Location>
          <Location onPress={() => {}}>Victoria Falls</Location>
          <Location onPress={() => {}}>Mutare</Location>
        </ScrollView>
      </View>
    )
  }
}
