import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import styles from './Styles/NavBarStyle'
import { Toolbar } from 'react-native-material-ui'
import { Fonts, Metrics } from '../Themes'

export default class NavBar extends Component {
  // // Prop type warnings
  static propTypes = {
    containerStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    ...Toolbar.propTypes
  }
  //
  // // Defaults for props
  static defaultProps = {
    style: {}
  }

  render () {
    return (
      <View style={this.props.containerStyle}>
        <Toolbar
          {...this.props}
          shadowOpacity={0}
          backgroundColor={'transparent'}
          shadowColor={'transparent'}
          shadowRadius={0}
          toolbarHeight={Metrics.navBarHeight}
          toolbarZIndex={1}
        />
      </View>
    )
  }
}
