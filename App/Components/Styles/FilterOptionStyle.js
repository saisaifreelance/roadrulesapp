import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics} from '../../Themes'

export default StyleSheet.create({
  container: {
    paddingVertical: Metrics.baseMargin,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
  },
  controlContainer: {
    marginLeft: Metrics.baseMargin,
    // alignItems: 'center',
    // justifyContent: 'center',
    // backgroundColor: 'red'
  },
  control: {

  },
  title: {
    ...Fonts.style.h6,
    color: Colors.charcoal,
  },
  description: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.medium,
    color: Colors.charcoal,
  }
})

export const controlStyles = StyleSheet.create({
  container: {
    flex: 0,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    color: Colors.amber,
  },
  label: {
    color: Colors.charcoal,
    marginLeft: 0,
    flex: 0,
  }
})
