import { StyleSheet } from 'react-native'
import {Colors, Fonts, Metrics } from '../../Themes'

export default StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    borderRadius: Metrics.screenWidth,
    padding: Metrics.baseMargin,
    backgroundColor: 'rgba(0,0,0,0.010)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  action: {
    backgroundColor: Colors.snow,
    paddingVertical: Metrics.smallMargin,
    paddingLeft: Metrics.baseMargin,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 1,
  },
  actionInner: {
    borderRightWidth: 1,
    borderRightColor: Colors.steel,
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstAction: {
    borderTopLeftRadius: Metrics.screenWidth,
    borderBottomLeftRadius: Metrics.screenWidth,
    // backgroundColor: 'red'
  },
  lastAction: {
    borderTopRightRadius: Metrics.screenWidth,
    borderBottomRightRadius: Metrics.screenWidth,
    // backgroundColor: 'blue',
  },
  icon: {
    color: Colors.charcoal,
    marginRight: Metrics.baseMargin,
    marginLeft: Metrics.baseMargin,
  },
  text: {
    ...Fonts.style.normal,
    color: Colors.charcoal
  }
})
