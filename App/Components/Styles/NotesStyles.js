import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  title: {
    fontSize: Fonts.size.h6,
    textAlign: 'center',
    marginHorizontal: 16,
    marginVertical: 16,
  },
  splash_image: {
    width: 300,
    height: 300,
    marginHorizontal: 16,
    marginVertical: 8,
    flex: 1,
  },
  instruction: {
    fontSize: Fonts.size.h6,
    textAlign: 'center',
    marginHorizontal: 16,
    marginVertical: 8,
    color: 'black'
  },
  buttons: {
    flexDirection: 'row',
    padding: 8,
  },
  button: {
    flex: 1,
    margin: 8,
  },
  sub_instruction: {
    fontSize: Fonts.size.regular,
    textAlign: 'center',
    marginHorizontal: 16,
    marginBottom: 16,
    marginTop: 8,
  },
})
