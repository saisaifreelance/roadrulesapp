import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginHorizontal: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin
  },
  divider: {
    marginVertical: Metrics.smallMargin,
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1
  },
  titleImageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleImage: {
    width: undefined,
    flex: 1,
    margin: Metrics.baseMargin,
    height: 150,
    marginVertical: 4
  },
  titleContainer: {
  },
  title: {
    ...Fonts.style.h5
  },
  hoverContent: {
    marginTop: Metrics.smallMargin
  },
  hoverContentText: {
    color: Colors.bloodOrange,
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
    textAlign: 'right'
  },
  selectedContentText: {
    ...Fonts.style.h6
  },
  selectedContent: {
    marginVertical: Metrics.baseMargin,
    marginHorizontal: Metrics.smallMargin,
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    padding: Metrics.baseMargin
  }
})
