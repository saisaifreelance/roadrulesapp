import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics} from '../../Themes'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  topContainer: {
    // backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
  },
  content: {
    backgroundColor: Colors.snow,
    height: (Metrics.screenHeight / 3) * 2,
  },
  container: {
    flex: 1
  },
  contentContainer: {
    backgroundColor: Colors.snow,
    padding: Metrics.doubleBaseMargin,
  },
  topBar: {
    height: Metrics.navBarHeight,
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: Metrics.baseMargin
  },
  topBarTextContainer: {
    alignItems: 'center'
  },
  topBarTextContainerCenter: {
    flex: 1,
    alignItems: 'center'
  },
  topBarText: {
    ...Fonts.style.normal,
    textAlign: 'center'
  },
  refreshControl: {
    opacity: 0,
  }
})
