import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts} from '../../Themes'

export default StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.silver,
  },
  contentContainer: {
    padding: Metrics.smallMargin
  },
  drivingSchoolCard: {
    margin: Metrics.smallMargin,
    marginTop: 0,
    borderTopColor: Colors.snow,

  }
})
