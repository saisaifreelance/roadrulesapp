import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics } from '../../Themes'

const width = (Metrics.screenWidth - (Metrics.baseMargin * 3)) / 2

export default StyleSheet.create({
  container: {
    width,
    backgroundColor: Colors.snow,
    elevation: 1,
    padding: 4,
    borderRadius: Metrics.buttonRadius,
  },
  imageContainer: {
    borderRadius: Metrics.buttonRadius,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width,
    borderRadius: Metrics.buttonRadius,
    height: 100,
  },
  location: {
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.bold,
    fontWeight: 'bold',
    color: Colors.panther,
    marginVertical: Metrics.smallMargin,
    // backgroundColor: 'red'
  },
  title: {
    ...Fonts.style.normal,
    fontWeight: 'bold',
    color: Colors.charcoal,
    // backgroundColor: 'blue'
  },
  description: {
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
  },
  stars: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  star: {

  }
})
