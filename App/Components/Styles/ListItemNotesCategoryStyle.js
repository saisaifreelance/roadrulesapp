import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginHorizontal: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin
  },
  divider: {
    marginVertical: Metrics.smallMargin,
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1
  },
  titleContainer: {
  },
  title: {
    ...Fonts.style.h5
  },
  serviceContainer: {
    margin: Metrics.baseMargin
  },
  service: {
    ...Fonts.style.h5,
    color: Colors.bloodOrange,
  },
  section: {
    margin: Metrics.smallMargin
  },
  hoverContent: {
    marginTop: Metrics.smallMargin
  },
  hoverContentText: {
    color: Colors.bloodOrange,
    ...Fonts.style.normal,
    fontSize: Fonts.size.small,
    textAlign: 'right'
  },
  selectedContentText: {
    ...Fonts.style.h6
  },
  selectedContent: {
    margin: Metrics.baseMargin,
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    padding: Metrics.baseMargin
  }
})
