import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.logo,
  container: {
    flex: 1
  },
  logoContainer: {
    ...ApplicationStyles.logo.logoContainer,
    flex: 1
  }
})
