import { StyleSheet } from 'react-native'
import { Fonts, Metrics } from '../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  }
})

export const markdownStyles = {
  heading1: {
    ...Fonts.style.normal,
    fontWeight: 'bold'
  },
  strong: {
    ...Fonts.style.h5
  },
  paragraph: {
    ...Fonts.style.normal,
    marginVertical: Metrics.smallMargin,
    textAlign: 'center',
  },
  view: {
    // borderWidth: 1
  }
}
