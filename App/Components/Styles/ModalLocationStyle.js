import { StyleSheet } from 'react-native'
import { Metrics, Colors } from '../../Themes'
import {Fonts, ApplicationStyles} from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.searchModal,
  locationContainer: {
    paddingVertical: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.frost,
    borderBottomWidth: 1
  },
  locationText: {
    ...Fonts.style.h6,
    color: Colors.snow,
  },
  inputContainer: {
    paddingBottom: Metrics.navBarHeight,
    paddingHorizontal: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.snow,
    borderBottomWidth: 1,
  },
  input: {
    ...Fonts.style.h4,
    color: Colors.snow,
  },
  list: {
    flex: 1,
  },
  listContent: {
    paddingHorizontal: Metrics.doubleBaseMargin,
    paddingVertical: Metrics.baseMargin,
  },
})
