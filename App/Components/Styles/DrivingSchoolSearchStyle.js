import { StyleSheet } from 'react-native'
import { Colors, Fonts, Metrics, ApplicationStyles } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.button,
  container: {
    padding: Metrics.baseMargin,
    paddingBottom: 0,
    backgroundColor: Colors.amber,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  top: {
    padding: Metrics.baseMargin,
    marginBottom: Metrics.baseMargin,
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {
    ...ApplicationStyles.button.button,
    backgroundColor: Colors.lightSnow,
    marginBottom: Metrics.baseMargin,
    elevation: 1,
  },
  buttonIcon: {
    ...ApplicationStyles.button.buttonTextIcon,
    marginRight: Metrics.smallMargin,
    flex: 0,
  },
  buttonText: {
    ...ApplicationStyles.button.buttonText,
    textAlign: 'left',
    ...Fonts.style.h6,
  },
})
