import { StyleSheet } from 'react-native'
import { Colors, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    backgroundColor: Colors.snow,
    borderRadius: 4,
    elevation: 1,
    marginHorizontal: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    padding: Metrics.baseMargin
  },
  titleContainer: {
    borderBottomColor: Colors.steel,
    borderBottomWidth: 1,
    flexDirection: 'row'
  },
  title: {
    ...Fonts.style.h5,
    flex: 1
  },
  titleContainerDefault: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleDefault: {
    ...Fonts.style.h5,
    flex: 1,
    textAlign: 'left'
  },
  fineContainer: {
    margin: Metrics.baseMargin
  },
  fine: {
    ...Fonts.style.h5,
    color: Colors.bloodOrange,
    textAlign: 'right',
    marginLeft: Metrics.baseMargin
  },
  section: {
    margin: Metrics.smallMargin
  }
})
