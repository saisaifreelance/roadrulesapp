import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  icon: {

  },
  text: {
    ...Fonts.style.h5,
    color: Colors.blue
  }
})
