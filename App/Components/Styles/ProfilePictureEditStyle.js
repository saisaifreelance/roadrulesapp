import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    flex: 1,
  },
  imageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageOuter: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: Colors.silver
  },
  image: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: Colors.silver
  },
  icon: {
    position: 'absolute',
    // bottom: Metrics.baseMargin,
    // right: Metrics.baseMargin,
    bottom: 0,
    right: 0,
  },
  title: {
    ...Fonts.style.h5,
    textAlign: 'center',
    marginTop: Metrics.smallMargin
  }
})
