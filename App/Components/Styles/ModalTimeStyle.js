import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'

const dimishedSnow = 'rgba(255, 255, 255, 0.75)'

const dayButtonWidth = Metrics.screenWidth / 7
const dayButtonCircleWidth = dayButtonWidth - 4
const dayButtonCircleRadius = dayButtonCircleWidth / 2

export default StyleSheet.create({
  ...ApplicationStyles.searchModal,
  titleContainer: {
    ...ApplicationStyles.modal.titleContainer,
    paddingBottom: Metrics.navBarHeight,
    paddingHorizontal: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.transparent,
    borderBottomWidth: 0,
  },
  title: {
    ...ApplicationStyles.modal.title,
    ...Fonts.style.h4,
    color: Colors.snow,
  },
  footer: {
    padding: Metrics.baseMargin,
    borderTopWidth: 1,
    borderTopColor: Colors.snow
  }
})

export const customStyle = StyleSheet.create({
  calendarContainer: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  calendarScrollView: {
    flex: 1
  },

  monthContainer: {
    width: Metrics.screenWidth,
  },
  calendarControls: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  controlButton: {
  },
  controlButtonText: {
    margin: 10,
    fontSize: 15,
  },
  title: {
    ...Fonts.style.h4,
    color: Colors.snow
  },
  titleText: {
    ...Fonts.style.h4,
    color: Colors.snow
  },
  calendarHeading: {
    flexDirection: 'row',
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderBottomColor: Colors.snow,
    paddingVertical: Metrics.baseMargin
  },
  dayHeading: {
    flex: 1,
    textAlign: 'center',
    marginVertical: 5,
    color: Colors.snow,
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.large,
  },
  weekendHeading: {
    flex: 1,
    textAlign: 'center',
    marginVertical: 5,
    color: dimishedSnow,
    fontFamily: Fonts.type.bold,
    fontSize: Fonts.size.large,
  },
  weekRow: {
    flexDirection: 'row',
  },
  weekendDayButton: {
    backgroundColor: Colors.transparent,
  },
  weekendDayText: {
    color: dimishedSnow,
  },
  dayButton: {
    alignItems: 'center',
    padding: 5,
    width: dayButtonWidth,
    borderTopWidth: 0,
    borderTopColor: Colors.transparent,
  },
  dayButtonFiller: {
    padding: 5,
    width: dayButtonWidth,
  },
  day: {
    ...Fonts.style.normal,
    color: Colors.snow,
    fontWeight: 'bold',
    fontFamily: Fonts.type.bold,
    textAlign: 'center',
    alignSelf: 'center',
  },
  eventIndicatorFiller: {
    marginTop: 3,
    borderColor: 'transparent',
    width: 4,
    height: 4,
    borderRadius: 2,
  },
  eventIndicator: {
    backgroundColor: Colors.fire
  },
  dayCircleFiller: {
    justifyContent: 'center',
    backgroundColor: 'transparent',
    width: dayButtonCircleWidth,
    height: dayButtonCircleWidth,
    borderRadius: dayButtonCircleRadius,
  },
  currentDayCircle: {
    backgroundColor: Colors.transparent,
    borderWidth: 2,
    borderColor: Colors.snow,
  },
  currentDayText: {
    color: Colors.snow,
  },
  selectedDayCircle: {
    backgroundColor: Colors.snow,
  },
  hasEventCircle: {
    backgroundColor: Colors.snow
  },
  hasEventDaySelectedCircle: {
  },
  hasEventText: {
    fontFamily: Fonts.type.bold,
    fontWeight: 'bold',
    color: Colors.amber,
  },
  selectedDayText: {
    fontFamily: Fonts.type.bold,
    fontWeight: 'bold',
    color: Colors.amber,
  },
})
