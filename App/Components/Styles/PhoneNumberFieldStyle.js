import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes'

export default {
  container: {
    flex: 1
  },
  ...ApplicationStyles.form,
  phoneInput: {
    height: 32
  }
}
