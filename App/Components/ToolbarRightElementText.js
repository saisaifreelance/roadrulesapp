import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity } from 'react-native'
import styles from './Styles/ToolbarRightElementTextStyle'

export default class ToolbarRightElementText extends Component {
  // // Prop type warnings
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    children: PropTypes.string.isRequired
  }

  // Defaults for props
  static defaultProps = {
    style: {},
    textStyle: {}
  }

  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={[styles.container, this.props.style]}>
        <Text style={[styles.textStyle, this.props.textStyle]}>
          {this.props.children}
        </Text>
      </TouchableOpacity>
    )
  }
}
