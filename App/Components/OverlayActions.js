import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/OverlayActionsStyle'
import { Colors, Fonts, Metrics } from '../Themes'

export default class OverlayActions extends Component {
  // Prop type warnings
  static propTypes = {
    actions: PropTypes.arrayOf(PropTypes.shape({
      label: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      onPress: PropTypes.func.isRequired,
    })).isRequired,
  }

  // Defaults for props
  static defaultProps = {
    actions: []
  }

  render () {
    const actions = this.props.actions.map((action, index) => {
      if (!index) {
        return (
          <TouchableOpacity onPress={action.onPress} style={[styles.action, styles.firstAction]} key={index}>
            <View style={styles.actionInner}>
              <Text style={styles.text}>{action.label}</Text>
              <Icon style={styles.icon} name={action.icon} size={Fonts.size.h4} color="red"/>
            </View>
          </TouchableOpacity>
        )
      }
      if (index === (this.props.actions.length - 1)) {
        return (
          <TouchableOpacity onPress={action.onPress} style={[styles.action, styles.lastAction]} key={index}>
            <Text style={styles.text}>{action.label}</Text>
            <Icon style={styles.icon} name={action.icon} size={Fonts.size.h4} color="red"/>
          </TouchableOpacity>
        )
      }
      return (
        <TouchableOpacity onPress={action.onPress} style={[styles.action]} key={index}>
          <View style={styles.actionInner}>
            <Text style={styles.text}>{action.label}</Text>
            <Icon style={styles.icon} name={action.icon} size={Fonts.size.h4} color="red"/>
          </View>
        </TouchableOpacity>
      )
    })
    return (
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          {actions}
        </View>
      </View>
    )
  }
}
