import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import MapView from 'react-native-maps'
import styles from './Styles/ModalMapStyle'
import { Metrics, Colors } from '../Themes'
import NavBar from '../Components/NavBar'
import MapListings from '../Components/MapListings'

export default class ModalMap extends Component {
  // Prop type warnings
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
    drivingSchools: PropTypes.func.isRequired,
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    const views = this.props.drivingSchools.map((drivingSchool, key) => {
      return (
        <MapView.Marker
          key={key}
          coordinate={drivingSchool.latlng}
          title={`US$${drivingSchool.cost}`}
          description={'marker.description'}
        />
      )
    })

    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: -17.836388,
            longitude: 31.041066,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        >{views}</MapView>
        <MapListings drivingSchools={this.props.drivingSchools} />
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.transparent,
            }
          }}
          containerStyle={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
          }}
          leftElement='close'
          onLeftElementPress={this.props.closeModal}
        />
      </View>
    )
  }
}
