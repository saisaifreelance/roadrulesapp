import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image } from 'react-native'
import styles from './Styles/NetworkImageStyle'

export default class NetworkImage extends Component {
  // Prop type warnings
  static propTypes = {
    url: PropTypes.string.isRequired,
    containerStyle: PropTypes.oneOfType([PropTypes.number, PropTypes.object]),
    ...Image.propTypes,
  }

  // Defaults for props
  static defaultProps = {
    someSetting: false
  }

  render () {
    return (
      <View style={[styles.container, this.props.containerStyle]}>
        <Image
          resizeMode='contain'
          {...this.props}
          source={require('../Images/logo.png')}
          style={[styles.image, this.props.style]}
        />
      </View>
    )
  }
}
