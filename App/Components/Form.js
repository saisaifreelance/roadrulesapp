import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/FormStyle'
import SelectField from './SelectField'
import TextField from './TextField'
import PhoneNumberField from './PhoneNumberField'
import Button from './Button'

export default class Form extends Component {
  // // Prop type warnings
  static propTypes = {
    fields: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      key: PropTypes.string.isRequired,
      type: PropTypes.oneOf(['text', 'select', 'phone_number', 'email', 'password', 'numeric']).isRequired,
      tableEditable: PropTypes.bool,
      formEditable: PropTypes.bool,
      required: PropTypes.bool,
      options: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
      }))
    })).isRequired,
    data: PropTypes.shape({
      value: PropTypes.string,
      error: PropTypes.string
    }).isRequired,
    onChangeField: PropTypes.func.isRequired,
    submitText: PropTypes.string.isRequired,
    legend: PropTypes.string.isRequired,
    error: PropTypes.string,
    clearError: PropTypes.func,
    onSubmit: PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmitEditing (field, index) {

  }

  onSubmit () {
    this.props.onSubmit(this.props.fields, this.props.data)
  }

  render () {
    const fields = this.props.fields.reduce((accummulator, field, index) => {
      if (!field.formEditable) {
        return accummulator
      }
      const onSubmitEditing = this.onSubmitEditing.bind(this, field, index)
      let error = ''
      let value = ''
      if (this.props.data[field.key] && typeof this.props.data[field.key] === 'object') {
        error = this.props.data[field.key].error ? this.props.data[field.key].error : ''
        value = this.props.data[field.key].value ? this.props.data[field.key].value : ''
      }
      switch (field.type) {
        case 'phone_number': {
          accummulator.push(
            <PhoneNumberField
              key={index}
              error={error}
              value={value}
              label={field.title}
              onChangePhoneNumber={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
            />
          )
          return accummulator
        }
        case 'select': {
          accummulator.push(
            <SelectField
              key={index}
              error={error}
              value={value}
              label={field.title}
              options={field.options}
              onValueChange={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
            />
          )
          return accummulator
        }
        case 'email': {
          accummulator.push(
            <TextField
              key={index}
              error={error}
              value={value}
              label={field.title}
              onChangeText={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
              textInputProps={{
                keyboardType: 'email-address'
              }}
            />
          )
          return accummulator
        }
        case 'password': {
          accummulator.push(
            <TextField
              key={index}
              error={error}
              value={value}
              label={field.title}
              onChangeText={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
              textInputProps={{
                secureTextEntry: true
              }}
            />
          )
          return accummulator
        }
        case 'numeric': {
          accummulator.push(
            <TextField
              key={index}
              error={error}
              value={value}
              label={field.title}
              onChangeText={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
              textInputProps={{
                keyboardType: 'numeric'
              }}
            />
          )
          return accummulator
        }
        case 'text':
        default: {
          accummulator.push(
            <TextField
              key={index}
              error={error}
              value={value}
              label={field.title}
              onChangeText={(value) => this.props.onChangeField(field.key, value)}
              onSubmitEditing={onSubmitEditing}
            />
          )
          return accummulator
        }
      }
    }, [])
    const error = this.props.error ? (
      <TouchableOpacity onPress={() => {
        if (this.props.clearError) {
          this.props.clearError()
        }
      }}>
        <Text style={styles.formError}>{this.props.error}</Text>
      </TouchableOpacity>
    ) : null
    const legend = this.props.legend ? (<Text style={styles.formLegend}>{this.props.legend}</Text>) : null
    return (
      <View style={styles.container}>
        {legend}
        {error}
        <View style={styles.form}>
          {fields}
        </View>
        <Button onPress={this.onSubmit}>{this.props.submitText}</Button>
      </View>
    )
  }
}
