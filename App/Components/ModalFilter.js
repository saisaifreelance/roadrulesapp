import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, ScrollView, RefreshControl, TouchableOpacity } from 'react-native'
import styles from './Styles/ModalFilterStyle'
import FilterOption from '../Components/FilterOption'

export default class ModalFilter extends Component {
  // Prop type warnings
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
  }

  // Defaults for props
  static defaultProps = {
    someSetting: false
  }

  render () {
    return (
      <View style={styles.mainContainer}>
        <TouchableOpacity style={styles.topContainer} onPress={this.props.closeModal}/>
        <View style={styles.content}>
          <View style={styles.topBar}>
            <View style={styles.topBarTextContainer}>
              <Text style={styles.topBarText}></Text>
            </View>
            <View style={styles.topBarTextContainerCenter}>
              <Text style={styles.topBarText}>300+ driving schools</Text>
            </View>
            <TouchableOpacity style={styles.topBarTextContainer}>
              <Text style={styles.topBarText}>clear</Text>
            </TouchableOpacity>
          </View>
          <ScrollView
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
            refreshControl={
              <RefreshControl
                style={styles.refreshControl}
                refreshing={false}
                onRefresh={this.props.closeModal}
              />
            }>
            <FilterOption type='check-box' title='Entire Place' description='Have a who place to yourself' checked={false} onChange={() => {}} />
            <FilterOption type='check-box' title='Private Room' description='Have your own room and share some common spaces' checked={true} onChange={() => {}} />
            <FilterOption type='radio' title='Private Room' description='Have your own room and share some common spaces' checked={true} onChange={() => {}} />
          </ScrollView>
        </View>
      </View>
    )
  }
}
