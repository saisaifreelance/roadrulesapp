import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/ListItemTrafficFineStyle'
import {Colors, Fonts} from '../Themes'
import {TrafficFine} from '../Types'
import MarkdownText from './MarkdownText'

export default class ListItemTrafficFine extends Component {
  // Prop type warnings
  static propTypes = {
    trafficFine: TrafficFine.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.bool,
    hovered: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
  }

  handlePress () {
    this.props.onPress(this.props.cardIndex, this.props.trafficFine.id)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex, this.props.trafficFine.id)
  }

  render () {
    const props = {
      style: [styles.container],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }
    let content = null
    let showMore = null
    let divider = null

    if (this.props.hovered) {
      content = (
        <MarkdownText>{this.props.trafficFine.text}</MarkdownText>
      )
    }
    if (this.props.selected) {
      divider = <View style={styles.divider} />
      content = (
        <MarkdownText>{this.props.trafficFine.text}</MarkdownText>
      )
    }
    if (!this.props.hovered && !this.props.selected) {
      showMore = (
        <View style={styles.hoverContent}>
          <Text style={styles.hoverContentText}>show explanation</Text>
        </View>
      )
    }
/*

    return (
      <TouchableOpacity {...props}>
        <Text style={{fontSize: Fonts.size.h5}}>
          <View style={{height: Fonts.size.h5, width: 80, backgroundColor: 'red'}}>
            <Text style={{fontSize: Fonts.size.h5}}>{this.props.trafficFine.description}</Text>
          </View>
          <Text style={{fontSize: Fonts.size.h5, backgroundColor: 'green'}}>{this.props.trafficFine.title}</Text>
        </Text>
        {divider}
        {showMore}
        {content}
      </TouchableOpacity>
    )
*/

    return (
      <TouchableOpacity {...props}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{this.props.trafficFine.title}</Text>
          <Text style={styles.fine}>{this.props.trafficFine.description}</Text>
        </View>
        {divider}
        {showMore}
        {content}
      </TouchableOpacity>
    )
  }
}
