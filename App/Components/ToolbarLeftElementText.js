import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import styles from './Styles/ToolbarLeftElementTextStyle'
import { Fonts, Colors } from '../Themes'

export default class ToolbarLeftElementText extends Component {
  // // Prop type warnings
  static propTypes = {
    onPress: PropTypes.func.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    children: PropTypes.string.isRequired,
    icon: PropTypes.string
  }

  // Defaults for props
  static defaultProps = {
    style: {},
    textStyle: {}
  }

  render () {
    let icon = null
    if (this.props.icon) {
      icon = (<Icon style={styles.icon} name={this.props.icon} size={Fonts.size.h2} color={Colors.blue} />)
    }
    return (
      <TouchableOpacity onPress={this.props.onPress} style={[styles.container, this.props.style]}>
        {icon}
        <Text style={[styles.text, this.props.textStyle]}>
          {this.props.children}
        </Text>
      </TouchableOpacity>
    )
  }
}
