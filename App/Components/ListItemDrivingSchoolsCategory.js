import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text, Image } from 'react-native'
import Button from './Button'
import MarkdownText from './MarkdownText'
import Communication from '../Services/Communication'
import { Colors, Images, Metrics } from '../Themes'
import styles from './Styles/ListItemDrivingSchoolsCategoryStyle'

const communication = Communication.create()

export default class ListItemDrivingSchoolsCategory extends Component {
  // Prop type warnings
  static propTypes = {
    category: PropTypes.object.isRequired,
    cardIndex: PropTypes.number.isRequired,
    selected: PropTypes.number,
    hovered: PropTypes.number,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }
  constructor (props) {
    super()
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.onContactPhoneCall = this.onContactPhoneCall.bind(this)
    this.onContactFacebook = this.onContactFacebook.bind(this)
    this.onContactWhatsApp = this.onContactWhatsApp.bind(this)
  }

  onContactPhoneCall () {
    const numberKey = Object.keys(this.props.category.contact.phone)[0]
    const number = this.props.category.contact.phone[numberKey].value
    communication
      .call({
        number,
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  onContactFacebook () {
    const messengerKey = Object.keys(this.props.category.contact.messenger)[0]
    const messenger = this.props.category.contact.messenger[messengerKey].value
    communication
      .messenger({
        messenger,
        message: 'Hi, I\'m a Road Rules App User, I\'d like to make an enquiry about your driving school',
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  onContactWhatsApp () {
    const whatsappKey = Object.keys(this.props.category.contact.whatsapp)[0]
    const whatsapp = this.props.category.contact.phone[whatsappKey].value
    communication
      .whatsapp({
        whatsapp,
        message: 'Hi, I\'m a Road Rules App User, I\'d like to make an enquiry about your driving school',
        prompt: false
      })
      .then((resp) => {
      })
      .catch((e) => {
      })
  }

  handlePress () {
    this.props.onPress(this.props.cardIndex)
  }

  handleLongPress () {
    this.props.onLongPress(this.props.cardIndex)
  }

  render () {
    const { category } = this.props
    const cardStyles = {}
    let hoverContent = null
    let faceContent = null
    let selectedContent = null
    let title = null
    let description = null
    let divider = null
    let text = null
    let image = null
    let props = {
      style: [styles.container, this.props.style, cardStyles],
      shadowColor: Colors.charcoal,
      shadowOffset: {width: 4, height: 4},
      shadowOpacity: 0.5,
      shadowRadius: 4,
      onPress: this.handlePress,
      onLongPress: this.handleLongPress
    }

    if (category.title) {
      title = (
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{category.title}</Text>
        </View>
      )
    }

    if (category.description) {
      description = (
        <View style={styles.fineContainer}>
          <Text style={styles.fine}>{category.description}</Text>
        </View>
      )
    }

    if (category.image) {
      image = (
        <View style={styles.titleImageContainer}>
          <Image
            style={styles.titleImage}
            source={{uri: 'image_22'}}
            resizeMode='contain'
          />
        </View>
      )
    }

    if (this.props.selected === this.props.cardIndex) {
      props.disabled = true

      if (category.text) {
        text = (<MarkdownText>{category.text}</MarkdownText>)
      }
      selectedContent = (
        <View style={styles.selectedContent}>
          <Button
            icon={Images.call}
            color={Colors.snow}
            backgroundColor={Colors.bloodOrange}
            style={{marginTop: Metrics.baseMargin}}
            onPress={this.onContactPhoneCall}>Phonecall</Button>

          <Button
            icon={Images.whatsapp}
            color={Colors.snow}
            backgroundColor={Colors.whatsapp}
            style={{marginTop: Metrics.baseMargin}}
            onPress={this.onContactWhatsApp}>WhatsApp</Button>

          <Button
            icon={Images.facebook}
            color={Colors.snow}
            backgroundColor={Colors.facebook}
            style={{marginTop: Metrics.baseMargin}}
            onPress={this.onContactFacebook}>Facebook</Button>
        </View>
      )
      faceContent = null
    }

    if (this.props.hovered === this.props.cardIndex) {
      const categoryText = `${category.text.slice(0, 160)}...`
      if (category.text) {
        text = (<MarkdownText>{categoryText}</MarkdownText>)
      }
    }

    if ((this.props.selected !== this.props.cardIndex) && typeof this.props.selected === 'number') {
      cardStyles.height = 0
      cardStyles.overflow = 'hidden'
      hoverContent = null
      faceContent = null
      selectedContent = null
      title = null
      text = null
      props = {
        disabled: true
      }
    }

    if (description || text || hoverContent) {
      divider = <View style={styles.divider} />
    }

    if (description || text || hoverContent) {
      divider = <View style={styles.divider} />
    }

    return (
      <View>
        <TouchableOpacity {...props}>
          {image}
          {title}
          {divider}
          {description}
          {text}
          {hoverContent}
          {faceContent}
        </TouchableOpacity>
        {selectedContent}
      </View>
    )
  }
}
