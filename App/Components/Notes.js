import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native'
import { ModalResolve } from './Modal'
import Button from './Button'
import { Colors, Fonts } from '../Themes'
import Mascot from './Mascot'
// import styles from './Styles/NotesStyle'

export default class Notes extends Component {
  // // Prop type warnings
  // static propTypes = {
  //   someProperty: PropTypes.object,
  //   someSetting: PropTypes.bool.isRequired,
  // }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <View style={styles.container}>
        <View style={{flex: 1, justifyContent: 'center'}}>
          <View style={{
            marginVertical: 8,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row'
          }}>
            <Mascot />
          </View>
          <Text style={styles.instruction}>Hi, you can study all the notes you need for the provisional driver's license test on this section.</Text>

        </View>
        <View style={styles.buttons}>
          <Button style={styles.button} onPress={this.props.startNotes}>start</Button>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  title: {
    fontSize: Fonts.size.h6,
    textAlign: 'center',
    marginHorizontal: 16,
    marginVertical: 16,
  },
  splash_image: {
    width: 300,
    height: 300,
    marginHorizontal: 16,
    marginVertical: 8,
    flex: 1,
  },
  instruction: {
    fontSize: Fonts.size.h6,
    textAlign: 'center',
    marginHorizontal: 16,
    marginVertical: 8,
    color: 'black'
  },
  buttons: {
    flexDirection: 'row',
    padding: 8,
  },
  button: {
    flex: 1,
    margin: 8,
  },
  sub_instruction: {
    fontSize: Fonts.size.regular,
    textAlign: 'center',
    marginHorizontal: 16,
    marginBottom: 16,
    marginTop: 8,
  },
})
