import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import Calendar from '../Components/Calendar'
import styles, { customStyle } from './Styles/ModalTimeStyle'
import NavBar from '../Components/NavBar'
import Button from '../Components/Button'
import ModalNavbarRightTextElement from '../Components/ModalNavbarRightTextElement'
import { Metrics, Colors } from '../Themes'

export default class ModalTime extends Component {
  // Prop type warnings
  static propTypes = {
    closeModal: PropTypes.func.isRequired,
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  constructor (props) {
    super(props)
    const now = new Date()
    const date = new Date(now.getFullYear(), now.getMonth(), 1)
    let month = `${date.getMonth() + 1}`
    if (month.length === 1) {
      month = `0${month}`
    }
    let day = `${now.getDate()}`
    if (day.length === 1) {
      day = `0${day}`
    }
    this.state = {
      date: `${date.getFullYear()}-${month}-${day}`,
      selectedDays: [],
      fullyBookedDays: [],
      unavailableDays: [],
    }
  }

  componentDidMount () {
  }

  render () {
    return (
      <View style={styles.container}>
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.transparent,
            }
          }}
          leftElement='close'
          rightElement={(<ModalNavbarRightTextElement onPress={() => {}}>Clear</ModalNavbarRightTextElement>)}
          onLeftElementPress={this.props.closeModal}
          onRightElementPress={this.props.closeModal}
        />
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Select Booking Time</Text>
        </View>
        <Calendar
          horizontal={false}
          currentMonth={this.state.date}       // Optional date to set the currently displayed month after initialization
          customStyle={customStyle} // Customize any pre-defined styles
          dayHeadings={['S', 'M', 'T', 'W', 'T', 'F', 'S']}               // Default: ['S', 'M', 'T', 'W', 'T', 'F', 'S']
          eventDates={['2017-08-09', '2017-08-10']}       // Optional array of moment() parseable dates that will show an event indicator
          // events={['2017-09-09', '2017-09-10']}// Optional array of event objects with a date property and custom styles for the event indicator
          // monthNames={Array}                // Defaults to english names of months
          // nextButtonText={'Next'}           // Text for next button. Default: 'Next'
          // onDateSelect={(date) => this.onDateSelect(date)} // Callback after date selection
          // onSwipeNext={this.onSwipeNext}    // Callback for forward swipe event
          // onSwipePrev={this.onSwipePrev}    // Callback for back swipe event
          // onTouchNext={this.onTouchNext}    // Callback for next touch event
          // onTouchPrev={this.onTouchPrev}    // Callback for prev touch event
          // onTitlePress={this.onTitlePress}  // Callback on title press
          // prevButtonText={'Prev'}           // Text for previous button. Default: 'Prev'
          removeClippedSubviews={false}     // Set to false for us within Modals. Default: true
          // renderDay={<CustomDay />}         // Optionally render a custom day component
          scrollEnabled={true}              // False disables swiping. Default: False
          // selectedDate={'2017-10-11'}       // Day to be selected
          showControls={false}               // False hides prev/next buttons. Default: False
          showEventIndicators={true}        // False hides event indicators. Default:False
          startDate={this.state.date}          // The first month that will display. Default: current month
          titleFormat={'MMMM YYYY'}         // Format for displaying current month. Default: 'MMMM YYYY'
          weekStart={1} // Day on which week starts 0 - Sunday, 1 - Monday, 2 - Tuesday, etc, Default: 1
        />
        <View style={styles.footer}>
          <Button style={styles.button} backgroundColor={Colors.lightSnow}>Save</Button>
        </View>
      </View>
    )
  }
}
