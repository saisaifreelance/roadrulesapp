import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, View, Text, ViewPropTypes } from 'react-native'
import { View as AnimatedView } from 'react-native-animatable'
import styles from './Styles/TabBarStyles'

const Button = (props) => {
  return (
    <TouchableOpacity {...props}>
      {props.children}
    </TouchableOpacity>
  )
}

export default class TabBar extends Component {
  // Prop type warnings
  static propTypes = {
    onIndexChange: React.PropTypes.func,
    index: React.PropTypes.number,
    tabs: React.PropTypes.array,
    containerWidth: React.PropTypes.number,

    backgroundColor: React.PropTypes.string,
    activeTextColor: React.PropTypes.string,
    inactiveTextColor: React.PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: ViewPropTypes.style,
    renderTab: React.PropTypes.func,
    underlineStyle: ViewPropTypes.style,
  }

  static defaultProps = {
    index: 0,
    tabs: ['one', 'two', 'three'],
    onIndexChange: () => {},
    backgroundColor: 'red',
    activeTextColor: 'blue',
    inactiveTextColor: 'green',
    textStyle: {},
    tabStyle: {},
    underlineStyle: {},
  }

  constructor (props) {
    super(props)
    this.renderTab = this.renderTab.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.index !== this.props.index) {

    }
  }

  renderTab (name, index, isTabActive, onPressHandler) {
    const {activeTextColor, inactiveTextColor, textStyle,} = this.props
    const textColor = isTabActive ? activeTextColor : inactiveTextColor
    const fontWeight = isTabActive ? 'bold' : 'normal'

    return (
      <Button
        style={styles.flexOne}
        key={name}
        accessible={true}
        accessibilityLabel={name}
        accessibilityTraits='button'
        onPress={() => onPressHandler(index)}
      >
        <View style={[styles.tab, this.props.tabStyle,]}>
          <Text style={[{color: textColor, fontWeight,}, textStyle,]}>
            {name}
          </Text>
        </View>
      </Button>
    )
  }

  render () {
    const containerWidth = this.props.containerWidth
    const numberOfTabs = this.props.tabs.length
    const tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 4,
      backgroundColor: 'navy',
      bottom: 0,
      left: (containerWidth / numberOfTabs) * this.props.index
    }
    const buttons = this.props.tabs.map((name, currentIndex) => {
      const isTabActive = this.props.index === currentIndex
      const renderTab = this.props.renderTab || this.renderTab
      return renderTab(name, currentIndex, isTabActive, this.props.onIndexChange)
    })
    return (
      <View style={[styles.tabs, {backgroundColor: this.props.backgroundColor}, this.props.style,]}>
        {buttons}
        <AnimatedView transition="left" style={[tabUnderlineStyle, this.props.underlineStyle]}/>
      </View>
    )
  }
}
