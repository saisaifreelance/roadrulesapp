import React, { Component } from 'react'
// import PropTypes from 'prop-types';
import { StackNavigator } from 'react-navigation'
import { View, Text, ActivityIndicator } from 'react-native'
import styles from './Styles/ModalStyle'
import IconButton from './IconButton'
import ModalConnect from './ModalConnect'
import ModalMenu from './ModalMenu'
import ModalPay from './ModalPay'
import ModalNotification from './ModalNotification'
import ModalRate from './ModalRate'
import ModalResolve from './ModalResolve'
import { Colors } from '../Themes'

class Modal extends Component {
  render () {
    return (
      <View style={styles.modalContainer}>
        <View style={styles.container}>
          <ActivityIndicator animating size='large' color={Colors.amber}/>
          <Text style={styles.title}>Loading Modal...</Text>
        </View>
      </View>
    )
  }
}

export default StackNavigator({
  Modal: {
    screen: Modal
  },
  ModalConnect: {
    screen: (props) => (<ModalConnect {...props} {...props.navigation.state.params} />)
  },
  ModalMenu: {
    screen: (props) => (<ModalMenu {...props} {...props.navigation.state.params} />)
  },
  ModalPay: {
    screen: (props) => (<ModalPay {...props} {...props.navigation.state.params} />)
  },
  ModalNotification: {
    screen: (props) => (<ModalNotification {...props} {...props.navigation.state.params} />)
  },
  ModalRate: {
    screen: (props) => (<ModalRate {...props} {...props.navigation.state.params} />)
  },
  ModalResolve: {
    screen: (props) => (<ModalResolve {...props} {...props.navigation.state.params} />)
  }
}, {
  cardStyle: {
    opacity: 1,
    backgroundColor: 'rgba(0,0,0,0.1)'
  },
  initialRouteName: 'ModalMenu',
  headerMode: 'none',
  // Keeping this here for future when we can make
  navigationOptions: {
    header: {
      left: (
        <IconButton
          icon='close'
          onPress={() => window.alert('pop')}
        />
      ),
      style: {
        backgroundColor: 'green'
      }
    }
  }
})
