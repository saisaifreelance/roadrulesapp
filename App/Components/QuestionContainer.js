import React, {Component} from 'react'
var PropTypes = require('prop-types');
import {
  View,
  Image,
  StyleSheet,
  Animated,
  PanResponder,
  Alert,
  Text
} from 'react-native'
import {Colors, Fonts, Metrics} from '../Themes'
import { Checkbox } from 'react-native-material-ui'
import { View as AnimatedView } from 'react-native-animatable'
import Button from './Button'
import IconButton from './IconButton'
import { ModalConnect } from './Modal'

const option_letters = [
  {
    option: 'option_1',
    letter: 'A'
  },
  {
    option: 'option_2',
    letter: 'B'
  },
  {
    option: 'option_3',
    letter: 'C'
  },
]

const modal_responses = {
  correct: "Correct!",
  incorrect: "Oops, you got that one wrong.",
}

const Answer = (props) => {
  const {text, is_selected, onSelect, option, correct_option, response} = props
  let checkbox = null
  let backgroundColor = Colors.snow

  // if (!response) {
  checkbox = (
    <View>
      <Checkbox
        checked={is_selected}
        onCheck={() => {}}
        style={checkboxStyle} />
    </View>
  )
  // }

  if (response === option && response !== correct_option) {
    backgroundColor = Colors.red
  }

  return (
    <Button
      onPress={onSelect}
      backgroundColor={backgroundColor}
      rippleColor={'rgba(255,255,0, 0.5)'}
      after={checkbox}
      style={styles.answer}
      textStyle={{color: Colors.panther, ...Fonts.style.h6, flex: 1, textAlign: 'left'}}>
      {text}
    </Button>
  )
}

const Question = (props) => {
  const {text, image: uri} = props.question

  let question_image = null
  if (uri) {
    question_image = (<Image source={{uri}} style={styles.question_image} resizeMode={"contain"}/>)
  }

  return (
    <View style={styles.question}>
      <Text style={styles.question_text}>{text}</Text>
      {question_image}
    </View>
  )
}

const Explanation = ({question, correct_answer, explanation, show_explanation, toggleShowExplanation, flagQuestion, selected, icon_close, icon_open}) => {

  return (
    <View
      style={[styles.modal, {backgroundColor: selected === question.correct_option ? Colors.green : Colors.red,}]}>
      <View style={styles.modal_row}>
        <View style={{flex: 1}}>
          <Text style={styles.modal_title}>
            {selected === question.correct_option ? modal_responses.correct : modal_responses.incorrect}
          </Text>
          <Text style={styles.modal_answer}>{correct_answer}</Text>
        </View>
        <View>
          <IconButton
            icon={show_explanation ? icon_close : icon_open}
            backgroundColor={'rgba(0,0,0,0.2)'}
            style={styles.modal_icon}
            onPress={toggleShowExplanation}/>
          <IconButton
            icon="flag"
            backgroundColor={'rgba(0,0,0,0.2)'}
            style={styles.modal_icon}
            onPress={flagQuestion}/>
        </View>
      </View>
      {show_explanation ? (
        <View style={{backgroundColor: Colors.snow, padding: 8, borderRadius: 4,}}>
          <Text style={[styles.modal_title, {color: Colors.panther}]}>Explanation</Text>
          <Text style={[styles.modal_answer, {color: Colors.charcoal}]}>{question.explanation}</Text>
        </View>
      ) : null}
    </View>
  )
}

Explanation.defaultProps = {
  icon_close: "cancel",
  icon_open: "info"
}

class QuestionContainer extends Component {
  constructor (props) {
    super(props)
    const { question } = props
    this.state = {
      explanation: false,
      modalY: new Animated.ValueXY(),
      modalOpacity: 1,
    }

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => {
        // alert('start')
        return true
      },
      onPanResponderMove: Animated.event([null, {
        // dx: this.state.modalY.x,
        dy: this.state.modalY.y
      }]),
      onPanResponderGrant: () => {
        this.setState({modalOpacity: 0.6})
      },
      onPanResponderRelease: (e, gesture) => {
        this.setState({modalOpacity: 1})
      },
      onPanResponderTerminate: (e, gesture) => {
        this.setState({modalOpacity: 1})
      }
    })
  }

  componentDidUpdate (prevProps) {
    if (this.state.explanation && prevProps.question !== this.props.question) {
      this.setState({explanation: false})
    }
  }

  render () {
    const {question, selected, isReview, explanationProps} = this.props
    let correct_answer = null

    const answers = option_letters.map((option) => {
      const text = option.letter + '. ' + question[option.option]
      if (question.correct_option === option.option)
        correct_answer = text
      return (
        <Answer
          text={text}
          onSelect={this.onSelect.bind(this, option.option)}
          key={option.option}
          is_selected={selected === option.option}
          option={option.option}
          response={question.response}
          correct_option={question.correct_option}/>)
    })

    let modal = null

    if (this.props.modal) {
      modal = (
        <View style={styles.modal_container}>
          <AnimatedView
            animation='bounceIn'
            duration={200}
            {...this.panResponder.panHandlers}
            style={[this.state.modalY.getLayout(), {opacity: this.state.modalOpacity}]}>
            <Explanation
              question={question}
              selected={selected}
              correct_answer={correct_answer}
              show_explanation={this.state.explanation}
              toggleShowExplanation={() => this.setState({explanation: !this.state.explanation})}
              flagQuestion={this.flagQuestion.bind(this)}/>
          </AnimatedView>
        </View>
      )
    }

    if (isReview) {
      return (
        <View style={styles.container}>
          <Question question={question} />
          <Explanation
            question={question}
            selected={selected}
            correct_answer={correct_answer}
            show_explanation={true}
            toggleShowExplanation={() => this.setState({explanation: !this.state.explanation})}
            flagQuestion={this.flagQuestion.bind(this)}
            {...explanationProps}
          />
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <Question question={question}/>
        <View style={{padding: 8}}>
          {answers}
        </View>
        {modal}
      </View>
    )
  }

  onSelect (option) {
    this.props.onSelect(option)
  }

  flagQuestion () {
    const {openModal, question} = this.props
    Alert.alert("Report Question", "Have you found a problem with this question and would like to report?", [
        {
          text: 'YES',
          onPress: openModal.bind(undefined, question)
        },
        {text: 'NO', style: 'cancel'}
      ],
      {cancellable: true})
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.snow
  },
  question: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: Metrics.baseMargin
  },
  question_text: {
    textAlign: 'center',
    fontSize: Fonts.size.h6,
    color: Colors.panther,
    marginVertical: 8,

  },
  question_image: {
    width: Metrics.screenWidth,
    height: undefined,
    flex: 1
  },
  answer: {
    margin: 8,
    padding: 8,
    borderRadius: 8,
    borderColor: 'rgba(0,0,0,0.2)',
    borderWidth: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },
  answer_text: {
    fontSize: Fonts.size.regular,
    flex: 1,
  },
  modal_container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0)',
    justifyContent: 'center',
  },
  modal: {
    backgroundColor: Colors.green,
    padding: 16,
    elevation: 1,
  },
  modal_icon: {
    marginBottom: 8,
  },
  modal_row: {
    flexDirection: 'row'
  },
  modal_title: {
    fontSize: Fonts.size.h6,
    color: Colors.snow
  },
  modal_answer: {
    fontSize: Fonts.size.regular,
    color: Colors.snow
  }
})

const checkboxStyle = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    fontSize: 100,
    color: Colors.panther
  },
  label: {
    marginLeft: 0,
    color: Colors.panther
  }
})

QuestionContainer.propTypes = {
  question: PropTypes.object.isRequired,
  selected: PropTypes.oneOf(['option_1', 'option_2', 'option_3',]),
  onSelect: PropTypes.func.isRequired,
  modal: PropTypes.bool.isRequired,
  openModal: PropTypes.func.isRequired,
}

QuestionContainer.defaultProps = {
  modal: false,
  explanationProps: {},
}

export default QuestionContainer
