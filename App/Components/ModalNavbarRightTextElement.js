import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TouchableOpacity, Text } from 'react-native'
import styles from './Styles/ModalNavbarRightTextElementStyle'

export default class ModalNavbarRightTextElement extends Component {
  // Prop type warnings
  static propTypes = {
    children: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
  }

  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <TouchableOpacity onPress={this.props.onPress} {...this.props} style={styles.toolbarTextContainer}>
        <Text style={styles.toolbarText}>{this.props.children}</Text>
      </TouchableOpacity>
    )
  }
}
