import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from 'react-native'
import { Checkbox, RadioButton } from 'react-native-material-ui'
import styles, {controlStyles} from './Styles/FilterOptionStyle'

export default class FilterOption extends Component {
  // Prop type warnings
  static propTypes = {
    type: PropTypes.oneOf(['check-box', 'radio']).isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }

  // Defaults for props
  static defaultProps = {
    value: 1
  }

  render () {
    let control = null
    switch (this.props.type) {
      case 'check-box':
        control = <Checkbox value={this.props.value} checked={this.props.checked} onCheck={this.props.onChange} style={controlStyles} />
        break
      case 'radio':
        control = (
          <Checkbox
            checkedIcon='radio-button-checked'
            uncheckedIcon='radio-button-unchecked'
            value={this.props.value}
            checked={this.props.checked}
            onCheck={this.props.onChange}
            style={controlStyles} />
        )
        break
      default:
    }
    return (
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.description}>{this.props.description}</Text>
        </View>
        <View style={styles.controlContainer}>
          {control}
        </View>
      </View>
    )
  }
}
