export default `||** INTRODUCTION TO DRIVING**

Road traffic safety issues are treated, and rightly so, as a matter of life-and-death! Driving is more than the movement of a vehicle from point A to point B. 

Driving does not only necessarily imply being in control of the steering apparatus of a vehicle, it includes a variety of deliberations which involve cooperation; rational and very clear communication with fellow road users. 

Therefore, thorough knowledge and respect of road regulations, traffic signs and traffic signals are an **absolute** pre-requisite. Any aspiring and responsible driver **MUST** invest a lot of honest, dedicated hard work in mastering these pre-requisites and should be willing to have their competence put to scrutiny. 

Zimbabwe’s Highway Code (our road traffic rule book) rightly states that "…..a vehicle is a good means of transport but a dangerous weapon in the hands of reckless people." An unlicensed driver is evidently in the class of the **reckless**, and **is a potential mass-murderer**.

To drive in Zimbabwe you must be in possession of a legally acquired Zimbabwean or other recognized driver’s license. In Zimbabwe, there is no other accepted qualification for driving other than the driver documents issued through the competent and prescribed Authorities which primarily are the Vehicle Inspectorate Department (VID) and the Central Vehicle Registry (CVR).

If you do not have a legally recognized driver’s license, you must take out a learner driver’s license which allows you to drive on public roads to gain experience before undergoing the test for your full driver’s license. The learner’s license is only issued after you have passed a test on the rules of the road and the traffic signs and traffic signals.

A learner’s license is used if and only you are accompanied by a holder of a certificate of competency as the minimum document.

**NOTE:**   A public service vehicle **CAN’T** be driven using a learner’s license.

||**LEARNER DRIVER’S LICENSE TEST **

A provisional driver’s license proves that the holder has acquired thorough knowledge of road regulations, signs and signals and has been authorised by the government to practically learn the skills and habits of manoeuvring a vehicle in conjunction with knowledge acquired. It proves and signifies that the holder of the license has gone through the driving examination and satisfactorily passed it.

The test is administered by the Vehicle Inspectorate Department (VID) of the Ministry of Transport and Infrastructure Development of the Government of Zimbabwe at a cost of **US$20.00** per person per sitting. The fee is non-refundable even if your book for the test and do not show up to take the test and neither is it rolled over for you to write the test on the next available date.

It’s a multiple choice questions test with twenty five (25) questions that are supposed to be answered within eight (8) minutes. The minimum score regarded as a pass mark is  twenty two (22) out of the twenty five (25) test questions, i.e. 88 % for those who wish to get a driver’s license for Classes Two (2), Three(3), Four(4) and Five (5). 

For Class One (1), one must score 100% for this test, that is, they must get **ALL (25) questions correct!** For the Class One (1) Provisional License, one must hold a driver’s license which is five (5) years old, a Defensive Driving Certificate (DDC) and a special medical certificate.

Before one goes to sit for the provisional driver’s license test, there is great need for one to study the Highway Code to gain a deep understating of the Zimbabwean driving rules. There is also a special need to undertake practice exam questions to prepare specifically for this theory test.

When going through the two materials, (Highway Code & Practice Questions) one becomes accustomed to the general road rules and gets insight on how to overcome driving situations that will be presented and tested in the exam.

**According to VID 2014 statistics about 60% **of the people who sit for this exam **fail the 1****st**** time** and most people pass well after the 2nd sitting. It is therefore very important to thoroughly prepare for this test. It is for this purpose that we have created this app to help you in your noble efforts to become a legal, responsible and safe driver on Zimbabwe’s roads.

**Minimum Requirements for the Provisional Driver’s License Test.**

1. 2 x (25 x 30)mm black and white driver’s license photos

2. Every driver’s license held by applicant. (Class 1 exclusive)

3. Documentary proof of identity (Valid and **NOT Expired** Passport / National ID)

4. Exam fee of US$20.00

||**TEST SYLLABUS**

The Zimbabwe provisional driver’s license test curriculum consists of the following areas;

1) 	Rules of the road (general road rules).

2) 	The different Classes of vehicles in Zimbabwe.

3) 	Documents required for the driver in Zimbabwe (Public Transport Driver and General Driver).

4) 	Documents required for vehicles and specifications (Public Transport Driver and General Driver).

5) 	Driving Requisites in Zimbabwe.

* Prestart checks and vehicle inspection

* Lane use, Speed Limits

* Night driving, Overtaking

* Traffic signs and signals

* Hand signals

* Reasoning and day to day practical driving situations.

6) 	Hazardous Conditions

* Turning left or right at robot (traffic light)controlled intersections.

* Parking vehicle/stopping vehicle outside theroad.

* Driving on an upgrade or downgrade

* Driving under the influence of alcohol

* Bad weather and Road Conditions

7)	The Highway Code, General Knowledge of the application of the Highway Code for all vehicles, Zimbabwe Traffic Legislation

||** ZIMBABWE’S ROAD RULES**

There are four fundamental rules of the road covered in the provisional license test.

**Rule # 1**: 

The Zimbabwean rule of the road states that vehicles should keep well left and give way to traffic on your right hand side.

**Diagram #1**

![image alt text](image_0.jpg)

**Car B** goes 1st.

According to (**Rule #1**) "give way to traffic approaching from the road on your right"

**Car A** gives way to **Car B**, therefore **Car B** goes 1st because he has no traffic on his right.

**Rule # 2**:

Never turn right in front of oncoming traffic.

**Diagram # 2**

![image alt text](image_1.jpg)

According to (**Rule #1**) "give way to traffic approaching from the road on your right"

**Car C** goes 1st, **Car B** goes 2nd and **Car A** goes last

**Car A** gives way to **Car B** and **Car B** gives way to **Car C. Car C** should go 1st**provided he is not **turning right in-front of oncoming traffic. If a vehicle with traffic on its right hand side does not have the intention of turning right in front of oncoming traffic, it can proceed straight ahead. **Car B** goes 2nd since he has no traffic on his right after **Car C** has gone. Thereafter **Car A** finally goes last. 

**Rule # 3**: 

In rural areas give way to traffic that has entered the intersection first.

**Diagram # 3**

![image alt text](image_2.jpg)

**Car C** has nothing on its right but intends to turn right in front of oncoming traffic. **Car C** should not break **Road Rule # 2** "never turn right in front of oncoming traffic". 

Therefore **Car A **which is classified as "oncoming traffic" goes 1st, then **Car C** can follow and finally **Car B** goes last.

**Frequently Asked Question by Students on This Diagram**

Why does **Car A** go 1st and yet it has **Car B** on its right hand side if we always say "give way to traffic on your right hand side?

**The Explanation**

**Car C **gives **Car A **the right to go first so that he (**Car C**) can be able to turn right without him (**Car C**) breaking the law (Road Rule # 2 which says "never turn right in front of oncoming traffic". By so doing **Car C** moves to the middle of the road and blocks **Car B** as **Car A** is passing.

After **Car A** has passed, **Car C** turns right and lastly **Car B** then turns right lastly.

**Rule # 4**: 

At traffic circles or roundabouts, give way to traffic already circulating.

**General Notes:**

* When reversing, a seat belt is not necessary.

* Vehicle being driven should display L - Plates at the front and rear of the vehicle when approaching vehicle displaying L Plates extreme caution.

**Controlled**** Intersections**

The general rule is that **the vehicle facing a control sign to its left side should stop or give way to all crossing traffic.**

**Diagram # 4**

![image alt text](image_3.jpg)

Under normal circumstances (in the absence of the control sign), **Car A** should give way to **Car B** because **Car B** is to his right. 

However, in the diagram above **Car B** has been controlled / regulated by the presence of that sign, therefore **Car A** goes first. The sign supersedes the rule of giving way to traffic on your right.

||** VEHICLE CLASSES**

Having understood the rules on Zimbabwe’s roads, it is equally important that one makes a decision on which class of vehicle does one wish to attain a driver’s license for.

**There are five (5) Classes of vehicle licenses in Zimbabwe**

<table>
  <tr>
    <td>License Class</td>
    <td>Vehicle Type</td>
    <td>Minimum Age</td>
    <td>Other Requirements</td>
  </tr>
  <tr>
    <td>


Class 1</td>
    <td>


Buses, 
Commuter Omnibus, Train, Trailer Bus (Artico)</td>
    <td>


25 years</td>
    <td>
Should have a Medical Certificate & should already have a License and a Defensive Driving Certificate
</td>
  </tr>
  <tr>
    <td>
Class 2
</td>
    <td>
Heavy Vehicles, Heavy Trucks,  Ambulance, Goods Vehicles, Hearse, Breakdown Vehicles (Tow Trucks)
</td>
    <td>
18 years</td>
    <td></td>
  </tr>
  <tr>
    <td>
Class 3
</td>
    <td>
Motor Cycles, 
Motor Bikes, Scooters, Vasolex</td>
    <td>
16 years</td>
    <td></td>
  </tr>
  <tr>
    <td>
Class 4
</td>
    <td>
Light Motor Vehicles,</td>
    <td>
16 years</td>
    <td></td>
  </tr>
  <tr>
    <td>

Class 5</td>
    <td>
Farming Equipment & Construction Vehicles which do not qualify to be classified as class 1, 2, 3 or 4 vehicles.
</td>
    <td>

18 years</td>
    <td></td>
  </tr>
</table>


Holders of a provisional license can only attempt Class One (1) and Class Two (2) at the Age of 18 and above. A Class One (1) driver’s license is for people with **a driver’s license already, a medical certificate and a defensive driving certificate.**

||** VEHICLE DOCUMENTATION**

Different types or classes of vehicles have different type of road documentation required of them. The following are the two common types of vehicle classes.

* **General  Motor Vehicle **

* **Public Transportation Vehicle**

It is **MANDATORY** that a vehicle be licensed before advancing onto the road. Driving an unlicensed vehicle is an offence.

**General Motor Vehicle**

1) Vehicle registration book

2) Vehicle insurance (4 months)

3) Vehicle license (4 months)

The above are the minimum license and insurance terms required by law even though the owner has an option to license and insure their vehicle for longer terms.

**Public Transportation Vehicle**

Public transportation vehicles require all of the above **plus a certificate of fitness and a route authority certificate**. A certificate of fitness is granted to a vehicle that proves to be roadworthy.

4) Certificate of fitness (6 months)

For buses the certificate of fitness is valid for six (6) months and for general vehicles it is valid for a period not exceeding one year (12) months.

5) Route Authority for passenger transport.

||** DRIVER’S LICENSE CLASSES**

The available combinations of Driver’s License Classes in Zimbabwe are as follows;

* Class 3 and 5

**Note; **

You are not given a class (5) driver’s license if you are below the age of eighteen (18) even if you have qualified for it, its withheld and endorsed later on the disc when you turn 18.

* Cass 4 and 5

* Class 2, 4 and 5

* Class 1, 2, 4 and 5

When a person is issued with a class two (2) or class four (4) license, class five (5) is offered freely. When issued with a class two (2) license, classes four (4) and five (5) are issued freely. Class one (1) is offered to **ALREADY** licensed drivers only.

A holder of a class four (4) license should have a minimum of five (5) years driving experience to attempt for class one (1). A holder of class two (2) license should have a minimum of a year driving experience to attempt for class one (1). 

However there is a legislative amendment of 2006 that stipulates a (5) year driving experience requirement for all classes which wishes to attempt for the class one (1) license.

||** REQUIRED DOCUMENTATION **

Different types or classes of vehicles have different type of documents required of them. The following are the two common types of vehicle classes.

* General  Motor Vehicle Driver

* Public Transportation Vehicle Driver

**General Motor Vehicle Driver**

A provisional / learner driver’s license or a full driver’s license. Holders of a provisional driver’s license **MUST** to be accompanied by person with a driver’s license and are **NOT **allowed to carry passengers.

**Public Transportation Vehicle Driver **

The minimum age for a public vehicle driver is twenty five (25) years. Public vehicle drivers are required to have;

* Driver's license

* Medical Certificate valid for 12 months

* Defensive Drivers Certificate (DDC) valid for 48 months.

Persons driving a public vehicle should have their license renewed after every five (5) years. Holders of a provisional driver’s license are **NOT** allowed to drive public transportation vehicles.

**Tractor Drivers**

Tractor drivers are to produce a tractor’s permit when driving.

A **registered** farm owner or registered mine owner can apply for a tractor’s permit for his or her employee for the purposes of driving the tractor within the farm during the purpose of use within the farm and execution of his duties.

This kind of permit works within 10 km from the farm boundaries that is 10 km radius from the farm perimeter. The permit only works for that particular employer and it **DOES NOT **work in areas under the jurisdiction of local authority or municipal council. 

||**PERSONS WITH DIASABILITIES**

Having a disability does not preclude one from obtaining a driver’s license in Zimbabwe. If a person living with a disability intends to sit for the provisional driver’s license test, they should first see a VID approved medical practitioner who will complete a medical form for them. The medical form is obtained from the VID offices.

The medical practitioner will assess the condition of the person and determine whether or not they are fit to drive a motor vehicle and if they are fit to drive, they endorse the conditions under which the person can drive.

Many optical problems or challenges do not necessarily need this type of medical assessment but it is essential to notify the Examination Officer of any eye problems or challenges if the challenges are to do with corrective lenses. This waiver does not apply in cases of a missing eye or deformed eyes.

After receiving a medical report and having passed the provisional driver’s license test, the examiner will endorse on the provisional license certificate the condition under which you should drive. This may appear as a code particularly Code (2) for a physical disability like corrective lenses and Code (1) for a condition that may need one to drive a specially adapted vehicle.

||**ROAD LANE USE**

It is important that every driver knows in which lane to drive. In Zimbabwe drivers keep to the far left of the road, meaning we use the left lane.

**Diagram # 5**

![image alt text](image_4.jpg)

**Diagram # 6				**

![image alt text](image_5.jpg)

The left lane can be subdivided into two or three other lanes. Vehicles **moving at a slow speed should use the left lane at all times.**

**Diagram # 7**

![image alt text](image_6.jpg)

**Three Lane Road**

In a three lane road - vehicles turning left should use the **LEFT** lane. When going straight, use the centre lane.

**Diagram # 8**

![image alt text](image_7.jpg)

**Changing lanes**

When changing lanes it is important to check first if it is safe to do so and then indicate your intention before moving to the intended lane.

**Procedure of changing lanes**

1. Check mirrors and blind spot.

2. Indicate your intention using light indicators and hand signals.

3. If safe, move into intended lane quickly but swiftly.

||** SPEED LIMITS**

Over speeding is the highest contributing factor in most traffic accidents. In as much as speed thrills it is also important to understand that speed kills.

When driving in a small town and urban areas the **general speed limit is 60 km/hr **and the **maximum speed limit is 80km/hr**

The maximum in Zimbabwe varies depending on the class of vehicle. It depends on the type of vehicle being driven. However the **GENERAL maximum limit** in Zimbabwe is 80 km / hr for heavy vehicles 120km/hr for light vehicles.

**Differential Speed**

Differential speed is the relationship between the speed of a vehicle and gap maintained between one vehicle and the vehicle in front. When travelling at 15km/ hr a vehicle should leave a gap equivalent to the length one vehicle in front of us.

**Therefore:**

30km / hr : 2 car gap, 45 km/ hr : 3 car gap, 60km/hr  :  4 car gap, 75km/hr  :  5 car gap and so forth. For every 15km/hr difference you leave one car gap and incrementally in that pattern.

**Total Stopping Distance**

The distance covered whilst trying to stop a vehicle is called the total stopping distance. This is determined using a vehicle in / assuming that a vehicle is in a roadworthy condition. Defective brakes alter the values.

Most Accidents are due to a vehicle failing to stop in time. It is important for a driver to know the distance covered by a vehicle before it comes to a complete halt (stop) when emergency brakes have been applied.

The total stopping distance can be calculated by adding the **Reaction Distance** and the **Braking Distance.**

Reaction Distance + Braking Distance** = Total Stopping Distance (R+B = S)**

**Reaction Distance** is the distance covered whilst reacting to a situation. Reaction distance is calculated as the distance covered from the time of a hazardous situation occurring to the

time just before the driver steps on the brakes. The time taken by the driver to step on the brakes measured from the time the when the driver realised he /she needed to apply the brakes

**Braking Distance **is the distance covered by the vehicle from the time the brakes are applied to the moment the vehicle stops /comes to a complete halt. This varies depending on the conditions of the road. In wet weather the distances becomes much longer. However, under normal road conditions, the following are considered to be correct;

**When travelling at 40 km/hr**

* Reaction Distance = 5,6m

* Braking Distance = 12, 4 m

* Total stopping distance = 18m

**When travelling at 60 km/ hr**

* Reaction Distance = 8,3m

* Braking Distance = 27,7m

* Total stopping distance = 36m

**When travelling at 120 km/hr**

* Reaction Distance = 16,7m

* Braking Distance = 113,3m

* Total stopping distance = 130m

||**NIGHT DRIVING**

In Zimbabwe’s driving legislation, night driving is regarded as driving between **1730 in the evening to 0600 **the following morning.

When driving at night, drivers should exercise extreme caution and skill. Visibility is greatly reduced when driving in the dark hence the need to drive slowly.

**Driving with the aid of vehicle lights / headlights**

It is **mandatory** that headlights be switched on between **5:30 pm and 6:00 am**. Headlights should be checked and cleaned before advancing on the road. Vehicle lighting should be easily accessible and comfortable to a driver.

**Motorcyclists** are to switch on their lights **at anytime of the day** when traveling on the road. Perfect lighting of a vehicle should allow an increase or decrease in intensity. An increase is called** flashing** and a decrease is called **dipping**.

When driving at night, vision is impaired, so it is the norm to flash lights. There are certain situations though that require light be dipped so as to avoid dazzling other road users with our lights.

**Conditions of dipping your lights**

* In a properly lit street

* In front t of oncoming traffic.

* In front policemen controlling traffic at night

* When following behind another vehicle at night

When dazzled by the lights of oncoming traffic it is advised to slow down and cast your eyes slightly to the left.

||**OVERTAKING**

Overtaking is the act of passing a vehicle with the intention of going ahead. At one point in driving, every driver is faced with the need to overtake the vehicle in front.

Overtaking can easily and dangerously become an obsession and that urge should fought if its unnecessary. When overtaking, driver should exercise extreme caution. We overtake using the right side, **BUT** in situations whereby the vehicle being overtaken is turning right, we use the left side to overtake.

There is no specific rule on overtaking an animal drawn wagon. For animal drawn wagons, assessment on the safest side to use should be done before overtaking i.e. drivers use whichever side is safe to do so.

**Conditions Suitable to Overtake**

- Give hand signals

- Check blind spots

- Check ahead and behind.

- Indicate your intention using light indicators.

- Move over to the right if it is clear

- Accelerate and tap horn

- When ahead, indicate to return to the left lane

- Resume normal speed.

When being overtaken, **DO NOT** increase speed.

**Conditions when NOT to Overtake**

- At a blind rise

- Ahead of a corner

- At a narrow bridge

- At a railroad crossing

- At a pedestrian crossing

- In front of oncoming traffic

- Ahead of road intersections

- When forbidden by carriageway markings

- When being overtaken by another vehicle

- Hen visibility is bad e.g. raining, misty or dusty

- Vehicles that have slowed down to give way to other traffic or pedestrians

A **Blind Spot** is an area not visible using view mirrors. A **Blind Rise **is an area uphill where the driver is not able to clearly see or anticipate oncoming traffic.

||** TRAFFIC SIGNS & SIGNALS**

There is constant communication between drivers and the road they travel on. Zimbabwean roads have been designed to caution, inform and regulate road users at all times. This communication is done through the display of traffic signs and signals.

**There are five (5) classes of traffic signs and signals**

1. Class A – Danger Warning Signs. These are always identified by the triangle or a combination with a triangle diagram below.

**Diagram # 9**

![image alt text](image_8.jpg)

2. Class B – Regulatory/ Mandatory signs. These are always identified by the circle or a combination with a circle diagram below.

**Diagram # 10**

![image alt text](image_9.jpg)

3. Class C – Informative Signs. These are always identified by the rectangle or a combination with a rectangle diagram below.

**Diagram # 11**

![image alt text](image_10.jpg)

4. Class D – Traffic Lights

**Diagram # 12**

![image alt text](image_11.jpg)

5. Class E – Carriageway markings

![image alt text](image_12.jpg)![image alt text](image_13.jpg)

It is of great importance that all drivers familiarize with all traffic signs. Through the identification of insignia, it becomes fairly simple to know which action is required when driving in particular environments.

* For areas with danger warning signs, extreme caution is necessary

* Environments with regulatory signs require strict compliance to law being displayed or governed.

* Informative Signs merely inform the driver of locations, directions and names of environments.

**Insignia (Symbols)**

Every class has its own symbol which can assist in identifying and categorizing each sign to its appropriate group. The following are insignias for traffic signs.

**Robots (Traffic Lights) **

Robots have three colours.

**Diagram # 16**

![image alt text](image_14.jpg)		

Note the order of the descending and ascending arrow is referred to as the **correct sequence of a traffic light**.

* **Green**; Proceed with caution

* **Amber**; Stop unless it is not safe to do so

* **Red**; Stop

Traffic Lights can also be referred to as a sequence. When the robot light is not functioning 

correctly, it displays flashing amber light.

**The Sequence **of a traffic light (robot) assumes that the red light is at the top and therefore is the first light on the robot. 

**The Correct Sequence **of a robot states that the robot starts on the green light.

The correct sequence of a robot has a more organized flow compared to the sequence of a robot which is drafted in a haphazard manner.

When a robot doesn't have power, it is just as good as flashing amber, therefore traffic from the right must be given way.

When turning right or left at a robot controlled intersection, drivers should give way to crossing pedestrians. According to the Highway Code, pedestrians have a right of way all the time.

**Q: **What must you do at a flashing amber robot? 

**A: **Give way to traffic from the right.

**Q: **When are you allowed to go through a red robot? 

**A: **When the red sequence is accompanied by a green arrow point in the direction of your travel, even if there is a police officer.

**Class D Traffic Lights**

Class D is for all information displayed in the form of lights. There are two types of traffic lights.

* Traffic Lights (Robots)

* Traffic Signs in conjunction with traffic lights- they emphasize the **existence of danger ahead**, e.g. when the robot is red, it moves next to green and then comes back to amber.

**Diagram # 17		Diagram # 18**

![image alt text](image_15.jpg)![image alt text](image_16.jpg)

**Class E Carriageway Markings**

Class E is for Information displayed on road surfaces.

**Types of carriageway markings**

* Direction arrows

* Longitudinal lines, e.g. broken white lines, continuous white line

* Pedestrian crossing lines

* Stop Line/ Traverse line

* Broken yellow lines – mark the edge of the road.

Longitudinal lines demarcate the centre of the road.

![image alt text](image_17.jpg)

![image alt text](image_18.jpg)

**Exceptional Signs Without Group Insignia**

There are three (3) special signs which do not carry group insignias.

**Diagram # 13		Diagram # 14 		Diagram # 15**

![image alt text](image_19.jpg)![image alt text](image_20.jpg)![image alt text](image_21.jpg)

||**DRIVING ON AN UPHILL OR DOWNHILL**

For any vehicle, it is important to understand the concept of gear changing.

**Q: **Why do we change gears? **A: **We change gears so as to have enough power to suit the conditions of the road or to reach the desired speed. 

Effort should only be used when required - lower gears give a lot of effort, whilst high gears produce less effort. Lower gears are also known as heavy gears due to the fact that they are heavy on the engine and end up reducing engine speed.

When starting to move off, the vehicle is heavy and therefore requires a lower gear, a gear that gives a lot of power. As the vehicle is moving less effort is needed, meaning higher gears are engaged into. When driving on an upgrade (uphill), a lot of effort is require so engage to lower gear.

When driving on a downgrade (downhill) also engage a lower gear. As stated earlier lower gears reduce engine speed and therefore vehicle speed is also reduced on a downgrade.

||** SPECIAL VEHICLES**

In Zimbabwe we have special vehicles that have a right of way above all vehicles. These vehicles can claim that right of way provided they are sounding their siren. Examples Include ambulances, fire brigade trucks, police vehicles and state motor vehicles (motorcade).

An animal drawn wagon is the only special vehicle that does not have a siren.

||** HAZARDOUS CONDITIONS**

There are certain areas when driving that require extreme caution. These areas include:-

* Sharp curves

* Changing lanes

* Parking at a corner

* Obstructions in the road

* Driving on a slippery road

* Dusty, wet, misty conditions  

* Approaching an accident scene

* Driving with passengers onboard

* Driving on an upgrade or downgrade.

* Driving under extreme humid conditions

* Driving in front of vehicle sounding a siren.

* Driving past a vehicle on the side of the road

* Driving under the influence of alcohol or drugs

* Turning left or right at a robot controlled intersection

**Driving past a vehicle parked on the side of the road**

When driving past a parked vehicle, reduce speed and exercise caution, the drivers might just venture into the road whilst you are passing and cause a collision

**Driving on a slippery road**

Drive at a low speed when on a slippery road and use extreme left side of the road.

**Driving under extreme humid conditions**

In humid conditions, it is extremely difficult to see other road users. When driving under such conditions, make sure headlights are on at all times even after 06:30 am. When driving in foggy or misty conditions, fog lights should be on.

**Obstructions in the Road**

In situations where a drier is faced with an obstruction, it is important that the assessment is done on the correct action to take. It is not necessary to stop unless your hazard perception requires you to. In such situations, move over to a clear and safe side and drive off. Give way to oncoming traffic when forced to use restricted space use restricted space.

**Approaching an accident scene**

Accident scenes should be treated with extreme caution when driving and if you come across an accident scene slow down and look out for obstructions. Obstructions could be:

* Car wreckage

* Injured people

* Dead Bodies

* Other vehicles assisting on the scene

If your intention is to stop and help then make sure your vehicle is parked in a very safe place and that it doesn't obstruct traffic.You should a􀆩end to the injured immediately and protect the bodies of the deceased.

If your vehicle had been involved in the accident it is mandatory that you report to the police within 24 hours. Submission of documents should be done within 7 days from the day of the accident.

**Duties of A Driver After An Accident**

-Guard the corpse in one is killed

-Furnish police with the required information

-Rendering possible help that can be needed

-Report to police within 24 hours

**Driving under the influence of drugs and alcohol**

Drunk drivers are **NOT** allowed behind the steering wheel. If vision is impaired by the use of alcohol or any drug, stay off the road.

**Passengers**

When driving, passengers should be seated and loud noises should be avoided. Children of the age 5 and below are not allowed to sit in front. Goods trucks and vehicles are not permitted to carry passengers. Vasolex, tractors and caterpillars are also not permitted to carry passengers.

**Parking at a corner**

It is not permitted to park a vehicle at a corner, it is **mandatory **to park 7,5 m from a corner.
`
