export default `
||**TOWING**

|**AA ZIMBABWE EMERGENCY ROAD RESCUE SERVICE**

By telephoning the AAZ 24 Hour Helpline +263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide.

    This service comprises road rescue officer’s in Harare, Bulawayo, Gweru and Mutare providing a FREE repair (excluding spares) or a FREE get – you – home service. Yet another valuable saving, which could recoup the entire annual subscription, many times over.

**BREAKDOWN SERVICE:**

The AAZ has exclusive Service Provider contracts with specially selected and reputable breakdown firms throughout the country to give priority 24 hour service to AAZ members. Members pay the contractor in full and claim a refund from the AAZ at prescribed rates.

**AAZ 24 HOUR HELPLINE**

By telephoning the AAZ 24 Hour Helpline + 263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide, managed from start to finish by the Helpline Operator. The Operator will dispatch either a Road Rescue Officer (Harare 24hrs) or a Service Provider.

**UNLOCKING SERVICE:**

Members who inadvertently lock their vehicle with keys inside may obtain the service of a Locksmith to open the vehicle and, within certain limits, claim a refund from the AAZ for the Locksmith’s charges.

|**CITY TOUR ROADSIDE ASSITANCE**

CTR offers a variety of towing services in addition to road service, including automobile transport, law enforcement towing, insurance salvage towing and roadside assistance.

We are Zimbabwe's leader in terms of Roadside Assistance. Our services include towing Services, Fuel Delivery, Jump Starting, 24 Hour Towing & Roadside Services as well as emergency roadside services for autos, vans, SUVs, light trucks, motor homes, motor vehicle trade calls, (delivery and collection of vehicles for mechanical services, etc.). Mobile car keys recovery, standard vehicle tyre changes, local & long-distance towing panel beating and spray painting.

||**POLICE**

|**ZIMBABWE REPUBLIC POLICE**

The** **Zimbabwe Republic Police (**ZRP**) is the national police force of Zimbabwe, and is head-quartered in Harare at the Police General Headquarters (**PGHQ**).

The force is organised by province, and comprises uniformed national police referred to as Duty Uniform Branch (**DUB**), the plain clothes comprised Criminal Investigation Department ( CID ) and Police Internal Security Intelligence (**PISI** ), and traffic police (part of DUB).

To date, there are seventeen (17) known provinces which are headed by a Senior Assistant Commissioner. It also includes specialist support units including the (paramilitary) Police Support Unit and riot police and the Canine units.

Overall command of the force is exercised by the Commissioner General Dr. Augustine Chihuri deputised by four Deputy Commissioner Generals who form part of the Central Planning Committee (CPC), a decision passing body in the ZRP. The deputy commissioner generals are also deputised by five commissioners. This structure makes the Commissioner General, a five star general.

|**Chitungwiza District (0270) 30571**

Chitungwiza (0270) 22006

Zengeza (0270) 30399

St Mary’s (0270) 21928

Hatfield (04) 570949

Airport (04) 575366

Epworth (04) 293741

|**Mbare District (04) 754 377**

Mbare (04) 774770

Braeside (04) 743020

Matapi (04) 756115

Stodart (04) 665568

Waterfalls (04) 665395

|**Harare Suburban District (04) 777 639**

Avondale (04) 336 361-2

Borrowdale (04) 860 148

Highlands (04) 496 767

Malbereign (04) 305 651

Mabvuku (04) 491 069

Marlborough (04) 301 802

Rhodesville (04) 495 753 / 481 111

Ruwa 0733 233 686

|**Harare Central District (04) 777 635**

Harare Central (04) 777 777

Milton Park (04) 708 113

|**Masvingo Province Community Relations Office (039) 264 421 or 0775 996 945**

Masvingo West District (0337) 729 705 or 0772 259 403

Mashava (035) 2582

Chivi (037) 225

Ngundu (036) 221

Mwenezi (014) 321

|**Masvingo East District (0338) 724**

Bikita (0338) 222

Mashoko (034) 22705

Chatsworth (0308) 223

Gutu (030) 2222

Zaka (034) 2222

Basera (030) 3179

|**Masvingo Central (039) 66446 or 0777 557 815**

Masvingo Rural (039) 263729

Chikato (039) 262137

Renko (036) 222281

Muchakata (039) 266944

Rujeko (039) 293015

|**Chiredzi District (031) 2456 or 0773 508 216**

Chiredzi (031) 2297/2333

Chikombedzi (014) 3607

Mkwasini (031) 2535

Triangle (033) 6511 / 6237

|**Bulawayo Province Community Relations Office (09)-60358 / 0776 097 122**

Bulawayo Central District (09) 69699

Bulawayo Central (09)71515

License Inspectorate (09)74318

|**Bulawayo West District	(09) 886 370**

Mzilikazi (09) 77619

Njube (09) 412096

Western Commonage (09) 406 775

Entumbane (09) 418 243

|**Nkulumane District (09) 881536**

Nkulumane (09) 48145

Luveve (09) 520 803

Magwegwe (09) 424 771

Pumula (09) 422 907

Tshabalala (09) 489 564

|**Bulawayo Suburban District (09) 65891**

Hillside (09) 242426

Donnington (09) 468 520

Sauerstown (09) 200 960 

Queenspark (09) 226 414

Airport (09) 296 589

|**Midlands Province Community Relations Office (054) 221 073 or 0782 722 722**

|**Kwekwe District (055) 24547**

Amaveni (055) 22808

Mbizo (055) 40446

Silobela (0558) 322

Zhombe (059) 20070 / 20069

Redcliff (059) 68601

Kwekwe Rural (059) 24546

Kwekwe Central (059) 24541-2

|**Gokwe District (059) 2281 / 2221**

Nembudziya (059)2412

Gokwe	(059) 2222 / 2223

|**Zvishavane District (051) 2121-4**

Zvishavane (051) 2044

Mberengwa (051) 221

Mataga	(051) 237 / 655

Buchwa (051) 4097

|**Gweru Urban District (054) 223 770**

Nehanda (054) 256 698

Gweru Central	 (054) 222 121-5

Mkoba	(054) 250 016

Senga (054) 260 411

Mutapa	 (054) 222 727

|**Gweru Rural District (054) 226454**

Gweru Rural (054) 222 184

Maboleni (054) 229 372

Shurugwi (052) 6279/6277

Tongogara (054) 6545

Mvuma (032) 214-5/ 228

Lalapanzi (054) 8338 or 301-2

Charandura (038) 217 /3905

|**Police Protection Unit Community Relations Office (04) 735 751-6 or 0785 004 917**

|**Criminal Investigation Department HQ Community Relations Office (04) 700 171-6**

|**Support Unit Province Community Relations Office (04)497334-6 or 0783 822 619**

|**National Traffic Community Relations Office (04) 754 333**

|**Border Control Community Relations Office (04)2901152-4 or 0773 006 737**

|**Bulawayo Border Control 0785 957 764 **

|**Bulawayo Border Control Community Relations Office (04) 700 931-5 or 0777 966 416**

|**Matebeleland North Province (0281)30259**

|**Mat North Community Relations Office (0281) 32739	** 

Hwange (0281) 32750

Dete (018) 212

Binga (015) 301

Lusulu 0775 580 868

Kamativi (018) 463

Siyabuwa (015) 354

|**Lupane District (09) 60132**

Lupane	 (0389)249

Tsholotsho (0878) 267

Jotsholo (0289) 284

Sipepa	(0898) 442

Insuza	(087) 245

Nyamadhlovu (0287) 304

|**Victoria Falls District (013)41571**

Victoria Falls 0773 029 967

Jambezi  (013) 44681

Kazungula (0558) 559

Nkayi District (0558) 221

Nkayi 0717 101 606

Mbembesi 0775 288 650

Inyati 0713 009 344

|**Mash Central Province Community Relations Office (0271) 7276 or 0778 682 035**

Guruve District (058)2400

Mvurwi	 (058)2316

**
|Bindura District (0271) 6264**

Bindura Central (0271) 6323 or 6745

Bindura Rural	(0271) 7686

Shamva (0371) 357 or 317

Concession (0375) 2212-5

Mazowe (0275) 2613 or 2221

Glendale (0376) 2660 or 2849

Chombira (0277) 2252

Chiwaridzo (0271) 6741 7841

|**Mt Darwin District (0276) 3016**

Mt Darwin (0276) 2394 or 3110

Rushinga (0276) 2391

Madziwa (0271) 2560

Mukumbura (0271) 2357

Dotito (0271) 2395

|**Matebeleland South Province (0284) 22834**

|**Mat South Community Relations Office (0284) 22810	or 0771 443 373**

Gwanda District (0284) 21432

Gwanda Urban	 (0284) 22860

Gwanda Rural	(0284) 23481

Esigodini (0288) 357

Kezi (0282) 334

Filabusi (017)2 47

Fortrixon (0288) 628

Fun – Yeit-Sen (082) 279

Colleenbawn (0284) 20454

West Nickleson (016) 452

Guyu (0284) 23225

|**Beitbridge District	(0286) 23060**

Beitbridge Urban (0286) 22554

Beitbridge Rural (0286) 23722

Tuli (0286) 3358

Zezani	0775 256 613

|**Bulilimamangwe District (019) 3276**

Plumtree (019) 2670

Matopo	 (0383) 225

Figtree	 (0283) 246

Madlambuzi (019)3 013 or 0776 133 875

Mangwe (019) 25209

Mphoengs (019)2669 or 0776 270 510

Mayombodo 0775 870 464

|**Manicaland Province (020) 68733**

Community Relations Office (020) 66637 or 0783 410 710

Mutare Rural District	 (020) 66354

Penhalonga (020) 22212

Mutare Rural (020) 64545

Marange 0716 348 850

Odzi (0204) 2223

|**Mutare Central District (020) 69157**

Mutare Central (020) 64717

Chikanga (020) 10299

Sakubva (020) 64717

Dangamvura (020) 30240

|**Rusape District (025) 2696**

Rusape Urban	(025)2359

Rusape Rural (025) 3723

Headlands (025) 822360

Inyati (025) 822444

Nyazura (025) 83246

Mayo (025) 8223243

 

|**Buhera (021) 2177**

Murambinda	(021) 2314 / 2116

Dorowa 0782 104 795

Muzokomba 0772 192 963

|**Chipinge District (0227) 3368**

Chipinge Urban (022) 72412-3

Chipinge Rural	 (022) 72416

Chimanimani (026) 2535

Chisumbanje (031) 7223

Middle Sabi (024) 322

Nyanyadzi (026) 2338

|**Nyanga District (029) 8851**

Nyanga	 (029) 8211

Ruda (028) 2524

Mutasa	(028) 2586

|**Mash East Province (0279) 23306**

Community Relations Office** **(0271) 7727

(0279) 27142

0783 189 906

|**Marondera District (0279) 23517 or 26508**

Marondera Central (0279) 24419 or 22706

Marondera Rural (0279) 23572 or 22706

Dombotombo (0379) 312-5

Macheke 0772 290 395 or 0779 192 239

Dema (0279) 25300

Mahusekwa (0274) 2213 or 2666

Goromomzi (0222) 22313-5

|**Murehwa District (0278) 22075 or 0734 100 967 or 0773 386 771**

Makosa	 (0278) 2255

Murehwa (0272) 2517

Nyamapanda 0773 514 992 or 0772 952 827

Chinamhora (0274) 22182

Juru (0274)2323 or 2222

Mutoko	 0772 339 235 or 0773 520 357

Mtawatawa (0272) 2595 or 0736 847 684

|**Chivhu District (056) 2244 or (056) 412**

Chivhu (056) 3052

Featherstone (0222) 2369

Sadza (065) 212-4

Beatrice 0772 736 797 or 0773 686 225

|**Mashonaland West Province (067) 24193**

Community Relations Office (067) 25505 or 0777 480 585

Makonde District (067) 23711

Chinhoyi Central (067) 23200

Chinhoyi Rural	 (067) 25397

Chemagamba (067) 23773

Banket (066) 2608

Zvimba (0678) 2210

Kutama (0678) 2180

Mhangura (067) 5520

Mutorashanga	(0668) 213

Murereka 0772 288 802

Kenzamba 0774 072 401

|**Manyame District (062) 3059**

Norton Urban	(062) 2120

Norton traffic	(062) 3530

Norton Rural (062) 3089

Nyabira (069) 259

Darwendale (069)(383) 212

Saruhwe (0628) 44213

Mubaira (065) (200) (400) 475

|**Hurungwe District	(064) 8015**

Karoi urban (064) 6456

Karoi rural (064) 7166

Magunje (064) 6804

Tengwe (064) 7082

Siakobva (061) 2748

|**Kariba District (061) 3010**

Kariba	(061) 3132 or 2704

Chirundu(063) 7642

Makuti	(063) 517

|**Kadoma District (068) 26525**

Kadoma Central (068) 24057

Kadoma Rural	(068) 26111

Rimuka (068) 23740

Battlefield (0557) 435

Effle Flats (068)23025

Sanyati	 (0687) 2551

|**Chegutu District (053) 3403**

Chegutu (053) 2490

Pfupajena (053) 2207

Mamina (068) 32102

Chakari (068) 8237

Chingondo 0782 102 900

`