export default `||**INTRODUCTION **

The Zimbabwe Republic Police (ZRP) uses a **Schedule of Deposit Fines** (Fines) for a range of traffic offences discovered during roadblock or roadside checks by the national traffic police officers. The fines are called deposit fines because the guilty motorist will be ‘depositing’ the fine to / with the traffic officer on behalf of the government.

The traffic police officers were issued with the **2016 Zimbabwe Republic Police National Traffic Schedule of Deposit Fines** in February of 2016 from which these summaries are drawn from. An update of the fines (which we have included) was widely announced on the 30th of March with the signing of the Finance Act into law the previous week by the President, His Excellency Cde R.G. Mugabe.

**What To Note Regarding Spot Fines**

*The Notes below are credited to Sean Quinlan of BigSky Supplies (Pvt) Ltd of *9 Pomona Shopping Centre - Pomona/Borrowdale – Harare *info@bigsky.co.zw. More notes and explanations can be found on the DearZRP FACEBOOK page.*

* The police officers **should not** insist on the payment of a spot fine where the road user is not in a position to do so immediately or where the offence is being contested. It’s both unlawful and unconstitutional.  It must be the **elective choice **of the accused person to pay the prescribed fine instead of appearing in court. 

* The ZRP cannot lawfully impose a spot fine above US$30 (the maximum fine for a **Level 3 Fine Offence**) as** Section 356 of the Criminal Procedure and Evidence Act prohibits this**. Fines higher than level (3) fines can only be imposed by **a magistrate after a court appearance**. 

* However, traffic fines are charged per offence, and if found with more than one offence, the police officer can sum the up to a total, which can in total amount to more than US$30.

* The maximum punishment that can be imposed by a magistrate for traffic offences under the Regulations is US$300 (a Level 5 fine) and a period of imprisonment not exceeding six months, in terms of **Section (87) **of the Regulations.

* The police officer should complete an Admission of Guilt form (Z.R.P N.TFC) with emphasis on the Charge (Section & Statutory Instrument), which informs you of the regulations that have been violated. A **legible and completed** copy of the Admission of Guilt form is required if any motorist wishes to verify and confirm the correctness of the fine imposed by the traffic officer. Therefore ensure that the police officer completes the form in a manner that you can easily read and understand as this is not usually the case. **Form 265, providing the facility of paying a fine within 7 days, is NOT currently allowed as an option.**

* Motorists NO** LONGER** have the option to pay a fine within seven days of the offence.

* According to **Inspector Muhoni from the ZRP**, anyone who is made to pay a fine at a traffic roadblock and is not satisfied as to how much they should have paid can immediately take the admission of guilt receipt to the nearest police station and get confirmation if they had been rightfully charged. 

* If over charged they can immediately get their money refunded. Sergeant Mujuru at the National Police Complaints also echoed Inspector Muhoni’s sentiments saying that anyone who feels cheated has a right to call the **National Complaints Hotline **on **04-7036311** where their complaints will be attended to immediately. 

* The Zimbabwe Republic Police recently launched a national **WhatsApp** hotline where members of the public can register their complaints to the police via WhatsApp. The ZRP National **WhatsApp** hotline number is **+263782475000**.

||**MOVING OFFENCES**

** ROAD TRAFFIC ACT, CHAPTER 13:11 A.R.W ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74**

|**Permitting any vehicle to stop or remain stationary in a dangerous position on any road or parking place (NOT TO BE USED FOR OFFENDERS WHO ARE DOUBLE PARKED)**

**US$15**

Motorist has contravened** Section 3 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall cause or permit any vehicle to stop or remain stationary in a dangerous position on any road or parking-place."*

|**Stop or park any vehicle except on extreme left of the road or in a parking place (DOUBLE PARKING)**

**US$15**

Motorist has contravened** Section 4 (a) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Unless compelled to do so by the presence of traffic on the road or by an instruction given by a policeman or traffic sig, no driver of any vehicle on any road shall stop such vehicle-*

1. *Expect on the extreme left of such road or in a parking-place."*

|**Stop or park within 7.5m of an intersection in the area of a local authority**

**US$15**

Motorist has contravened** Section 4 (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Unless compelled to do so by the presence of traffic on the road or by an instruction given by a policeman or traffic sig, no driver of any vehicle on any road shall stop such vehicle-*

2. *within a distance of 7.5 metres from any corner or, in the area of jurisdiction of a local authority, within such distance of an intersection as may be prescribed in any by-law in force within such areas."*

|**Failure to give way to right at uncontrolled intersection**

**US$15**

Motorist has contravened** Section 6 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of a vehicle on any road in an urban area which meets or intersects another road shall, if no “stop" sign or “give way” sign is placed near such point of meeting on the road on his right hand side.”*

|**Overtaking on the wrong side (NO TRAFFIC LANES OR INDICATION OF TURNING)**

**US$15**

Motorist has contravened** Section 7 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall, when overtaking other traffic, pass such traffic on its right, or off, side:*

* Provided that he may overtake such traffic on its left, or near side-*

1. * within demarcated traffic lanes on any road which is demarcated into two or more such lanes for the same direction or travel, or*

2. * If the driver of the vehicle which he intends to overtake has signalled his intension to turn to the right."*

|**Failure to keep to the left of the road when approaching a corner or meeting other traffic**

**US$15**

Motorist has contravened** Section 7 (1) (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"When meeting other traffic or approaching a corner, drive to the left, or near side of the road."*

|**Overtaking where the driver does not have a clear and unobstructed view of the road ahead**

**US$30**

Motorist has contravened** Section7 (3) (a) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall not overtake other traffic- *

1. *Unless he has a clear and unobstructed view of the road ahead."*

|**Overtaking in face of oncoming traffic**

**US$30**

Motorist has contravened** Section 7 (3) (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall not overtake other traffic- *

*On its right, or off, side unless the road ahead is clear for a distance sufficient to enable him to complete the manoeuvre and return to his proper side before meeting traffic coming from the opposite direction."*

|**Overtaking when driver cannot see sufficiently far ahead to complete the manoeuvre with safety**

**US$30**

Motorist has contravened** Section 7 (3)(c) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"When rounding a corner, or where the road passes over the brow hill, or in any circumstances where he cannot see sufficiently far ahead to complete the manoeuvre with safety."*

|**Overtaking at junction or intersection unmarked road**

**US$30**

Motorist has contravened** Section 7 (3) (d) (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Entry on the road which he is travelling is controlled by a “stop" sign or a “give way” sign at such road intersection or junction, and the road is demarcated into more than two traffic lanes.”*

|**Change lane to cause obstruction**

**US$15**

Motorist has contravened** Section 7 (4)(b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle being overtaken by other traffic shall-*

2. *Where the road is demarcated into two or more traffic lanes for each direction of travel remain in the lane in which he is travelling, and shall not move into any other lane until he can do so without obstructing such other traffic."*

|**Motor vehicles fail to give way on strips**

**US$15**

Motorist has contravened** Section 8 of the ROAD TRAFFIC ACT; CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Subject to the provision of Section 17, any person who is driving any vehicle on a strip that shall-*

1. *When meeting or being overtaken by another vehicle, so give way to the other vehicle that, at the time of passing the right-hand wheel or wheels of his vehicle are not nearer to the middle of the strip track than the left hand strip."*

|**Driving on the wrong side of the road**

**US$15**

Motorist has contravened** Section 9 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Notwithstanding the provisions of Section (7), the driver of any vehicle on any road which is outside an urban area,  and which is demarcated into two or more lanes for the same direction of travel, shall drive in the left-hand, except-*

1. *When overtaking other traffic*

2. *It is his intention to turn to the right*

3. *In order to position himself correctly at an intersection."*

|**Driving the wrong way in a one way road**

**US$30**

Motorist has contravened** Section 10 (a) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive a vehicle on a one way road-*

1. *Opposite to that indicated by the sign as the direction in which traffic shall move."*

|**Driving wrong way in a separate carriageway**

**US$30**

Motorist has contravened** Section 10 (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive a vehicle on a one way road-*

2. *Which constitutes a separate carriage-way in a direction opposite to that in which traffic within that carriage way is intended to move."*

|**Failure to keep to the left of the road when turning left**

**US$15**

Motorist has contravened** Section 11 (1)(a) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle turning from one road into another road-*

1. *To the left shall drive close to the left side of each road."*

|**Cut corner when turning right**

**US$15**

Motorist has contravened** Section 11 (1)(b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states, *"That the driver of any vehicle turning from one road into another road-*

2. *To the right shall drive around the centre-point of the intersection or meeting-place of the two roads, or shall follow any other routes indicated by traffic signs or by other means."*

|**Approaching cross-road, corner, bridge, sharp turn or steep decent other than at a safe speed**

**US$15**

Motorist has contravened** Section 12 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall not approach a cross-road, curve, corner, bridge, sharp turn or steep descent other than at a safe speed."*

|**Alter direction when road not clear to do so**

**US$15**

Motorist has contravened** Section 13 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall not alter direction until he has ascertained that the road is clear."*

|**Failure to signal intention to slow down, stop or alter direction**

**US$15**

Motorist has contravened** Section13 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle shall not slow down, stop or alter directions unless he has given a signal of his intension immediately prior to such slowing down, stopping or alteration of direction."*

|**Failure to make hand or mechanical signal in the manner prescribed**

**US$15**

Motorist has contravened** Section 13 (3) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The signal referred to in Subsection (2) may be given by the use of direction-indicator or other mechanical device or subject to the provisions of Section 14, by the use of the arm and hand."*

|**Failure to hault on the extreme left of the road (single carriage) and remain stationery when being passed by Police, Ambulance or Fire brigade where warning device is being sounded**

**US$30**

Motorist has contravened** Section 16 (a) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"On the approach and during the passing of any ambulance or any vehicle used by a fire-brigade or the police, on which a warning device is being sounded the driver of any vehicle on any road shall-*

1. *If he is travelling on a road consisting of a single carriage-way, draw his vehicle to a hault at the extreme left of the road and remain stationary for as long as may reasonably be necessary."*

|**Same as above nut for other than a single carriageway road**

**US$30**

Motorist has contravened** Section16 (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"On the approach and during the passing of any ambulance or any vehicle used by a fire-brigade or the police, on which a warning device is being sounded the driver of any vehicle on any road shall-*

2. *If he is travelling on any other road, move his vehicle to such a position as to facilitate the passage of such ambulance, fire-brigade or police vehicle, and, if necessary, draw his vehicle to a halt in a safe position and remain stationary for as long as may reasonably be necessary."*

|**Pedal cycle-more than two abreast**

**US$10**

Motorist has contravened** Section 18 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No persons riding pedal cycles having less than three wheels on any road shall ride more than two abreast."*

|**Pedal cycle-hold onto moving vehicle/trailer**

**US$30**

Motorist has contravened** Section 19 of the ROAD TRAFFIC ACT; CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person riding a pedal-cycle on any road shall take or retain hold of a motor vehicle or trailer which is in motion on such road."*

|**Cyclists fail to use cycle track**

**US$10**

Motorist has contravened** Section 20 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Where a portion of road has been set aside as a cycle-track no person shall ride a pedal-cycle on any other portion of such road, unless it is an auto-cycle, which is being propelled entirely or partially by mechanical or electrical power.*

|**Auto-cycle use track with engine on**

**US$10**

Motorist has contravened** Section 20 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall ride an auto-cycle on a cycle-track unless such auto-cycle is being propelled other than a mechanical or electrical power."*

|**Drive on cycle track:-**

**Motor vehicle 	US$30**

**Motor cycle		US$15**

**Animal drawn vehicle	US$15	**

Motorist has contravened** Section20 (3) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive a motor-vehicle or animal-drawn vehicle along a cycle-track."*

|**Drive, cause or permit to be driven any animals at night without two drovers each equipped with reflective staff**

**US$30**

Motorist has contravened** Section 21 (5) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive, or cause or permit to be driven, any livestock along any road at night unless, in addition to the drivers required in terms of subsection (1), the livestock are accompanied by two persons, each carrying a staff complying with the requirements of subsection (7), and one such person precedes and the other follows the livestock at a distance of not less than fifty metres and not more than one hundred and fifty metres."*

|**Cause or permit animals to graze on any road having a bituminous surface of six metres or more in width (PER HEAD)**

**     Cattle, pigs, asses	US$30 **

**     Goats and sheep	US$15**

Motorist has contravened** Section 23 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall cause or permit any animal owned by him or under his charge to raze on any road having a bituminous-coated surface of six metres or more in width if such road has been fenced on both sides."*

|**Causing or permitting animals to stray on any road as described on 23 (1) above**

**US$15**

Motorist has contravened** Section23 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"If any animal is found straying on any road referred to in the subsection (1), the owner or person under whose charge such animal shall be guilty of an offense unless he proves that he has taken and maintained reasonable measures to prevent such animal from straying on to such road."*

|**Leaving animal drawn vehicle unattended**

**US$15**

Motorist has contravened** Section 24 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person driving a vehicle, whether it is in motion or at rest, unless it is attended by a competent person."*

|**Animal drawn vehicle without reins or leader**

**US$15**

Motorist has contravened** Section 25 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that*, "No person shall drive on any road a vehicle drawn by animals which are not led or controlled by reins unless he has placed a person at the head of such animals."*

|**Motor cycle ride more than two abreast**

**US$15**

Motorist has contravened** Section 27 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74, **which states that, *"No persons driving motor-cycles having less than three wheels may, on any road, drive more than two abreast."*

|**Motor vehicle unattended with engine running**

**US$15**

Motorist has contravened** Section 30 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall leave any motor-vehicle with engine running on any road or parking-place unless attended by a competent person."*

|**Driving in such position as not to have full control of vehicle**

**US$15**

Motorist has contravened** Section29 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive a motor-vehicle from such a position that he has no full control of the vehicle or a full view of the road and traffic abreast on either side as well as ahead of him, or permit any person to sit beside him in such a position or manner as in any way to obstruct his view or to hinder him in steering or controlling the vehicle."*

|**Failure to slow down or stop when meeting or overtaking animals being led, driven, ridden or drawing vehicle on road**

**US$15**

Motorist has contravened** Section 31 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any motor-vehicle, when meeting any animal being led or ridden or driven, or a vehicle drawn by an animal, on any road, and on receiving a call or signal from the person in control of such animal or vehicle, shall stop at a sufficient distance to avoid danger, or if overtaking such animal or vehicle, slow down, and, in either case, if the road be upon a hillside or embankment, he shall, on being so required by call or signal, take that side of the road next to the downward slope.”*

|**Permit persons to ride on wings, fenders, luggage grid, roof, running board, towing-bar bumpers or bonnet or motor vehicle (Except for mechanic carrying out repairs)**

**US$15**

Motorist has contravened** Section 32 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall permit any person to ride, and no person shall ride, on the wings, fenders, luggage-grid, roof, running-board, towing-bar, bumpers or bonnet of a motor-vehicle, expect for purpose incidental to and necessary for testing such motor vehicle during or after repairs."*

|**Driver permit person to sit between the driver’s seat and the side of the vehicle nearest the driver’s seat**

**US$15**

Motorist has contravened** Section 33 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No driver of a motor vehicle on any road shall permit any person to sit between the driver’s seat and the side of the vehicle nearest the driver’s sit."*

|**Failure to obey directions, orally or by signal from a police officer in uniform who is controlling traffic**

**US$15**

Motorist has contravened** Section36 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"The driver of any vehicle, and any pedestrian, shall obey directions, whether given orally or by signal, by a policeman in uniform who is controlling the traffic."*

|**No Insurance (1st offence)**

**US$15**

Motorist has contravened** Section (22) (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Subject to this part, no person shall use a motor vehicle or trailer on a road unless there is in force in relation to the use of the motor vehicle or trailer by the user-*

1. *A policy of insurance, or*

2. *A security,*

*In respect of third-party risks which complies with the requirements of this part."*

|**Failure to stop after serious accident**

**Court**

Motorist has contravened** Section 70 (2) (i) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"A person who is a driver of a vehicle on or near a road at the time when the vehicle is involved in or contributes to an accident in which-*

1. *Injury or damage, as the case may be, is caused to any person, animal or property should immediately stop the vehicle."*

|**Failure to stop after minor accident**

**US$30**

Motorist has contravened** Section 70 (2) (i) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"A person who is a driver of a vehicle on or near a road at the time when the vehicle is involved in or contributes to an accident in which-*

1. *Injury or damage, as the case may be, is caused to any person, animal or property should immediately stop the vehicle."*

|**Refusing or failure to supply name and address after minor accident**

**US$30**

Motorist has contravened** Section 70 (2)(v) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"If required to do so by any person having reasonable grounds for so requiring, give to such a person *

1. *His name and address and,*

2. *If he is not the owner of the vehicle, the name and address of the owner of the vehicle, and *

3. *The registration mark and number or other identifying particulars of the vehicle, and*

4. *The name of the insurer by whom the vehicle has been insured, whether in terms of a statutory policy or otherwise, or the name of the giver of a statutory security by whom the vehicle has been secured, as the case may be."*

|**Failure to report minor accident to Police not having supplied name and address to other party**

**US$30**

Motorist has contravened** Section 70 (5) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"If a driver of a motor vehicle which is involved or contributes to an accident referred to in the subsection (2) does not have name and address to any person requiring the same in terms of subparagraphs (v) of subsection (2) and having reasonable grounds for so requiring or if no such requirements made he shall report such accident at a police station or to a police station or to a police officer of a above the rank of sergeant or such other rank as may be prescribed as soon as if reasonably practicable and in any event within twenty four hours of the occurrence of the accident."*

|**Failure to comply with lawful instructions given by police officer**

**US$30**

Motorist has contravened** Section 72 (1) OR (3) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Subject to subsection (2), a police officer or an inspecting officer may require the driver of a vehicle."*

|**Use motor vehicle in contravention of R.T. 16**

**US$15**

Motorist has contravened** Section 73 (2) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"If in the opinion of a police officer or inspecting officer, a vehicle does not comply with this Act, he may, by notice on the prescribed form given to the driver or owner of the vehicle, direct that the vehicle shall not be used on any road."*

|**Failure to produce registration book to V.I.D. within 7 days after demand**

**US$15**

Motorist has contravened** Section 73 (4) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"An inspecting officer who has given a notice in terms of subsection (2) in respect of a motor vehicle or trailer may, by order in writing on the prescribed form given to the owner to deliver to him seven days of  the date of such order the registration book, certificate of fitness and licence, if any of the motor vehicle or trailer."*

|**ALL VEHICLES-displaying any red light on front of vehicle**

**US$30**

Motorist has contravened** Section 15 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"Subject to the provision of Section (30), no person shall display on any vehicle a red light visible from the front of the vehicle."*

|**Animal drawn vehicle-No front white light (either 1 in front of animal or 2 fitted to the vehicle)**

**US$15**

Motorist has contravened** Section 17 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive an animal drawn vehicle on any road at night unless the vehicle is provided with-*

1. *Two substantially white lights visible at a distance of 75 meters from the front of the vehicle or; *

2. *One substantially white light in front of the foremost animal, visible at a distance of 75 meters from the front of an animal."*

|**Animal drawn vehicle-no red rear light**

**US$15**

Motorist has contravened** Section 21 of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall drive an animal drawn vehicle on any road at night unless the vehicle is provided with-*

1. *Two substantially white lights visible at a distance of 75 meters from the front of the vehicle or; *

2. *One substantially white light in front of the foremost animal, visible at a distance of 75 meters from the front of an animal."*

|**No headlight (2 allowed)**

**US$30**

Motorist has contravened** Section 20 (1) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"No person shall be equipped with more than 2 headlights*.

|**Inadequate headlight (30mm minimum)**

**US$15**

Motorist has contravened** Section 20 (2) (b) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"A side car attached to a motor cycle shall be equipped with a lamp, not exceeding seven watts, so placed that no part of the side car on the off or nearside, as the case may be extends laterally more than 400 millimetres beyond the outside edge of the lens of the lamps."*

|**Headlight focused or aimed causing dazzle**

**US$15**

Motorist has contravened** Section 20 (2) (c) of the ROAD TRAFFIC ACT, CHAPTER 13:11 as read with ROAD AND ROAD TRAFFIC (RULES OF THE ROAD) REGS. 308/ 74 **which states that, *"A motor cycle shall be focused and directed so as to avoid dazzling the vision of the driver of any approaching vehicle on a level road."*

||**LIGHTS**

**What To Note**

*The ***_Notes _***below are credited to Sean Quinlan of BigSky Supplies (Pvt) Ltd of *9 Pomona Shopping Centre - Pomona/Borrowdale – Harare *info@bigsky.co.zw. More notes and explanations can be found on the DearZRP FACEBOOK page.*

The fines listed for this type of offences are to be regarded as the maximum fines for the offence committed. Where the offence has occurred for example on a bright moonlight, or in a clearly lit area, the deposit may be halved.

|**Displaying any red light at the front of your vehicle**

**US$30**

Motorists has contravened **Section 15 (1**) of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section 27 and 28 no person shall drive a vehicle of any road if there displayed on the vehicle, any white light which is visible from the vehicle trail"*

|**Failure to switch on lights in lit up area (Where Lights are in working order)**

**US$30**

Motorist has contravened **Section 18 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive any motor vehicle other a motor cycle on any road unless the vehicle is equipped with lamps which shall:*

1. *Two substantial white lights visible at a distance of 75m from the front of the vehicle: or *

2. *One substantial white light in front of the foremost animal visible at a distance of 75m from the front of the animal."*

|**No headlights or side lights**

**US$30**

Motorist has contravened **Section 18 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive any motor vehicle other a motor cycle on any road unless the vehicle is equipped with lamps which shall:*

1. *Two substantial white lights visible at a distance of 75m from the front of the vehicle: or *

2. *One substantial white light in front of the foremost animal visible at a distance of 75m from the front of the animal".*

|**No headlights but side lights only working**

**US$15**

Motorist has contravened **Section 18 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive any motor vehicle other a motor cycle on any road unless the vehicle is equipped with lamps which shall:*

1. *Two substantial white lights visible at a distance of 75m from the front of the vehicle: or *

2. *One substantial white light in front of the foremost animal visible at a distance of 75m from the front of the animal."*

|**One headlight only and no side lights**

**US$15**

Motorist has contravened **Section 18 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive any motor vehicle other a motor cycle on any road unless the vehicle is equipped with lamps which shall:*

1. *Two substantial white lights visible at a distance of 75m from the front of the vehicle: or *

2. *One substantial white light in front of the foremost animal visible at a distance of 75m from the front of the animal."*

|**One headlight and one side light same side of the vehicle**

**US$30**

Motorist has contravened **Section 18 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive any motor vehicle other a motor cycle on any road unless the vehicle is equipped with lamps which shall:*

1. *Two substantial white lights visible at a distance of 75m from the front of the vehicle: or *

2. *One substantial white light in front of the foremost animal visible at a distance of 75m from the front of the animal."*

|**Headlights focused or aimed causing dazzle**

**US$15**

Motorist has contravened **Section 18 (3)(b)(iii) ** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Fitted at the same height on either side of the longitudinal axis of the vehicle, equidistant from such axis and each headlamp shall be in such position that no part of the vehicle or its fittings or fixtures extend laterally on the same side as the headlamp more than four hundred millimetres beyond the outside edge of the headlamp."*

|**Dip switch not working**

**US$15**

Motorist has contravened **Section 18 (3) (iv)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Provide that vehicles equipped with head lamps fitted such that part of the vehicle or its fittings extend laterally more than 400 2011, and equipped with side lamps complying in this respect may be used on a road."*

|**Fog or pass lamps focused or aimed causing dazzle**

**US$15**

Motorist has contravened **Section 19 (1)(b)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Notwithstanding section 18, a motor vehicle may, in addition to the headlamps, be equipped with not more than two  lamps commonly known as “pass-lamps" or “fog-lamps” fitted in accordance with the section.”*

|**Use of fog or pass lamps together with headlight, per lamp**

**US$30**

Motorist has contravened **Section 19 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015) which states that, *"If a motor vehicle is fitted with lamps referred to in subsection."*

|**Displaying white light on the rear of the vehicle (other than reversing light or number plate illumination light)**

**US$30**

Motorist has contravened **Section 21 (2)(b)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, "*A emit red light directed the rear of the vehicle."*

|**Failure to maintain direction indicators (where fitted) in a clean and efficient condition (not temporary cause)**

**US$30**

Motorist has contravened **Section 58 (1)h** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Maintained in a clean unobscured and efficient condition at all times."*

|**No tail lamps on Motor Vehicle or Trailer (more than 2,75m in width), no rear lights (2) (Moving)**

**Unlit US$15, Lit US$15**

Motorist has contravened **Section 21 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle, other than a motor cycle with or without a side car, on any road unless the vehicle is equipped with at least two tail lamps."*

|**No serviceable stoplights (motor vehicles)**

**US$15**

Motorist has contravened **Section 24 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle, other than motor cycle on a road unless the motor vehicle or trailer with at least two stop lights each."*

|**No rear number plate light**

**US$30**

Motorist has contravened **Section 26** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle or motor cycle or trailer on any road unless the motor vehicle or motor cycle or trailer is equipped with at least one lamp capable of illuminating the rear registration plate of the motor cycle, trailer with a white light in complying with the requirements of this section."*

|**No reverse lamp**

**US$15**

Motorist has contravened **Section 27(2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"The reversing lamp referred in subsection (1) shall-*

1. *Have power output of less than 15watts*

2. *No light shall be emitted except when the vehicle is engaged in reverse gear and shall be maintained so as to operate in this manner in all times. *

3. *Be kept clean, undamaged and properly unsecured and in an inefficient operating condition at all times.*

4. *This section shall not apply to vehicles manufactured before 1990."*

|**Use spotlight unlawfully at night**

**US$15**

Motorist has contravened **Section 28 (3)(b)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Except in the case of an emergency, use any spot light on any road in substitution of any lamp mention in subsection 18."*

||**BEACONS**

|**Driving a motor vehicle illegally equipped with a beacon light**

**US$30**

Motorist has contravened **Section 29(1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Except as provided in this section, no person shall drive a motor vehicle on any road if the vehicle is equipped with a beacon light."* 

|**Driving a motor vehicle equipped with a capable of being confused with a beacon light**

**US$30**

Motorist has contravened **Section 29(10)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states, *"No person shall drive on any road a motor vehicle equipped with a lamp capable of being confused with a beacon light, whatever is colour and whether or not it emits a  flashing light."*

|**No Height Lamps (Heavy Vehicles Only)**

**US$15**

Motorist has contravened **Section 30 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states, *"No person shall drive a heavy vehicle on any road unless the vehicle ids fitted with height lamps complying with the requirements of subsection."*

 

||**REFLECTORS**

|**Display red reflectors in front**

**US$15**

Motorist has contravened **Section 32 (4) (a)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Front thereof a retro reflector which is not a white reflector."*

|**Display white reflectors on the rear**

**US$15**

Motorist has contravened **Section 32 (4) (b)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Back thereof a retro reflector which is not a red reflector."*

|**No White front reflectors**

**US$10**

Motorist has contravened **Section 34 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Side thereof a retro reflector which is not a amber reflector."*

|**Inefficient or under size reflectors (Min size 4, 000msq - 35mm wide)**

**US$15**

Motorist has contravened **Section 32 (3)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Every reflector required to be fitted to a vehicle or load in terms of these regulations shall, be not lower than 300 millimetres and not higher than 1.2 meters from the ground level measured to the centre of the retro reflector."*

|**No red rear reflectors (35mm in diameter)**

**US$30**

Motorist has contravened **Section 39 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall on any road drive a motor vehicle other than*

* (a)  Heavy vehicle*

3. *Motor cycle*

4. *Commercial motor vehicle."*

|**No white front reflectors (35mm in diameter)**

**US$30**

Motorist has contravened **Section 39 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall on any road drive a motor vehicle other than a heavy vehicle or a motor cycle, unless two white reflectors complying with section (32) are fixed to the front of the vehicle-*

*(a) within four hundred millimetres of either side of the vehicle measured from the outer edges of the reflector to the edges of the rear of the vehicle; and*

*(b) at the same height."*

|**No continuous white retro reflector in front (Heavy / Commercial Vehicles) **

**US$15**

Motorist has contravened **Section 36(1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a heavy vehicle, unless white retro reflector complying with the requirements of subsection."*

|**No chevron on the rear (Heavy / Commercial Vehicles)**

**US$30**

Motorist has contravened **Section 37** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a vehicle on any road unless a warning sign, conforming with the requirements of subsections."*

|**No continuous rear red reflective warning sign**

**US$15**

Motorist has contravened **Section 38 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a commercial motor vehicle on any road unless the red retro reflector complying with the subsections."*

|**No continuous front white reflective strips**

**US$15**

Motorist has contravened **Section 38 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"The red retro reflector in the subsection (a) consist of a continuous strip of red reflective material, and not less than  fifty millimetres in width, (b) and fixed to the rear of the vehicle and (c)extend horizontally for such as to indicate the vehicle’s width to within four hundred millimetres on either side."*

||**TRAILERS**

**Note**

*The Notes below are credited to Sean Quinlan of BigSky Supplies (Pvt) Ltd of *9 Pomona Shopping Centre - Pomona/Borrowdale – Harare *info@bigsky.co.zw. More notes and explanations can be found on the DearZRP FACEBOOK page.*

For the purposes of this scale, trailers with factory rated local capacity of 545 kgs or more are classified as ‘**HEAVY**’. *Note Credited to Sean Quinlan of BigSky Supplies*

|**All other trailers no continuous red rear reflectors (35mm in diameter)**

**US$15 **

Motorist has contravened **Section 41 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle drawing a light trailer on any road unless a continuous red retro reflector is fixed to the rear of the trailer extending to within 400 millimetres of the trailer measured from the outer edges of the reflector to the outer edges of the trailer."*

|**All trailers (EXCEPT CONSTRUCTION TRAILERS) No continuous no white front reflective "T"**

**US$15 **

Motorist has contravened **Section 41 (3)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle drawing any trailer on any road unless -*

1. *a reflective white "T" of type III complying with the requirements of subsection *

*(4) is fitted to the extreme right front of the trailer;*

*(b) 	a reflective red "T" complying with the requirements of subsection (4) is fitted to the extreme right rear of the trailer and not more than one comma two meters above ground level;*

*(4) 	The letter "T" referred to in subsection (3) shall be not less than 150 millimetres high and 120 millimetres wide, and the strokes of the letters shall be not less than 50 millimetres thick and placed on a black background not less than 200 millimetres in height and 175millimetres in width."*

|**No rear red reflective "T" (EXCEPT CONSTRUCTION TRAILERS)**

**US$15 **

Motorist has contravened **Section 41 (3)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle drawing any trailer on any road unless -*

*(a)	a reflective white "T" of type III complying with the requirements of subsection (4) is fitted to the extreme right front of the trailer;*

*(b) 	a reflective red "T" complying with the requirements of subsection (4) is fitted to the extreme right rear of the trailer and not more than one comma two meters above ground level;*

*(4) 	The letter "T" referred to in subsection (3) shall be not less than 150 millimetres high and 120 millimetres wide, and the strokes of the letters shall be not less than 50 millimetres thick and placed on a black background not less than 200 millimetres in height and 175millimetres in width."*

|**Light 4 Wheel- no brakes at all**

**US$15**

Motorist has contravened **Section 47 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle drawing a trailer, other than a light trailer unless the trailer has an efficient twin line braking system."*

|**Light 4 Wheel- inefficient brakes**

**US$15**

Motorist has contravened **Section 47 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle drawing a trailer, other than a light trailer unless the trailer has an efficient twin line braking system which is constructed according to the following requirements-*

*(a) 	every axle of the trailer shall be equipped with brakes which shall operate on each wheel and in the case of air brakes each wheel shall be equipped with an individual brake chamber; and*

*(b) 	the brake system shall not be rendered immediately ineffective by the non-rotation of the towing vehicle's engine; and*

*(c) 	the brakes on a trailer must operate automatically and quickly if the trailer breaks away from the towing vehicle and remain in operation after such break away; and*

*(d)	every trailer must be fitted with a parking brake-*

*(i) 	if fitted to a trailer of a gross mass in excess of 2 000 kg, it shall be of the spring brake type; and*

*(ii)	in all other cases it should be possible to apply the parking brake manually, or otherwise directly on the trailer; and*

*(e) 	brake line couplings shall not be interchangeable; and*

*(f)	every trailer equipped with air brakes shall in addition to the footbrake and parking brake be equipped with an emergency brake capable of being operated from the driver's position of the towing vehicle."*

|**Heavy (not construction) - no brakes at all**

**US$30**

Motorist has contravened **Section 47 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle drawing a trailer, other than a light trailer unless the trailer has an efficient twin line braking system."*

|**Heavy (not construction) - inefficient brakes**

**US$15**

Motorist has contravened **Section 47 (3)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Any trailer to which this section applies, whether loaded or unloaded, must be capable of achieving a brake efficiency of at least twenty per centum."*

|**Failure to apply brakes when parked (not light or construction vehicle)**

**US$15**

Motorist has contravened **Section 71 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Whenever a trailer, other than a light trailer, is not being drawn or is left unattended, the person in charge of it shall set its parking brake or hand brake so as to maintain it in a stationary position."*

||**TOOLS**

|**Failure to carry 2 Red warning Triangles, in case of trailers, every trailer must have its own set**

**US$15**

Motorist has contravened **Section 52 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle on a road unless two warning devices complying with the requirements of this section are carried in the vehicle: Provided that, if the motor vehicle is drawing one or more trailers, two additional warning devices shall be carried in respect of each such trailer."*

|**Failure to display Red Triangle**

**US$15**

Motorist has contravened **Section 52 (4)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Whenever a motor vehicle or trailer is stationary on any road, except in a place set aside for the stopping of vehicles, the driver shall place one special visual warning device referred to in subsection (1) at the front of the vehicle or trailer and another such device at thereof, so that-*

*(a) 	both special visual warning devices arc on the same side of the carriageway as the vehicle or trailer; and*

*(b) 	the front of each special visual warning device faces away from the vehicle or trailer and towards oncoming traffic; and*

*(c) 	each special visual warning device is not less than thirty meters and not more than fifty."*

|**No serviceable spare wheel all (motor vehicles) / jack, no wheel spanner or wheel brace, No serviceable fire extinguisher light motor vehicle (0,75 kgs) No serviceable fire extinguisher heavy vehicle (1,5 kgs) **

**US$10 for each offence**

Motorist has contravened **Section 53 (1)(a)-(d)**of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle, other than a motor cycle on any road unless the vehicle is equipped with-*

1. *a serviceable spare wheel; and*

2. *an efficient jack; and*

3. *a wheel-brace or wheel-spanner capable of undoing the vehicle's wheel-nuts; an*

*(d) 	in the case of-*

*(i) a light motor vehicle, a serviceable fire extinguisher weighing a minimum of zero comma seven five kilograms; or*

*(ii) a heavy vehicle, a serviceable fire extinguisher weighing a minimum of one comma five kilograms.*

||**HORNS (HOOTER)**

|**No horn (hooter)**

**US$15**

Motorist has contravened **Section 51 (1) (a) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road any motor vehicle unless-*

1. *the motor vehicle is equipped with an efficient audible warning device which is in good working order and when used, capable of emitting a sound which under normal conditions is clearly audible from a distance of at least 100 meters; and*

*(ii) 	the audible warning device is operated by a button or switch that breaks contact automatically when it is released."*

|**Failure to use horn to warn of approach or Use of Horn unnecessarily – not for the purpose of safety or Cause offensive noise with horn**

**US$15 for each offence**

Motorist has contravened **Section 51 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall operate an audible warning device on any motor vehicle in an offensive manner"*

||**TRIANGLES**

|**Failure to carry red warning triangles (one only) or Failure to display red triangle as prescribed at night (one rear)**

**US$15 for each offence**

Motorist has contravened **Section 52 (4)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Whenever a motor vehicle or trailer is stationary on any road, except in a place set aside for the stopping of vehicles, the driver shall place one special visual warning device referred to in subsection (1) at the front of the vehicle or trailer and another such device at the rear, so that-*

1. *both special visual warning devices arc on the same side of the carriageway as the vehicle or trailer; and*

2. *the front of each special visual warning device faces away from the vehicle or trailer and towards oncoming traffic; and*

3. *each special visual warning device is not less than thirty meters and not more than fifty meters from the nearest point of the vehicle or trailer."*

|**Insufficient triangles (two for each drawn vehicle / trailer)**

**US$15 **

Motorist has contravened **Section 52 (1)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive a motor vehicle on a road unless two warning devices complying with the requirements of this section are carried in the vehicle: Provided that, if the motor vehicle is drawing one or more trailers. Two additional warning devices shall be carried in respect of each such trailer."*

||**BRAKE OFFENCES**

**What To Note**

*The Notes below are credited to Sean Quinlan of BigSky Supplies (Pvt) Ltd of *9 Pomona Shopping Centre - Pomona/Borrowdale – Harare *info@bigsky.co.zw. More notes and explanations can be found on the DearZRP FACEBOOK page.*

Where the accused is being charged for defective foot and hand brakes, only one offence under **Section 43(1) (a)** is created. The fine is calculated together with that laid down for hand brake depending on the degree of effectiveness. Where the combined total exceeds US$30, the case must go to court.

|**Foot brakes not working At All**

**Light + Heavy Motor Vehicle & Bus**

**US$30**

Motorist has contravened **Section 46 (1) (a)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle other than*

1. *a tractor not designed to form part of an articulated unit; or*

2. *a construction vehicle; or*

3. *a motorcycle;*

*unless the motor vehicle is equipped with an efficient footbrake and handbrake independently operated, so adjusted as to operate equally with respect to the wheels on either side of the vehicle."*

|**Excessive stopping distance (up to 75% increase on allowed stopping distances), thereafter treat as NOT WORKING AT ALL**

**Light + Heavy Motor Vehicle & Bus**

**US$15**

Motorist has contravened **Section 46 (2)** of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"For the purposes of subsection (1) a brake which is not capable of bringing a motor vehicle, whether loaded or unloaded, to rest from a speed of thirty kilometres per hour on a level, dry road surface in the following distances shall be deemed to be having inefficient brakes unless the brakes comply with the standard specified in the tables below-*

|**Foot brake of motor vehicle or combination of motor vehicles**

<table>
  <tr>
    <td></td>
    <td>INITIAL SPEED IN KM/HR</td>
    <td>MAX STOPPING DISTANCE IN M</td>
    <td>MAX DECELERATION IN M/S2</td>
    <td>MIN EQUIVALENT BRAKING FORCE IN N/KG</td>
  </tr>
  <tr>
    <td>5, 000 KG AND LESS</td>
    <td>
30</td>
    <td>
7</td>
    <td>
5</td>
    <td>
5</td>
  </tr>
  <tr>
    <td>MORE THAN     5, 000 KG</td>
    <td>
30</td>
    <td>
9</td>
    <td>
3,8</td>
    <td>
3,8</td>
  </tr>
</table>


|**Footbrake Insecure fittings (e.g. no split pins) – (locknuts)**

**Light + Heavy Motor Vehicle & Bus**

**US$15 for each**

Motorist has contravened **Section (67) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle or trailer which is in such condition as to endanger any person on the vehicle or on the road."*

|**Handbrake not working at all - maximum**

**Light + Heavy Motor Vehicle & Bus**

**US$15 for each**

Motorist has contravened **Section 67 **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle other than*

1. *a tractor not designed to form part of an articulated unit; or*

2. *a construction vehicle; or*

3. *a motorcycle;*

*unless the motor vehicle is equipped with an efficient footbrake and handbrake independently operated, so adjusted as to operate equally with respect to the wheels on either side of the vehicle."*

|**Handbrake Excessive lever or pull rod movement**

**Light + Heavy Motor Vehicle & Bus**

**US$30 for each**

Motorist has contravened **Section 46 (2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"For the purposes of subsection (1) a brake which is not capable of bringing a motor vehicle, whether loaded or unloaded, to rest from a speed of thirty kilometres per hour on a level, dry road surface in the following distances shall be deemed to be having inefficient brakes unless the brakes comply with the standard specified in the tables below"*

|**Hand brake of motor vehicle or combination of motor vehicles**

<table>
  <tr>
    <td></td>
    <td>INITIAL SPEED IN KM/HR</td>
    <td>MAX STOPPING DISTANCE IN M</td>
    <td>MAX DECELERATION IN M/S2</td>
    <td>MIN EQUIVALENT BRAKING FORCE IN N/KG</td>
  </tr>
  <tr>
    <td>5, 000 KG AND LESS</td>
    <td>
30</td>
    <td>
18</td>
    <td>
1,9</td>
    <td>
1,9</td>
  </tr>
  <tr>
    <td>MORE THAN     5, 000 KG</td>
    <td>
30</td>
    <td>
24</td>
    <td>
1,4</td>
    <td>
1,4</td>
  </tr>
</table>


|**Handbrake Unable to maintain vehicle in stationary position when fully applied**

**Light & Motor Vehicle + Bus**

**US$15 each**

Motorist has contravened **Section 46 (2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"For the purposes of subsection (1) a brake which is not capable of bringing a motor vehicle, whether loaded or unloaded, to rest from a speed of thirty kilometres per hour on a level, dry road surface in the following distances shall be deemed to be having inefficient brakes unless the brakes comply with the standard specified in the tables below"*

|**Hand brake of motor vehicle or combination of motor vehicles**

<table>
  <tr>
    <td></td>
    <td>INITIAL SPEED IN KM/HR</td>
    <td>MAX STOPPING DISTANCE IN M</td>
    <td>MAX DECELERATION IN M/S2</td>
    <td>MIN EQUIVALENT BRAKING FORCE IN N/KG</td>
  </tr>
  <tr>
    <td>5, 000 KG AND LESS</td>
    <td>
30</td>
    <td>
18</td>
    <td>
1,9</td>
    <td>
1,9</td>
  </tr>
  <tr>
    <td>MORE THAN     5, 000 KG</td>
    <td>
30</td>
    <td>
24</td>
    <td>
1,4</td>
    <td>
1,4</td>
  </tr>
</table>


|**Handbrake Insecure fittings (e.g. no split pins) – (locknuts)**

**Light + Heavy Motor Vehicle & Bus**

**US$15for each**

Motorist has contravened **Section 67 **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle other than*

1. *a tractor not designed to form part of an articulated unit; or*

2. *a construction vehicle; or*

3. *a motorcycle;*

*unless the motor vehicle is equipped with an efficient footbrake and handbrake independently operated, so adjusted as to operate equally with respect to the wheels on either side of the vehicle."*

|**Failure to apply handbrake when parked**

**Light + Heavy Motor Vehicle & Bus**

**US$10 for each**

Motorist has contravened **Section 71 (1 & 2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"Wherever a motor vehicle is not being driven or is left unattended, the person in charge of it shall set its hand brake or parking brake so as to maintain it in a stationary position. (2) Whenever a trailer, other than a light trailer, is not being drawn or is left unattended, the person in charge of it shall set its parking brake or hand brake so as to maintain it in a stationary position."*

||**WINDSCREENS & WINDOWS**

|**No window wiper**

**US$10 for each**

Motorist has contravened **Section 71 (1 & 2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle equipped with a windscreen, unless the vehicle is fitted with at least one windscreen wiper-*

*(a) 	which is not operated manually; and*

*(b) 	whose blade, when in operation, wipes the outside of the windscreen continuously, evenly and adequately: Provided that if a motor vehicle is equipped with more than one windscreen all such wipers shall comply with the requirements of paragraphs (a) and (b)."*

|**Drive motor vehicle with glass windscreen not made of safety glass**

**US$15 for each**

Motorist has contravened **Section 54 (1) (a) (i) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle or motor cycle fitted with a cab unless it is equipped with a wind screen complying with the following requirements-*

*if it is composed of glass-It shall consist of safety glass manufactured from either clear plate glass or flat glass that is transparent glass, the surfaces of which are flat and parallel to each other so that they provide clear, undistorted vision and reflection by grinding and polishing on both sides or by production by the float process;"*

|**Drive motor vehicle with glass windscreen that does not provide clear undistorted vision / cracked screen**

**US$15 for each**

Motorist has contravened **Section 54 (1) (b) (ii) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle or motor cycle fitted with a cab unless it is equipped with a wind screen complying with the following requirements-*

*If it is composed of any other material, it shall consist of transparent material so constructed or treated that-*

*(i) 	If fractured, it will not readily shatter into fragments capable of causing severe cuts; and*

*(ii) 	it provides clear, undistorted vision and reflection; and*

*(iii) 	if damaged in any manner, it will ensure a safe degree of visibility for the driver."*

|**Drive motor vehicle with safety glass windscreen which when damaged, fails to remain transparent**

**US$15 for each**

Motorist has contravened **Section 54 (1) (b) (iii) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle or motor cycle fitted with a cab unless it is equipped with a wind screen complying with the following requirements-*

*If it is composed of any other material, it shall consist of transparent material so constructed or treated that-*

*(i) 	if fractured, it will not readily shatter into fragments capable of causing severe cuts; and*

*(ii) 	it provides clear, undistorted vision and reflection; and*

*(iii) 	if damaged in any manner, it will ensure a safe degree of visibility for the driver."*

|**Drive motor vehicle with windscreen not of glass, which will shatter and cause danger if fractured**

**US$15 for each**

Motorist has contravened **Section 54 (2) (b) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Any window or transparent partition other than the \ windscreen of a motor vehicle used on a road shall-*

*If composed of any other material, consist of transparent material so constructed or treated that is fractured. It will not readily shatter into fragments capable of causing severe cuts."*

||**TOWING**

|**Towing another vehicle with tow rope longer than 4m**

**US$15**

Motorist has contravened **Section 69 (1) (a) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall drive on any road a motor vehicle towing another motor vehicle - if the space between the motor vehicles exceeds four meters."*

|**Towing a motor vehicle using a motor vehicle of lesser net mass**

**US$15**

Motorist has contravened **Section 69 (1) (b) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle towing another motor vehicle - if the net mass of the motor vehicle being towed exceeds the net mass of the towing motor vehicle."*

|**Towing a motor vehicle using a motor vehicle of lesser net mass**

**US$15**

Motorist has contravened **Section 69 (1) (c) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road a motor vehicle towing another motor vehicle - unless the motor vehicle being towed is so attached to the towing motor vehicle as to be under proper control."*

|**Towing more than three trailers**

**US$15**

Motorist has contravened **Section 69 (2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person shall drive on any road any motor vehicle towing more than three trailers."*

|**Drive Motor Vehicle so as to push another vehicle**

**US$15**

Motorist has contravened **Section 70**of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"No person, when driving a motor vehicle on any road, shall cause any other motor vehicle to move on a road by pushing it with another motor vehicle."*

||**LOADING **

|**Failure to display gross and net mass on the left side of the vehicle**

**US$15**

Motorist has contravened **Section 72 (4)**of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that,

*"Subject to subsection (8), no person shall drive on any road a commercial vehicle unless there is displayed on the left outside of the vehicle a notice clearly showing in kilograms, the vehicle's gross mass and net mass determined in accordance with subsection (6)."*

|**Driving with load overhanging sides more than 600mm**

**US$15**

Motorist has contravened **Section 76 (a) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section 72, no person shall drive any vehicle on a road if the vehicle's load-projects more than 600 millimetres beyond the lateral extremities of the vehicles;"*

|**Driving with load in excess of 4,6m or likely to damage overhead wires, bridges or other constructions**

**US$30**

Motorist has contravened **Section 76 (b) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section 72, no person shall drive any vehicle on a road if the vehicle's load -is of such a height that is likely to interfere with or damage any bridge, wire or other construction lawfully erected above the road surface;"*

|**Driving with load in excess of 4,6m or likely to damage overhead wires, bridges or other constructions**

**US$15**

Motorist has contravened **Section 76 (c) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section 72, no person shall drive any vehicle on a road if the vehicle's load - is not safely contained with the vehicle's body or securely fastened to the vehicle.”*

|**Driving with load obstructing the driver’s view abreast or ahead**

**US$30**

Motorist has contravened **Section 76 (d) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section (72), no person shall drive any vehicle on a road if the vehicle's load - is carried or arranged in such a way as to obstruct the driver's view of traffic abreast on either side of him or her or ahead of him or her."*

|**Driving with load overhang front more than 900mm**

**US$30**

Motorist has contravened **Section 75 (b) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section (74), no person shall drive any vehicle on a road if the vehicle's load - is carried or arranged in such a way as to obstruct the driver's view of traffic abreast on either side of him or her or ahead of him or her."*

|**Driving with load overhang rear more than 1,2m**

**US$30**

Motorist has contravened **Section 75 (c) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section (74), no person shall drive any vehicle on a road if the vehicle's load - is carried or arranged in such a way as to obstruct the driver's view of traffic abreast on either side of him or her or ahead of him or her."*

|**Driving with load over 2,5m in width**

**US$30**

Motorist has contravened **Section 75 (d) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section (74), no person shall drive any vehicle on a road if the vehicle's load - extends beyond two comma five meters on either side of the vehicle."*

|**Driving with a dangerous load – weight, distribution packing, etc. likely to cause danger to another person or vehicle on the road**

**US$30**

Motorist has contravened **Section 75 (e) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"Subject to section (74), no person shall drive any vehicle on a road if the vehicle's load is - likely to cause danger to any person on the vehicle or on the road owing to its mass distribution, packing or adjustment."*

|**Driving with insufficient or no red flag on extended load of 600mm or more at rear by day including stationary vehicles (Min size of red flag is 600 mm sq)**

**US$30**

Motorist has contravened **Section 78 (1) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *" No person shall, by day drive a loaded vehicle on any road or cause or permit a loaded vehicle to be stationary on any road, if the vehicle's load projects to the rear more than 600 millimetres beyond the back of the vehicle, unless a flag of bright red cloth, at least 600 square millimetres, is attached at the extreme rear of the load."*

|**Driving with insufficient or no bright red light or red reflector on extended load of 600mm or more at rear at night including stationary vehicles (Round Reflector 250mm diameter, Square reflector 250mm sides)**

**US$30**

Motorist has contravened **Section 78 (2) **of the **ROADS AND ROAD TRAFFIC (CONSTRUCTION EQUIPMENT AND USE) STATUTORY INSTRUMENT 129/2015 **(SI 129/2015), which states that, *"No person shall, at night, drive a loaded vehicle on any road or cause or permit a loaded vehicle to be stationary on any road, if the vehicle's load projects to rear more than 600 millimetres behind the motor vehicle's tail-lamp, unless a bright red light is attached at the extreme rear of the load, facing directly to the rear and so placed on the load as to be not more than one comma two meters above ground level or, if the lowest point of the load is more than one comma two meters above ground level, placed at the lowest point of the load: *

*Provided that a solid square or circular retro of diamond grade reflector, facing directly to the rear, with sides not less than 250 millimetres or with a diameter not less than 250 millimetres, as the case may be, may be used instead of a bright red light."*

||**LICENCING OFFENCES**

**VEHICLE REGISTRATION AND LICENCING ACT, (CHAPTER 13:14)**

|**Failure to register a Motor Vehicle**

**US$30**

The Motorist would have contravened **Section 6 (1)** of the **VEHICLE REGISTRATION AND LICENCING ACT, **as read with **Section 2** which states that, *"Every vehicle which is to be used any road shall be registered in terms of this Act. It further states that, “It shall be the duty of the owner of any vehicle to register such vehicle in terms of this Act."*

|**Failure to register a Trailer**

**US$15**

The Motorist would have contravened **Section 6 (1)** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that *"Every vehicle which is to be used any road shall be registered in terms of this Act. It further states that, “It shall be the duty of the owner of any vehicle to register such vehicle in terms of this Act."*

|**Failure to display the registration mark and number **

**US$15**

The Motorist would have contravened **Section 10** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The Registration Mark and Number Allocated to a vehicle shall be displayed and maintained upon that vehicle in the prescribed manner."*

|**Illegible registration mark and number (NOT TEMPORAY CAUSE)**

**US$15**

The Motorist would have contravened **Section 10** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The Registration Mark and Number Allocated to a vehicle shall be displayed and maintained upon that vehicle in the prescribed manner."*

|**Tempering with 3rd number plate or defaced 3rd number plate**

**US$30**

The Motorist would have contravened **Section 10** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The Registration Mark and Number Allocated to a vehicle shall be displayed and maintained upon that vehicle in the prescribed manner."*

|**Failure to register the permanent removal of a vehicle the road within 14 days**

**US$15**

The Motorist would have contravened **Section 11 (1)** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"When any registered vehicle, other than a vehicle which is exported from Zimbabwe, permanently ceases to be used on any road, the owner shall, not later than 14 days thereafter, remove or obliterate the registration of the vehicle."*

|**Previous owner’s failure to notify officer prescribed within 14 days upon sale or change of ownership of a registered vehicle**

**US$30**

The Motorist would have contravened **Section 13** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"Upon the sale or other change of ownership of a registration vehicle the person selling or disposing the vehicle shall deliver to the new owner the registration book and any current license relating to the vehicle and shall, not later than 14 days after such change of ownership, notify a registering officer thereof in the prescribed manner."*

|**Previous owner fails to handover registration book and/or vehicle to new owner or upon sale of vehicle**

**US$15**

The Motorist would have contravened **Section 13** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"Upon the sale or other change of ownership of a registration vehicle the person selling or disposing the vehicle shall deliver to the new owner the registration book and any current license relating to the vehicle and shall, not later than 14 days after such change of ownership, notify a registering officer thereof in the prescribed manner."*

|**New owner’s failure to notify registering officer within 14 days upon change of ownership **

**US$15**

The Motorist would have contravened **Section 14 (1)** of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The owner of a vehicle referred to in section thirteen shall, not later than 14days after the vehicle is acquired by him, apply to a registering officer for the change of ownership to be registered."*

|**Second hand car dealer’s failure to render monthly returns to registering officer**

**US$30**

Contravening** Section 15 (2) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"Every person who carries on the business of dealing in second-hand vehicles shall, within 14 days after the commencement of each month, furnish to the Registrar in the prescribed form a return indicating the registration mark and number of every registered vehicle kept by him for sale or other disposal and his possession or custody on the first day of that month."*

|**Failure to display current vehicle licence or temporary licence**

**US$10**

Contravening** Section 29 **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"Any licence or temporary licence shall be displayed and maintained in the prescribed manner upon the vehicle to which it relates."*

|**Owner’s failure to register change of permanent address within 14 days**

**US$10**

Contravening** Section 16 (1) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"When any permanent change occurs in the address at which a registered vehicle is ordinary kept at night, the owner shall, not later than 14 days after such change occurs, apply to a registering officer for the change of address to be registered.” *

|**Owner’s failure to register in Zimbabwe a vehicle registered outside Zimbabwe within 30 days of becoming a resident**

**US$30**

Contravening** Section 41 (1) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The owner of any vehicle registered outside Zimbabwe who brings such vehicle into Zimbabwe and who become a permanent residence of Zimbabwe shall, within 30 days of becoming a permanent  resident, register and license such vehicle in accordance to this Act: Provided that section forty shall apply during such period of thirty days.”*

|**Failure to produce documents (Registration Book) to an authorized Police Officer within 7 days of request**

**US$10**

Contravening** Section 46 (1) as read with Section (2) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"When making an application under subsection (1) the owner may surrender to the registering officer any valid licence issued in respect of such vehicle by an authority outside Zimbabwe.”*

|**Owner’s failure to licence a registered vehicle**

Light Motor Vehicle (L.M.V): under 2, 300kgs  

**US$15**

Heavy Motor Vehicle (H.M.V): >2, 300Kg < 4, 600kgs

**US$15**

Heavy Motor Vehicle (H.M.V): >4, 600Kg <9, 000kgs

**US$15**

Heavy Motor Vehicle (H.M.V): 9, 000kgs& above

**US$15**

Motor Cycle (M.C) up to 700cc

**US$15**

Motor Cycle (M.C) over 700cc

**US$15**

Trailers

**US$15**

Tractors

**US$15**

Contravening** Section 22 (1) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"A vehicle shall be deemed to be licensed under this Act and a registration book shall be deemed to have been issued under this Act:  *

1. *valid temporary identification card issued in respect of the vehicle is attached to it in the prescribed position and,*

*b) the vehicle is being used on the route specified on the temporary identification card."*

|**Failure to affix garage plates to vehicle used for the prescribed purpose**

**US$15**

Contravening** Section 42 (5) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The registration officer, if satisfied that the application is in order, shall issue a garage licence in the prescribed form all shall allocate a distinctive mark and number approved by the Register to be used in relation to such licence."*

|**Using a licence that was issued to another vehicle**

**US$30**

Contravening** Section 44 (2) (e) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"Any person who-*

1. *uses on any vehicle a licence, temporary license or temporary identification card issued in respect of any other vehicle;*

*shall be guilty of an offence."*

**Please Note:**

Any contravention of Section (44) of any offence involving Forgery of vehicle papers of licence will be prosecuted in court.

|**Refusal to give or giving misleading, false or inaccurate information regarding the cc of a Motor Cycle Engine**

**US$30**

Contravening** Section (48) as read with Section (45) **of the** VEHICLE REGISTRATION AND LICENCING ACT, **which states that, *"The owner of cycle which has a motor attached thereto or permanently forming part thereof shall, if so required by an authorized officer or police officer, furnish such evidence as may be reasonably available to or obtainable by him in regard to the engine capacity of such motor."*

||**SIGNS, SIGNALS & ROAD MARKINGS**

|**Failure to obey right turn arrows (Motor Vehicle + Cycle)**

**US$15**

The motorist has Contravened** Section 43 (5) **of the **ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Failure to obey left turn arrows (Motor Vehicle + Cycle)**

**US$15**

The motorist has Contravened** Section 43 (5)** of the **ROAD TRAFFIC ACT CHAPTER 13:11**which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Turn right from straight ahead lane **

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the **ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against no left turn**

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the **ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against no right turn **

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the **ROAD TRAFFIC ACT CHAPTER 13:11**which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Failure to obey compulsory direction sign  **

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the **ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Failure to obey direction prohibited **

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against passage prohibited **

**Motor Vehicle US$15, Cycle US$15**

The motorist has Contravened** Section 19 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"Subject to the subsection (2), a person who drives the vehicle on a road at a speed in excess of the appropriate maximum speed which (a) has been provided for or fixed in terms of one or the other of the provisions referred to the subsection (1) of Section (41) in respect of the road.*

*(b) is deemed in terms of subsection (4) of Section (41) to be applicable to the road."*

|**Failure to obey prohibition **

**Motor Vehicle US$15, Cycle US$15**

The motorist has Contravened** Section 19 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"Subject to the subsection (2), a person who drives the vehicle on a road at a speed in excess of the appropriate maximum speed which (a) has been provided for or fixed in terms of one or the other of the provisions referred to the subsection (1) of Section (41) in respect of the road.*

*(b) is deemed in terms of subsection (4) of Section (41) to be applicable to the road."*

|**Failure to obey ‘’No stopping sing’’**

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against weight prohibited sign **

**Motor Vehicle US$15, Cycle 10**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against red robot **

**Motor Vehicle US$30, Cycle UUS$30**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Proceed against amber robot **

**Motor Vehicle US$15, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Encroach over white line at a robot **

**Motor Vehicle US$15, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of the Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

**Enter intersection when exit is not clear **

**Motor Vehicle US$30, Cycle US$30**

The motorist has Contravened** Section 49 (2) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Failure to stop at a flash lights (railway crossing)**

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**Failure to obey regulatory signs:**

**No stopping, no parking, no left turn **

**Motor Vehicle US$30, Cycle US$15**

The motorist has Contravened** Section 43 (5) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"The driver shall obey: (b) notwithstanding any instruction conveyed by a traffic sign placed in terms of this Part or by any prohibition, condition, restriction, or limitation made or imposed in terms of Section (40), all directions whether verbal or by signal, given by a police officer in uniform."*

|**All light motor vehicles (urban and rural): speed limit**

(1 to 5) km/hr in excess of speed limit –caution 

(6 to 15) km/hr in excess of the speed limit - **$US5**

(16 to 25) km/hr in excess of the speed limit - **$US10** 

(26 to 35) km/hr in excess of the speed limit - **$US15** 

(36 to 50) km/hr in excess of the speed limit - **$US20** 

Over 50 km/hr in excess of the speed limit - **Court **

The motorist has Contravened** Section 50 (1) **of the** ROAD TRAFFIC ACT CHAPTER 13:11 **which states that, *"Subject to subsection (2), a person who drives the vehicle on a road at a speed in excess of the appropriate maximum speed which; (a) has been provided for or fixed in terms of one or the other of the provisions referred to subsection (1) of Section (41) in respect of the road. (b) is deemed in terms of subsection (4) of Section (41) to be applicable to the road."*

|**Heavy vehicles to which restrictive speed limits apply (urban and rural)**

(1 to 5) km/hr in excess of the speed limit –** Caution **

(6 to 15) km/hr in excess of the speed limit - **$US5**

(16 to 25) km/hr in excess of the speed limit - **$US10** 

(26 to 35) km/hr in excess of the speed limit - **$US15** 

(36 to 50) km/hr in excess of the speed limit - **$US20** 

0ver 50 km/hr in excess of the speed limit** – Court **

The motorist has Contravened **Section 50 (1)** of the **ROAD TRAFFIC ACT CHAPTER** 13:11 which states that, *"Subject to the subsection (2), a person who drives the vehicle on a road at a speed in excess of the appropriate maximum speed which (a) has been provided for or fixed in terms of one or the other of the provisions referred to the subsection (1) of Section (41) in respect of the road.*

*(b) is deemed in terms of subsection (4) of Section (41) to be applicable to the road."*

||**COURT OFFENCES**

|**Court Offences**

* Driving under influence of alcohol

* Driving in a reckless manner

* Negligent driving

* Driving without care and attention

* No driver’s license

* Failure to produce documents to the police

* Permit unlicensed driver to driver

* Driving at a dangerous speed

* Failure to stop after a road accident

The motorist has Contravened **Section 52-70** of the **ROAD TRAFFIC ACT **CHAPTER 13:11 which states that, *"Negligent driving, Reckless Driving, Driving with prohibited concentration of alcohol in the blood, Driving under the influence of blood is subject to Part IX, a court which convicts a person of an offense in terms of subsection (1) involving the driving of a motor vehicle"*

`