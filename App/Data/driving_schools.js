export default `||**RECOMMENDED DRIVING SCHOOLS**

|**Ring Driving School**

![image alt text](image_22.jpg)

In August 2009, after having noticed that the driving school industry was lacking a high standard of professionalism, quality vehicles and integrity, we started Ring Driving School to bring these aspects to driver training in Zimbabwe. 

Our **88% pass rate** for the student drivers who train with us testifies of our professionalism and experience and is undisputed evidence that we are very good at what we do.

We take pride in the fact that we have brand new cars in our pool and maintain a modern fleet as can be seen below;

Our Nissan UD70 Trucks were bought brand new from Dulys, the oldest being five years old and the other (8) other UD70 trucks are averagely three years old.

Our (3) Chery cars are less than two years old, bought brand new from Quest Motors, (2) Chevrolets less than a year old, bought brand new from GM Chevrolet and the Nissan March cars averagely three years old.

We have the following classes and vehicles to serve you;

* One Class (4) vehicle in Bulawayo, two in Chitungwiza, and nine in Harare.

* One Class (1) vehicle (Bus) covering Bulawayo every 1st week of every month, Chitungwiza every Thursday and Harare the 2nd, 3rd and 4th week of every month.

* Two Class (3) vehicles (Motor Bikes) currently covering Harare only with the plan to expand as the need arises.

* One Class (2) vehicle (UD70 Truck) in Bulawayo, two in Chitungwiza and four in Harare.

* One Horse & Tanker Class (1) which run for 2 weeks. After two weeks of training you get certificate of training valid within the SADC region in collaboration with the Traffic Safety Council of Zimbabwe.


We also conduct Oral Lessons / Classes for those that wish to go a sit for their provisional driver’s license test.
`

