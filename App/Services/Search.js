var JsSearch = require('js-search')
const create = () => {
  const search = (data, query) => {
    return new Promise((resolve, reject) => {
      try {
        const search = new JsSearch.Search('id')
        search.addIndex(['title'])
        search.addIndex(['text'])
        search.addIndex(['description'])
        search.addIndex(['title', 'text', 'description'])
        const documents = Object.keys(data).map((key) => data[key])
        search.addDocuments(documents)
        const res = search.search(query)
        const timeout = setTimeout(() => {
          clearTimeout(timeout)
          resolve(res)
        }, 0)
      } catch (e) {
        return reject(e)
      }
    })
  }

  return {
    search
  }
}

export default {
  create
}
