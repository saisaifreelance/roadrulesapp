import { NavigationActions } from 'react-navigation'
import { actionTypes } from 'react-redux-firebase'
import AppNavigation from '../Navigation/AppNavigation'

export const NAV_TEST = 'NAV_TEST'

export const reducer = (state, action) => {
  if (action.type === actionTypes.LOGOUT) {
    const { routeName } = state.routes[state.index]
    if (routeName === 'LaunchScreen') {
      return state
    }
    return AppNavigation.router.getStateForAction({
      type: NavigationActions.RESET,
      index: 0,
      actions: [
        {
          type: NavigationActions.NAVIGATE,
          routeName: 'LaunchScreen',
          params: {}
        }
      ]
    }, state)
  }
  const newState = AppNavigation.router.getStateForAction(action, state)
  return newState || state
}
