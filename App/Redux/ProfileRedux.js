import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import { REHYDRATE } from 'redux-persist/constants'
import { actionTypes } from 'react-redux-firebase'
import { fields as profileFields } from '../Schema/Profile'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  fieldChangeAction: ['fields'],
  fieldErrorsAction: ['fields', 'errors'],
  fetchProfilePictureSuccessAction: ['image'],
  fetchProfilePictureFailAction: ['error'],
  clearVerificationIdAction: null,
  clearErrorAction: null,
  logoutRequestAction: null,
  logoutSuccessAction: null,
  logoutFailureAction: ['error'],
  loginRequestAction: ['fields', 'form'],
  loginSuccessAction: ['verificationId'],
  loginFailureAction: ['error'],
  signInAnonymouslyAction: null,
  verifyRequestAction: ['fields', 'form'],
  verifySuccessAction: ['user'],
  verifyFailureAction: ['error'],
  updateProfileRequestAction: ['fields', 'form'],
  updateProfileSuccessAction: ['user', 'response'],
  updateProfileFailureAction: ['user', 'profile', 'error'],
  updateProfilePictureAction: ['image'],
  updateProfilePictureRequestAction: ['image'],
  updateProfilePictureSuccessAction: ['image'],
  updateProfilePictureFailureAction: ['error'],
  activateAction: ['activationCode'],
  activateRequestAction: ['activationCode'],
  activateSuccessAction: ['activationCode', 'activatedAt'],
  activateFailureAction: ['error'],
  setProfileAction: ['profile'],
  onVerificationCompletedAction: ['verificationCode'],
  onCodeAutoRetrievalTimeoutAction: ['verificationId'],
  getTokenFailAction: ['error'],
  refreshProfileAction: ['id'],
  updateTokenAction: ['token'],
  updateTokenSuccessAction: ['token', 'response'],
  updateTokenFailAction: ['token', 'error'],
  onMessageAction: ['message']
}, {prefix: 'ProfileRedux/'})

export const ProfileTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  firstRun: true,
  isFetching: null,
  error: null,
  isHydrated: false,
  isSet: false,
  data: null,
  activatedAt: null,
  form: {},
  image: null
})

/* ------------- Reducers ------------- */

export const rehydrate = (state, { payload: { profile } }) => {
  const { isFetching } = state
  if (!profile) {
    return state.merge({form: {}, image: null}).set('isHydrated', true)
  }
  state = state.set('activatedAt', profile.activatedAt)
  if (!state.isSet) {
    return state.merge(profile).merge({form: {}, image: null, isFetching}).set('isHydrated', true)
  }
  return state.set('isHydrated', true)
}

export const setProfile = (state, { profile }) => {
  let returnState = state
    .set('data', profile)
    .set('isSet', true)
  for (let i = 0; i < profileFields.length; i += 1) {
    const fieldName = profileFields[i].key
    if (profileFields[i].formEditable && profile[fieldName]) {
      returnState = returnState.setIn(['form', fieldName], {value: profile[fieldName], error: ''})
    }
  }
  return returnState
}

export const fieldChange = (state, { fields }) => {
  let returnState = state
  const keys = Object.keys(fields)
  for (let i = 0; i < keys.length; i += 1) {
    returnState = returnState
      .setIn(['form', keys[i], 'value'], fields[keys[i]])
      .setIn(['form', keys[i], 'error'], '')
  }
  return returnState
}

export const fieldErrors = (state, { fields, errors }) => {
  let returnState = state
  for (let i = 0; i < fields.length; i += 1) {
    const {key} = fields[i]
    returnState = returnState
      .setIn(['form', key, 'error'], errors[key] ? errors[key] : '')
      .set('isFetching', null)
  }
  return returnState
}

export const loginRequest = (state, { fields, form }) => {
  let returnState = state.set('isFetching', 'Sending verification code...')
  const keys = Object.keys(form)
  for (let i = 0; i < keys.length; i += 1) {
    const key = keys[i]
    const value = form[key].value
    returnState = returnState.setIn(['form', key, 'value'], value).setIn(['form', key, 'error'], '')
  }
  return returnState
}

export const loginSuccess = (state, { verificationId }) => {
  return state.merge({ isFetching: null, verificationId, error: null })
}

export const loginFailure = (state, {error}) => {
  return state.merge({ isFetching: null, error })
}

export const clearVerificationId = (state) => state.set('verificationId', null).set('error', null)

export const clearError = (state) => state.set('error', null)

export const updateProfilePictureRequest = (state, { image }) => {
  return state.set('image', { ...image, percent: 0 })
}

export const updateProfilePictureProgress = (state, { file, payload: { percent } }) => {
  if (!state.image || file.id !== state.image.id) {
    return state
  }
  return state.set('image', { ...file, percent })
}

export const updateProfilePictureSuccess = (state, { image }) => {
  return state.set('image', image)
}

export const updateProfilePictureFailure = (state, { image, error }) => {
  return state.set('image', { ...image, error })
}

export const fetchProfilePictureSuccess = (state, { image }) => {
  return state.set('image', image)
}

export const fetchProfilePictureFailure = (state, { image, error }) => {
  return state.set('image', { error })
}

export const updateProfileRequest = (state, { user, profile }) => {
  return state.set('isFetching', 'Updating profile...')
}

export const updateProfileSuccess = (state, { user, response }) => {
  return state.merge({isFetching: null, error: null})
}

export const updateProfileFailure = (state, { user, profile, error }) => {
  return state.merge({isFetching: null, error})
}

export const verifyRequest = (state, { user, profile }) => {
  return state.set('isFetching', 'Verifying phone number...')
}

export const verifySuccess = (state, { user }) => {
  return state.merge({isFetching: null, verificationId: null, data: { id: user.uid }, error: null})
}

export const verifyFailure = (state, { user, profile, error }) => {
  return state.merge({isFetching: null, error})
}

export const logoutRequest = (state, { user, profile }) => {
  return state.set('isFetching', 'Logging out...')
}

export const logoutSuccess = (state) => {
  return state.merge({
    isFetching: null,
    error: null,
    isSet: true,
    data: null,
    form: {},
    image: null
  })
}

export const logoutFailure = (state, { user, profile, error }) => {
  return state.merge({isFetching: null, error})
}

export const onVerificationCompleted = (state, { verificationCode }) => {
  return state
}

export const onCodeAutoRetrievalTimeout = (state, { verificationId }) => {
  return state
}

export const onActivate = (state, { activationCode: enteredActivationCode }) => {
  const { activationCode } = state.data
  if (enteredActivationCode === activationCode) {
    const activatedAt = Date.now()
    return state.set('activatedAt', activatedAt)
  }
  return state
}

export const onActivateSuccess = (state, { activationCode, activatedAt }) => {
  return state
    .setIn(['data', 'activatedAt'], activatedAt)
    .setIn(['data', 'activationCode'], activationCode)
}

export const noop = (state) => state

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {

  [REHYDRATE]: rehydrate,
  [Types.FIELD_CHANGE_ACTION]: fieldChange,
  [Types.FIELD_ERRORS_ACTION]: fieldErrors,
  [Types.CLEAR_VERIFICATION_ID_ACTION]: clearVerificationId,
  [Types.CLEAR_ERROR_ACTION]: clearError,
  [Types.LOGIN_REQUEST_ACTION]: loginRequest,
  [Types.LOGIN_SUCCESS_ACTION]: loginSuccess,
  [Types.LOGIN_FAILURE_ACTION]: loginFailure,
  [Types.LOGOUT_REQUEST_ACTION]: logoutRequest,
  [Types.LOGOUT_SUCCESS_ACTION]: logoutSuccess,
  [actionTypes.LOGOUT]: logoutSuccess,
  [Types.LOGOUT_FAILURE_ACTION]: logoutFailure,
  [Types.UPDATE_PROFILE_REQUEST_ACTION]: updateProfileRequest,
  [Types.UPDATE_PROFILE_SUCCESS_ACTION]: updateProfileSuccess,
  [Types.UPDATE_PROFILE_FAILURE_ACTION]: updateProfileFailure,
  [Types.UPDATE_PROFILE_PICTURE_REQUEST_ACTION]: updateProfilePictureRequest,
  [Types.UPDATE_PROFILE_PICTURE_SUCCESS_ACTION]: updateProfilePictureSuccess,
  [Types.UPDATE_PROFILE_PICTURE_FAILURE_ACTION]: updateProfilePictureFailure,
  [Types.FETCH_PROFILE_PICTURE_SUCCESS_ACTION]: fetchProfilePictureSuccess,
  [Types.FETCH_PROFILE_PICTURE_FAILURE_ACTION]: fetchProfilePictureFailure,
  [Types.VERIFY_REQUEST_ACTION]: verifyRequest,
  [Types.VERIFY_SUCCESS_ACTION]: verifySuccess,
  [Types.VERIFY_FAILURE_ACTION]: verifyFailure,
  [Types.ON_VERIFICATION_COMPLETED_ACTION]: onVerificationCompleted,
  [Types.ON_CODE_AUTO_RETRIEVAL_TIMEOUT_ACTION]: onCodeAutoRetrievalTimeout,
  [Types.ACTIVATE_ACTION]: onActivate,
  [Types.ACTIVATE_REQUEST_ACTION]: noop,
  [Types.ACTIVATE_SUCCESS_ACTION]: onActivateSuccess,
  [Types.ACTIVATE_FAILURE_ACTION]: noop,
  [Types.SET_PROFILE_ACTION]: setProfile,
  [actionTypes.FILE_UPLOAD_PROGRESS]: updateProfilePictureProgress
})

/* ------------- Selectors ------------- */

const selectFirebase = ({ firebase }) => firebase

const selectProfileDomain = (state) => {
  return state.profile
}

export const makeSelectProfile = () => createSelector(
  selectFirebase,
  selectProfileDomain,
  (firebase, profileDomain) => {
    const { verificationId, data, isSet, isHydrated, error, image, form, activatedAt } = profileDomain
    let { isFetching } = profileDomain
    let isAuthenticated = false
    let hasProfile = true
    let isReady = false
    let isActivated = false
    let hasRated = false

    // Is firebase fetching profile?
    if (firebase.isInitializing) {
      isFetching = 'Initialiasing...'
    } else if (!firebase.auth.isLoaded && firebase.auth.isEmpty) {
      isFetching = 'Authenticating...'
    }

    // Is user authenticated?
    if (firebase.auth.isLoaded && !firebase.auth.isEmpty && firebase.auth.uid) {
      if (data && typeof data === 'object' && data.id) {
        isAuthenticated = true
      }
    }

    // Does user have a complete profile?
    if (!data || typeof data !== 'object') {
      hasProfile = false
    } else {
      for (let i = 0; i < profileFields.length; i += 1) {
        if (profileFields[i].required && !data[profileFields[i].key]) {
          hasProfile = false
        }
      }
    }

    // Is profile ready to be used for app logic?
    if (!isFetching && (isSet || isHydrated)) {
      isReady = true
    }

    if (activatedAt || (data && data.isActivated)) {
      isActivated = true
    }

    if (data && data.rated) {
      hasRated = true
    }

    return {
      isFetching,
      isAuthenticated,
      isHydrated,
      isSet,
      isReady,
      hasProfile,
      isActivated,
      hasRated,
      error,
      verificationId,
      form,
      image,
      data
    }
  }
)

export const makeSelectActions = (rules) => createSelector(
  makeSelectProfile(),
  (profile) => {
    const actions = []
    return actions
  }
)
