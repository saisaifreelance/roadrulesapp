import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import {REHYDRATE} from 'redux-persist/constants'

export const getData = () => {
    let notes = require('../Data/driving_schools').default.trim()
    const categories_raw = notes.split('||')

    return categories_raw.reduce((categories, category_raw, i) => {
        category_raw = category_raw.trim()
        if (!category_raw) {
            return categories
        }

        let start = category_raw.indexOf('**') + 2
        let stop = category_raw.indexOf('**', start)

        const title = category_raw.slice(start, stop)
        start = stop + 2
        stop = category_raw.indexOf('|', start)
        const text = category_raw.slice(start, stop).trim()

        const items_raw = category_raw.slice(stop).trim().split('|')
        const items = stop === -1 ? [] : items_raw.reduce((items, raw, i) => {
            const item_raw = raw.trim()
            if (!item_raw.trim())
                return items

            let start = item_raw.indexOf('**') + 2
            let stop = item_raw.indexOf('**', start)
            const title = item_raw.slice(start, stop)
            start = stop + 2

            let image = null
            if (item_raw.indexOf('![image', start) > 0) {
                start = item_raw.indexOf('![image', start)
                start = item_raw.indexOf('(', start) + 1
                stop = item_raw.indexOf(')', start)
                image = item_raw.slice(start, stop)
                start = stop + 1
            }

            const text = item_raw.slice(start).trim()

            items.push({
                id: 'item' + i,
                title,
                description: 'One of the best and most accomplished driving schools in the city',
                image,
                text,
                item_raw,
                city: 'Harare',
                rating: 5,
                latlng: {
                  latitude: -17.836388,
                  longitude: 31.041066,
                },
                cost: 8,
                contact: {
                  name: 'Ring Driving School',
                  phone: {
                    office: {
                      label: 'office',
                      value: '+263778029823'
                    }
                  },
                  whatsapp: {
                    office: {
                      label: 'office',
                      value: '+263778029823'
                    }
                  },
                  messenger: {
                    office: {
                      label: 'office',
                      value: 'ringdrivingschool'
                    }
                  },
                }
            })
            return items
        }, [])

        categories.push({
            id: "cat" + i,
            title,
            items
        })
        return categories
    }, [])
}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  drivingSchoolsRequest: ['data'],
  drivingSchoolsSuccess: ['payload'],
  drivingSchoolsFailure: null
})

export const DrivingSchoolsTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  categories: [],
  fetching: null,
  payload: null,
  error: null,
  drivingSchools: []
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const onRehydrate = (state, action) => {
  return Immutable({categories: getData()[0].items})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DRIVING_SCHOOLS_REQUEST]: request,
  [Types.DRIVING_SCHOOLS_SUCCESS]: success,
  [Types.DRIVING_SCHOOLS_FAILURE]: failure,
  [REHYDRATE]: onRehydrate
})

const makeSelectDrivingSchoolsDomain = () => ({ drivingSchools }) => drivingSchools.asMutable()

export const makeSelectDrivingSchools = () => createSelector(
  makeSelectDrivingSchoolsDomain(),
  (drivingSchoolsDomain) => {
    let categories = []
    try {
      categories = Array.isArray(drivingSchoolsDomain.categories) ? drivingSchoolsDomain.categories : []
    } catch (e) {}
    return {
      categories
    }
  })
