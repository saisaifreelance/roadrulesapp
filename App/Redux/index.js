import { combineReducers } from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    nav: require('./NavigationRedux').reducer,
    profile: require('./ProfileRedux').reducer,
    quiz: require('./QuizRedux').reducer,
    notes: require('./NotesRedux').reducer,
    drivingSchools: require('./DrivingSchoolsRedux').reducer,
    firebase: require('react-redux-firebase').firebaseStateReducer
  })

  return configureStore(rootReducer, rootSaga)
}
