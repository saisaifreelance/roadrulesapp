import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import {REHYDRATE} from 'redux-persist/constants'

const htmlNotes = {
  a: {
    id: "a",
    title: "INTRODUCTION TO DRIVING",
    description: "Road traffic safety issues are treated, and rightly so, as a matter of life-and-death! Driving is more than the movement of a vehicle from point A to point B...",
    html: `<h3 id="introduction-to-driving">INTRODUCTION TO DRIVING</h3>
    <p>Road traffic safety issues are treated, and rightly so, as a matter of life-and-death! Driving is more than the movement of a vehicle from point A to point B.</p>
    <p>Driving does not only necessarily imply being in control of the steering apparatus of a vehicle, it includes a variety of deliberations which involve cooperation; rational and very clear communication with fellow road users.</p>
    <p>Therefore, thorough knowledge and respect of road regulations, traffic signs and traffic signals are an <strong>absolute</strong> pre-requisite. Any aspiring and responsible driver <strong>MUST</strong> invest a lot of honest, dedicated hard work in mastering these pre-requisites and should be willing to have their competence put to scrutiny.</p>
    <p>Zimbabwe’s Highway Code (our road traffic rule book) rightly states that “……a vehicle is a good means of transport but a dangerous weapon in the hands of reckless people.” An unlicensed driver is evidently in the class of the <strong>reckless</strong>, and <strong>is a potential mass-murderer</strong>.</p>
    <p>To drive in Zimbabwe you must be in possession of a legally acquired Zimbabwean or other recognized driver’s license. In Zimbabwe, there is no other accepted qualification for driving other than the driver documents issued through the competent and prescribed Authorities which primarily are the Vehicle Inspectorate Department (VID) and the Central Vehicle Registry (CVR).</p>
    <p>If you do not have a legally recognized driver’s license, you must take out a learner driver’s license which allows you to drive on public roads to gain experience before undergoing the test for your full driver’s license. The learner’s license is only issued after you have passed a test on the rules of the road and the traffic signs and traffic signals.</p>
    <p>A learner’s license is used if and only you are accompanied by a holder of a certificate of competency as the minimum document.</p>
    <p><strong>NOTE:</strong> A public service vehicle <strong>CAN’T</strong> be driven using a learner’s license.</p>`
  },
  b: {
    id: "b",
    title: "LEARNER DRIVER’S LICENSE TEST",
    description: "A provisional driver’s license proves that the holder has acquired thorough knowledge of road regulations, signs and signals and has been authorised by the g...",
    html: `<h3 id="learner-driver’s-license-test">LEARNER DRIVER’S LICENSE TEST</h3>
    <p>A provisional driver’s license proves that the holder has acquired thorough knowledge of road regulations, signs and signals and has been authorised by the government to practically learn the skills and habits of manoeuvring a vehicle in conjunction with knowledge acquired. It proves and signifies that the holder of the license has gone through the driving examination and satisfactorily passed it.</p>
    <p>The test is administered by the Vehicle Inspectorate Department (VID) of the Ministry of Transport and Infrastructure Development of the Government of Zimbabwe at a cost of <strong>US$20.00</strong> per person per sitting. The fee is non-refundable even if your book for the test and do not show up to take the test and neither is it rolled over for you to write the test on the next available date.</p>
    <p>It’s a multiple choice questions test with twenty five (25) questions that are supposed to be answered within eight (8) minutes. The minimum score regarded as a pass mark is twenty two (22) out of the twenty five (25) test questions, i.e. 88 % for those who wish to get a driver’s license for Classes Two (2), Three(3), Four(4) and Five (5).</p>
    <p>For Class One (1), one must score 100% for this test, that is, they must get <strong>ALL (25) questions correct!</strong> For the Class One (1) Provisional License, one must hold a driver’s license which is five (5) years old, a Defensive Driving Certificate (DDC) and a special medical certificate.</p>
    <p>Before one goes to sit for the provisional driver’s license test, there is great need for one to study the Highway Code to gain a deep understating of the Zimbabwean driving rules. There is also a special need to undertake practice exam questions to prepare specifically for this theory test.</p>
    <p>When going through the two materials, (Highway Code &amp; Practice Questions) one becomes accustomed to the general road rules and gets insight on how to overcome driving situations that will be presented and tested in the exam.</p>
    <p>According to <strong>VID 2014 statistics</strong> about <strong>60%</strong> of the people who sit for this exam <strong>fail the 1<strong><strong>st</strong></strong> time</strong> and most people pass well after the 2nd sitting. It is therefore very important to thoroughly prepare for this test. It is for this purpose that we have created this app to help you in your noble efforts to become a legal, responsible and safe driver on Zimbabwe’s roads.</p>
    <p><strong>Minimum Requirements for the Provisional Driver’s License Test.</strong></p>
    <ol>
    <li>
    <p>2 x (25 x 30)mm black and white driver’s license photos</p>
    </li>
    <li>
    <p>Every driver’s license held by applicant. (Class 1 exclusive)</p>
    </li>
    <li>
    <p>Documentary proof of identity (Valid and <strong>NOT Expired</strong> Passport / National ID)</p>
    </li>
    <li>
    <p>Exam fee of US$20.00</p>
    </li>
    </ol>`
  },
  c: {
    id: "c",
    title: "TEST SYLLABUS",
    description: "he Zimbabwe provisional driver’s license test curriculum consists of the following areas:...",
    html: `<h3 id="test-syllabus">TEST SYLLABUS</h3>
    <p>The Zimbabwe provisional driver’s license test curriculum consists of the following areas:</p>
    <ol>
    <li>Rules of the road (general road rules).</li>
    <li>The different Classes of vehicles in Zimbabwe.</li>
    <li>Documents required for the driver in Zimbabwe (Public Transport Driver and General Driver).</li>
    <li>Documents required for vehicles and specifications (Public Transport Driver and General Driver).</li>
    <li>Driving Requisites in Zimbabwe.</li>
    </ol>
    <ul>
    <li>Prestart checks and vehicle inspection</li>
    <li>Lane use, Speed Limits</li>
    <li>Night driving, Overtaking</li>
    <li>Traffic signs and signals</li>
    <li>Hand signals</li>
    <li>Reasoning and day to day practical driving situations.</li>
    </ul>
    <ol start="6">
    <li>Hazardous Conditions</li>
    </ol>
    <ul>
    <li>Turning left or right at robot (traffic light)controlled intersections.</li>
    <li>Parking vehicle/stopping vehicle outside theroad.</li>
    <li>Driving on an upgrade or downgrade</li>
    <li>Driving under the influence of alcohol</li>
    <li>Bad weather and Road Conditions</li>
    </ul>
    <ol start="7">
    <li>The Highway Code, General Knowledge of the application of the Highway Code for all vehicles, Zimbabwe Traffic Legislation.</li>
    </ol>`
  },
  d: {
    id: "d",
    title: "ZIMBABWE’S ROAD RULES",
    description: "There are four fundamental rules of the road covered in the provisional license test...",
    html: `<h3 id="zimbabwe’s-road-rules">ZIMBABWE’S ROAD RULES</h3>
    <p>There are four fundamental rules of the road covered in the provisional license test.</p>
    <h4 id="rule--1">Rule # 1:</h4>
    <p>The Zimbabwean rule of the road states that vehicles should keep well left and give way to traffic on your right hand side.</p>
    <h5 id="diagram-1">Diagram #1</h5>
    <p><img src="image_0.jpg" alt="image alt text"></p>
    <p><strong>Car B</strong> goes 1st.</p>
    <p>According to (<strong>Rule #1</strong>) “give way to traffic approaching from the road on your right”</p>
    <p><strong>Car A</strong> gives way to <strong>Car B</strong>, therefore <strong>Car B</strong> goes 1st because he has no traffic on his right.</p>
    <h4 id="rule--2">Rule # 2:</h4>
    <p>Never turn right in front of oncoming traffic.</p>
    <h5 id="diagram--2">Diagram # 2</h5>
    <p><img src="image_1.jpg" alt="image alt text"></p>
    <p>According to (<strong>Rule #1</strong>) “give way to traffic approaching from the road on your right”</p>
    <p><strong>Car C</strong> goes 1st, <strong>Car B</strong> goes 2nd and <strong>Car A</strong> goes last</p>
    <p><strong>Car A</strong> gives way to <strong>Car B</strong> and <strong>Car B</strong> gives way to <strong>Car C. Car C</strong> should go 1st**provided he is not **turning right in-front of oncoming traffic. If a vehicle with traffic on its right hand side does not have the intention of turning right in front of oncoming traffic, it can proceed straight ahead. <strong>Car B</strong> goes 2nd since he has no traffic on his right after <strong>Car C</strong> has gone. Thereafter <strong>Car A</strong> finally goes last.</p>
    <h4 id="rule--3">Rule # 3:</h4>
    <p>In rural areas give way to traffic that has entered the intersection first.</p>
    <h5 id="diagram--3">Diagram # 3</h5>
    <p><img src="image_2.jpg" alt="image alt text"></p>
    <p><strong>Car C</strong> has nothing on its right but intends to turn right in front of oncoming traffic. <strong>Car C</strong> should not break <strong>Road Rule # 2</strong> “never turn right in front of oncoming traffic”.</p>
    <p>Therefore **Car A **which is classified as “oncoming traffic” goes 1st, then <strong>Car C</strong> can follow and finally <strong>Car B</strong> goes last.</p>
    <h5 id="frequently-asked-question-by-students-on-this-diagram">Frequently Asked Question by Students on This Diagram</h5>
    <p><strong>Q:</strong> Why does <strong>Car A</strong> go 1st and yet it has <strong>Car B</strong> on its right hand side if we always say "give way to traffic on your right hand side?<br>
    <strong>A:</strong> <strong>Car C</strong> gives <strong>Car A</strong> the right to go first so that he (<strong>Car C</strong>) can be able to turn right without him (<strong>Car C</strong>) breaking the law (Road Rule # 2 which says “never turn right in front of oncoming traffic”. By so doing <strong>Car C</strong> moves to the middle of the road and blocks <strong>Car B</strong> as <strong>Car A</strong> is passing.</p>
    <p>After <strong>Car A</strong> has passed, <strong>Car C</strong> turns right and lastly <strong>Car B</strong> then turns right lastly.</p>
    <h4 id="rule--4">Rule # 4:</h4>
    <p>At traffic circles or roundabouts, give way to traffic already circulating.</p>
    <h4 id="general-notes">General Notes:</h4>
    <ul>
    <li>When reversing, a seat belt is not necessary.</li>
    <li>Vehicle being driven should display L - Plates at the front and rear of the vehicle when approaching vehicle displaying L Plates extreme caution.</li>
    </ul>
    <h4 id="controlled-intersections">Controlled Intersections</h4>
    <p>The general rule is that <strong>the vehicle facing a control sign to its left side should stop or give way to all crossing traffic.</strong></p>
    <h5 id="diagram--4">Diagram # 4</h5>
    <p><img src="image_3.jpg" alt="image alt text"></p>
    <p>Under normal circumstances (in the absence of the control sign), <strong>Car A</strong> should give way to <strong>Car B</strong> because <strong>Car B</strong> is to his right.</p>
    <p>However, in the diagram above <strong>Car B</strong> has been controlled / regulated by the presence of that sign, therefore <strong>Car A</strong> goes first. The sign supersedes the rule of giving way to traffic on your right.</p>`
  },
  e: {
    id: "e",
    title: "VEHICLE CLASSES",
    description: "Having understood the rules on Zimbabwe’s roads, it is equally important that one makes a decision on which class of vehicle does one wish to attain a driver...",
    html: `<h3 id="vehicle-classes">VEHICLE CLASSES</h3>
    <p>Having understood the rules on Zimbabwe’s roads, it is equally important that one makes a decision on which class of vehicle does one wish to attain a driver’s license for.<br>
    <strong>There are five (5) Classes of vehicle licenses in Zimbabwe</strong></p>
    
    <table>
    <thead>
    <tr>
    <th>License Class</th>
    <th>Vehicle Type</th>
    <th>Minimum Age</th>
    <th>Other Requirements</th>
    </tr>
    </thead>
    <tbody>
    <tr>
    <td>Class 1</td>
    <td>Buses, Commuter Omnibus, Train, Trailer Bus (Artico)</td>
    <td>25 years</td>
    <td>Should have a Medical Certificate &amp; should already have a License and a Defensive Driving Certificate</td>
    </tr>
    <tr>
    <td>Class 2</td>
    <td>Heavy Vehicles, Heavy Trucks, Ambulance, Goods Vehicles, Hearse, Breakdown Vehicles (Tow Trucks)</td>
    <td>18 years</td>
    <td></td>
    </tr>
    <tr>
    <td>Class 3</td>
    <td>Motor Cycles, Motor Bikes, Scooters, Vasolex</td>
    <td>16 years</td>
    <td></td>
    </tr>
    <tr>
    <td>Class 4</td>
    <td>Light Motor Vehicles</td>
    <td>16 years</td>
    <td></td>
    </tr>
    <tr>
    <td>Class 5</td>
    <td>Farming Equipment &amp; Construction Vehicles which do not qualify to be classified as class 1, 2, 3 or 4 vehicles.</td>
    <td>18 years</td>
    <td></td>
    </tr>
    </tbody>
    </table><p>Holders of a provisional license can only attempt Class One (1) and Class Two (2) at the Age of 18 and above. A Class One (1) driver’s license is for people with <strong>a driver’s license already, a medical certificate and a defensive driving certificate.</strong></p>`
  },
  f: {
    id: "f",
    title: "VEHICLE DOCUMENTATION",
    description: "Different types or classes of vehicles have different type of road documentation required of them. The following are the two common types of vehicle classes...",
    html: `<h3 id="vehicle-documentation">VEHICLE DOCUMENTATION</h3>
    <p>Different types or classes of vehicles have different type of road documentation required of them. The following are the two common types of vehicle classes.</p>
    <ul>
    <li><strong>General Motor Vehicle</strong></li>
    <li><strong>Public Transportation Vehicle</strong></li>
    </ul>
    <p>It is <strong>MANDATORY</strong> that a vehicle be licensed before advancing onto the road. Driving an unlicensed vehicle is an offence.</p>
    <h4 id="general-motor-vehicle">General Motor Vehicle</h4>
    <ol>
    <li>Vehicle registration book</li>
    <li>Vehicle insurance (4 months)</li>
    <li>Vehicle license (4 months)</li>
    </ol>
    <p>The above are the minimum license and insurance terms required by law even though the owner has an option to license and insure their vehicle for longer terms.</p>
    <h4 id="public-transportation-vehicle">Public Transportation Vehicle</h4>
    <p>Public transportation vehicles require all of the above <strong>plus a certificate of fitness and a route authority certificate</strong>. A certificate of fitness is granted to a vehicle that proves to be roadworthy.</p>
    <ol start="4">
    <li>Certificate of fitness (6 months) - For buses the certificate of fitness is valid for six (6) months and for general vehicles it is valid for a period not exceeding one year (12) months.</li>
    <li>Route Authority for passenger transport.</li>
    </ol>`
  },
  g: {
    id: "g",
    title: "DRIVER’S LICENSE CLASSES",
    description: "The available combinations of Driver’s License Classes in Zimbabwe are as follows...",
    html: `<h3 id="driver’s-license-classes">DRIVER’S LICENSE CLASSES</h3>
    <p>The available combinations of Driver’s License Classes in Zimbabwe are as follows;</p>
    <ul>
    <li>Class 3 and 5<br>
    <strong>Note:</strong> You are not given a class (5) driver’s license if you are below the age of eighteen (18) even if you have qualified for it, its withheld and endorsed later on the disc when you turn 18.</li>
    <li>Cass 4 and 5</li>
    <li>Class 2, 4 and 5</li>
    <li>Class 1, 2, 4 and 5</li>
    </ul>
    <p>When a person is issued with a class two (2) or class four (4) license, class five (5) is offered freely. When issued with a class two (2) license, classes four (4) and five (5) are issued freely. Class one (1) is offered to <strong>ALREADY</strong> licensed drivers only.</p>
    <p>A holder of a class four (4) license should have a minimum of five (5) years driving experience to attempt for class one (1). A holder of class two (2) license should have a minimum of a year driving experience to attempt for class one (1).</p>
    <p>However there is a legislative amendment of 2006 that stipulates a (5) year driving experience requirement for all classes which wishes to attempt for the class one (1) license.</p>`
  },
  h: {
    id: "h",
    title: "REQUIRED DOCUMENTATION",
    description: "Different types or classes of vehicles have different type of documents required of them. The following are the two common types of vehicle classes...",
    html: `<h3 id="required-documentation">REQUIRED DOCUMENTATION</h3>
    <p>Different types or classes of vehicles have different type of documents required of them. The following are the two common types of vehicle classes.</p>
    <ul>
    <li>General Motor Vehicle Driver</li>
    <li>Public Transportation Vehicle Driver</li>
    </ul>
    <h4 id="general-motor-vehicle-driver">General Motor Vehicle Driver</h4>
    <p>A provisional / learner driver’s license or a full driver’s license. Holders of a provisional driver’s license <strong>MUST</strong> to be accompanied by person with a driver’s license and are **NOT **allowed to carry passengers.</p>
    <h4 id="public-transportation-vehicle-driver">Public Transportation Vehicle Driver</h4>
    <p>The minimum age for a public vehicle driver is twenty five (25) years. Public vehicle drivers are required to have:</p>
    <ul>
    <li>Driver’s license</li>
    <li>Medical Certificate valid for 12 months</li>
    <li>Defensive Drivers Certificate (DDC) valid for 48 months.</li>
    <li></li>
    </ul>
    <p>Persons driving a public vehicle should have their license renewed after every five (5) years. Holders of a provisional driver’s license are <strong>NOT</strong> allowed to drive public transportation vehicles.</p>
    <h4 id="tractor-drivers">Tractor Drivers</h4>
    <p>Tractor drivers are to produce a tractor’s permit when driving.</p>
    <p>A <strong>registered</strong> farm owner or registered mine owner can apply for a tractor’s permit for his or her employee for the purposes of driving the tractor within the farm during the purpose of use within the farm and execution of his duties.</p>
    <p>This kind of permit works within 10 km from the farm boundaries that is 10 km radius from the farm perimeter. The permit only works for that particular employer and it **DOES NOT **work in areas under the jurisdiction of local authority or municipal council.</p>`
  },
  i: {
    id: "i",
    title: "PERSONS WITH DIASABILITIES",
    description: "Having a disability does not preclude one from obtaining a driver’s license in Zimbabwe. If a person living with a disability intends to sit for the provisio...",
    html: `<h3 id="persons-with-diasabilities">PERSONS WITH DIASABILITIES</h3>
    <p>Having a disability does not preclude one from obtaining a driver’s license in Zimbabwe. If a person living with a disability intends to sit for the provisional driver’s license test, they should first see a VID approved medical practitioner who will complete a medical form for them. The medical form is obtained from the VID offices.</p>
    <p>The medical practitioner will assess the condition of the person and determine whether or not they are fit to drive a motor vehicle and if they are fit to drive, they endorse the conditions under which the person can drive.</p>
    <p>Many optical problems or challenges do not necessarily need this type of medical assessment but it is essential to notify the Examination Officer of any eye problems or challenges if the challenges are to do with corrective lenses. This waiver does not apply in cases of a missing eye or deformed eyes.</p>
    <p>After receiving a medical report and having passed the provisional driver’s license test, the examiner will endorse on the provisional license certificate the condition under which you should drive. This may appear as a code particularly Code (2) for a physical disability like corrective lenses and Code (1) for a condition that may need one to drive a specially adapted vehicle.</p>`
  },
  j: {
    id: "j",
    title: "ROAD LANE USE",
    description: "It is important that every driver knows in which lane to drive. In Zimbabwe drivers keep to the far left of the road, meaning we use the left lane...",
    html: `<h3 id="road-lane-use">ROAD LANE USE</h3>
    <p>It is important that every driver knows in which lane to drive. In Zimbabwe drivers keep to the far left of the road, meaning we use the left lane.</p>
    <h5 id="diagram--5">Diagram # 5</h5>
    <p><img src="image_4.jpg" alt="image alt text"></p>
    <h5 id="diagram--6">Diagram # 6</h5>
    <p><img src="image_5.jpg" alt="image alt text"></p>
    <p>The left lane can be subdivided into two or three other lanes. Vehicles <strong>moving at a slow speed should use the left lane at all times.</strong></p>
    <h5 id="diagram--7">Diagram # 7</h5>
    <p><img src="image_6.jpg" alt="image alt text"></p>
    <h4 id="three-lane-road">Three Lane Road</h4>
    <p>In a three lane road - vehicles turning left should use the <strong>LEFT</strong> lane. When going straight, use the centre lane.</p>
    <h5 id="diagram--8">Diagram # 8</h5>
    <p><img src="image_7.jpg" alt="image alt text"></p>
    <h4 id="changing-lanes">Changing lanes</h4>
    <p>When changing lanes it is important to check first if it is safe to do so and then indicate your intention before moving to the intended lane.</p>
    <h5 id="procedure-of-changing-lanes">Procedure of changing lanes</h5>
    <ol>
    <li>Check mirrors and blind spot.</li>
    <li>Indicate your intention using light indicators and hand signals.</li>
    <li>If safe, move into intended lane quickly but swiftly.</li>
    </ol>`
  },
  k: {
    id: "k",
    title: "SPEED LIMITS",
    description: "Over speeding is the highest contributing factor in most traffic accidents. In as much as speed thrills it is also important to understand that speed kills...",
    html: `<h3 id="speed-limits">SPEED LIMITS</h3>
    <p>Over speeding is the highest contributing factor in most traffic accidents. In as much as speed thrills it is also important to understand that speed kills.</p>
    <p>When driving in a small town and urban areas the **general speed limit is 60 km/hr **and the <strong>maximum speed limit is 80km/hr</strong></p>
    <p>The maximum in Zimbabwe varies depending on the class of vehicle. It depends on the type of vehicle being driven. However the <strong>GENERAL maximum limit</strong> in Zimbabwe is 80 km / hr for heavy vehicles 120km/hr for light vehicles.</p>
    <h4 id="differential-speed">Differential Speed</h4>
    <p>Differential speed is the relationship between the speed of a vehicle and gap maintained between one vehicle and the vehicle in front. When travelling at 15km/ hr a vehicle should leave a gap equivalent to the length one vehicle in front of us.</p>
    <p><strong>Therefore:</strong><br>
    30km / hr : 2 car gap, 45 km/ hr : 3 car gap, 60km/hr : 4 car gap, 75km/hr : 5 car gap and so forth. For every 15km/hr difference you leave one car gap and incrementally in that pattern.</p>
    <h4 id="total-stopping-distance">Total Stopping Distance</h4>
    <p>The distance covered whilst trying to stop a vehicle is called the total stopping distance. This is determined using a vehicle in / assuming that a vehicle is in a roadworthy condition. Defective brakes alter the values.</p>
    <p>Most Accidents are due to a vehicle failing to stop in time. It is important for a driver to know the distance covered by a vehicle before it comes to a complete halt (stop) when emergency brakes have been applied.</p>
    <p>The total stopping distance can be calculated by adding the <strong>Reaction Distance</strong> and the <strong>Braking Distance.</strong></p>
    <p>Reaction Distance + Braking Distanc = <strong>Total Stopping Distance (R+B = S)</strong></p>
    <p><strong>Reaction Distance</strong> is the distance covered whilst reacting to a situation. Reaction distance is calculated as the distance covered from the time of a hazardous situation occurring to the</p>
    <p>time just before the driver steps on the brakes. The time taken by the driver to step on the brakes measured from the time the when the driver realised he /she needed to apply the brakes</p>
    <p><strong>Braking Distance</strong> is the distance covered by the vehicle from the time the brakes are applied to the moment the vehicle stops /comes to a complete halt. This varies depending on the conditions of the road. In wet weather the distances becomes much longer. However, under normal road conditions, the following are considered to be correct;</p>
    <p><strong>When travelling at 40 km/hr</strong></p>
    <ul>
    <li>Reaction Distance = 5,6m</li>
    <li>Braking Distance = 12, 4 m</li>
    <li>Total stopping distance = 18m</li>
    </ul>
    <p><strong>When travelling at 60 km/ hr</strong></p>
    <ul>
    <li>Reaction Distance = 8,3m</li>
    <li>Braking Distance = 27,7m</li>
    <li>Total stopping distance = 36m</li>
    </ul>
    <p><strong>When travelling at 120 km/hr</strong></p>
    <ul>
    <li>Reaction Distance = 16,7m</li>
    <li>Braking Distance = 113,3m</li>
    <li>Total stopping distance = 130m</li>
    </ul>`
  },
  l: {
    id: "l",
    title: "NIGHT DRIVING",
    description: "In Zimbabwe’s driving legislation, night driving is regarded as driving between **1730 in the evening to 0600 **the following morning...",
    html: `<h3 id="night-driving">NIGHT DRIVING</h3>
    <p>In Zimbabwe’s driving legislation, night driving is regarded as driving between **1730 in the evening to 0600 **the following morning.</p>
    <p>When driving at night, drivers should exercise extreme caution and skill. Visibility is greatly reduced when driving in the dark hence the need to drive slowly.</p>
    <h4 id="driving-with-the-aid-of-vehicle-lights--headlights">Driving with the aid of vehicle lights / headlights</h4>
    <p>It is <strong>mandatory</strong> that headlights be switched on between <strong>5:30 pm and 6:00 am</strong>. Headlights should be checked and cleaned before advancing on the road. Vehicle lighting should be easily accessible and comfortable to a driver.</p>
    <p><strong>Motorcyclists</strong> are to switch on their lights <strong>at anytime of the day</strong> when traveling on the road. Perfect lighting of a vehicle should allow an increase or decrease in intensity. An increase is called** flashing** and a decrease is called <strong>dipping</strong>.</p>
    <p>When driving at night, vision is impaired, so it is the norm to flash lights. There are certain situations though that require light be dipped so as to avoid dazzling other road users with our lights.</p>
    <h4 id="conditions-of-dipping-your-lights">Conditions of dipping your lights</h4>
    <ul>
    <li>In a properly lit street.</li>
    <li>In front t of oncoming traffic.</li>
    <li>In front policemen controlling traffic at night</li>
    <li>When following behind another vehicle at night</li>
    </ul>
    <p>When dazzled by the lights of oncoming traffic it is advised to slow down and cast your eyes slightly to the left.</p>`
  },
  m: {
    id: "m",
    title: "OVERTAKING",
    description: "Overtaking is the act of passing a vehicle with the intention of going ahead. At one point in driving, every driver is faced with the need to overtake the ve...",
    html: `<h3 id="overtaking">OVERTAKING</h3>
    <p>Overtaking is the act of passing a vehicle with the intention of going ahead. At one point in driving, every driver is faced with the need to overtake the vehicle in front.</p>
    <p>Overtaking can easily and dangerously become an obsession and that urge should fought if its unnecessary. When overtaking, driver should exercise extreme caution. We overtake using the right side, <strong>BUT</strong> in situations whereby the vehicle being overtaken is turning right, we use the left side to overtake.</p>
    <p>There is no specific rule on overtaking an animal drawn wagon. For animal drawn wagons, assessment on the safest side to use should be done before overtaking i.e. drivers use whichever side is safe to do so.</p>
    <h4 id="conditions-suitable-to-overtake">Conditions Suitable to Overtake</h4>
    <ul>
    <li>Give hand signals</li>
    <li>Check blind spots</li>
    <li>Check ahead and behind.</li>
    <li>Indicate your intention using light indicators.</li>
    <li>Move over to the right if it is clear</li>
    <li>Accelerate and tap horn</li>
    <li>When ahead, indicate to return to the left lane</li>
    <li>Resume normal speed.</li>
    </ul>
    <p>When being overtaken, <strong>DO NOT</strong> increase speed.</p>
    <h4 id="conditions-when-not-to-overtake">Conditions when NOT to Overtake</h4>
    <ul>
    <li>At a blind rise</li>
    <li>Ahead of a corner</li>
    <li>At a narrow bridge</li>
    <li>At a railroad crossing</li>
    <li>At a pedestrian crossing</li>
    <li>In front of oncoming traffic</li>
    <li>Ahead of road intersections</li>
    <li>When forbidden by carriageway markings</li>
    <li>When being overtaken by another vehicle</li>
    <li>When visibility is bad e.g. raining, misty or dusty</li>
    <li>Vehicles that have slowed down to give way to other traffic or pedestrians</li>
    </ul>
    <p>A <strong>Blind Spot</strong> is an area not visible using view mirrors. A **Blind Rise **is an area uphill where the driver is not able to clearly see or anticipate oncoming traffic.</p>`
  },
  n: {
    id: "n",
    title: "TRAFFIC SIGNS & SIGNALS",
    description: "There is constant communication between drivers and the road they travel on. Zimbabwean roads have been designed to caution, inform and regulate road users a...",
    html: `<h3 id="traffic-signs--signals">TRAFFIC SIGNS &amp; SIGNALS</h3>
    <p>There is constant communication between drivers and the road they travel on. Zimbabwean roads have been designed to caution, inform and regulate road users at all times. This communication is done through the display of traffic signs and signals.</p>
    <h4 id="there-are-five-5-classes-of-traffic-signs-and-signals">There are five (5) classes of traffic signs and signals</h4>
    <ol>
    <li>Class A – Danger Warning Signs. These are always identified by the triangle or a combination with a triangle diagram below.</li>
    </ol>
    <h5 id="diagram--9">Diagram # 9</h5>
    <p><img src="image_8.jpg" alt="image alt text"></p>
    <ol start="2">
    <li>Class B – Regulatory/ Mandatory signs. These are always identified by the circle or a combination with a circle diagram below.</li>
    </ol>
    <h5 id="diagram--10">Diagram # 10</h5>
    <p><img src="image_9.jpg" alt="image alt text"></p>
    <ol start="3">
    <li>Class C – Informative Signs. These are always identified by the rectangle or a combination with a rectangle diagram below.</li>
    </ol>
    <h5 id="diagram--11">Diagram # 11</h5>
    <p><img src="image_10.jpg" alt="image alt text"></p>
    <ol start="4">
    <li>Class D – Traffic Lights</li>
    </ol>
    <h5 id="diagram--12">Diagram # 12</h5>
    <p><img src="image_11.jpg" alt="image alt text"></p>
    <ol start="5">
    <li>Class E – Carriageway markings</li>
    </ol>
    <h5 id="diagram--13">Diagram # 13</h5>
    <p><img src="image_12.jpg" alt="image alt text"></p>
    <h5 id="diagram--14">Diagram # 14</h5>
    <p><img src="image_13.jpg" alt="image alt text"></p>
    <p>It is of great importance that all drivers familiarize with all traffic signs. Through the identification of insignia, it becomes fairly simple to know which action is required when driving in particular environments.</p>
    <ul>
    <li>For areas with danger warning signs, extreme caution is necessary</li>
    <li>Environments with regulatory signs require strict compliance to law being displayed or governed.</li>
    <li>Informative Signs merely inform the driver of locations, directions and names of environments.</li>
    </ul>
    <h4 id="insignia-symbols">Insignia (Symbols)</h4>
    <p>Every class has its own symbol which can assist in identifying and categorizing each sign to its appropriate group. The following are insignias for traffic signs.</p>
    <h4 id="robots-traffic-lights">Robots (Traffic Lights)</h4>
    <p>Robots have three colours.</p>
    <h5 id="diagram--16">Diagram # 16</h5>
    <p><img src="image_14.jpg" alt="image alt text"></p>
    <p>Note the order of the descending and ascending arrow is referred to as the <strong>correct sequence of a traffic light</strong>.</p>
    <ul>
    <li><strong>Green</strong>; Proceed with caution</li>
    <li><strong>Amber</strong>; Stop unless it is not safe to do so</li>
    <li><strong>Red</strong>; Stop</li>
    </ul>
    <p>Traffic Lights can also be referred to as a sequence. When the robot light is not functioning correctly, it displays flashing amber light.</p>
    <p><strong>The Sequence</strong>of a traffic light (robot) assumes that the red light is at the top and therefore is the first light on the robot.</p>
    <p><strong>The Correct Sequence</strong>of a robot states that the robot starts on the green light.</p>
    <p>The correct sequence of a robot has a more organized flow compared to the sequence of a robot which is drafted in a haphazard manner.</p>
    <p>When a robot doesn’t have power, it is just as good as flashing amber, therefore traffic from the right must be given way.</p>
    <p>When turning right or left at a robot controlled intersection, drivers should give way to crossing pedestrians. According to the Highway Code, pedestrians have a right of way all the time.</p>
    <p><strong>Q:</strong> What must you do at a flashing amber robot?<br>
    <strong>A:</strong> Give way to traffic from the right.</p>
    <p><strong>Q:</strong> When are you allowed to go through a red robot?<br>
    <strong>A:</strong> When the red sequence is accompanied by a green arrow point in the direction of your travel, even if there is a police officer.</p>
    <h4 id="class-d-traffic-lights">Class D Traffic Lights</h4>
    <p>Class D is for all information displayed in the form of lights. There are two types of traffic lights</p>
    <ul>
    <li>Traffic Lights (Robots)</li>
    <li>Traffic Signs in conjunction with traffic lights- they emphasize the <strong>existence of danger ahead</strong>, e.g. when the robot is red, it moves next to green and then comes back to amber.</li>
    </ul>
    <h5 id="diagram--17">Diagram # 17</h5>
    <p><img src="image_16.jpg" alt="image alt text"></p>
    <h5 id="diagram--18">Diagram # 18</h5>
    <p><img src="image_15.jpg" alt="image alt text"></p>
    <h4 id="class-e-carriageway-markings">Class E Carriageway Markings</h4>
    <p>Class E is for Information displayed on road surfaces.</p>
    <h4 id="types-of-carriageway-markings">Types of carriageway markings</h4>
    <ul>
    <li>Direction arrows</li>
    <li>Longitudinal lines, e.g. broken white lines, continuous white line</li>
    <li>Pedestrian crossing lines</li>
    <li>Stop Line/ Traverse line</li>
    <li>Broken yellow lines – mark the edge of the road.</li>
    </ul>
    <p>Longitudinal lines demarcate the centre of the road.</p>
    <h5 id="diagram--19">Diagram # 19</h5>
    <p><img src="image_17.jpg" alt="image alt text"></p>
    <h5 id="diagram--20">Diagram # 20</h5>
    <p><img src="image_18.jpg" alt="image alt text"></p>
    <h4 id="exceptional-signs-without-group-insignia">Exceptional Signs Without Group Insignia</h4>
    <p>There are three (3) special signs which do not carry group insignias.</p>
    <h5 id="diagram--21">Diagram # 21</h5>
    <p><img src="image_19.jpg" alt="image alt text"></p>
    <h5 id="diagram--22">Diagram # 22</h5>
    <p><img src="image_20.jpg" alt="image alt text"></p>
    <h5 id="diagram--23">Diagram # 23</h5>
    <p><img src="image_21.jpg" alt="image alt text"></p>`
  },
  o: {
    id: "o",
    title: "DRIVING ON AN UPHILL OR DOWNHILL",
    description: "For any vehicle, it is important to understand the concept of gear changing...",
    html: `<h3 id="driving-on-an-uphill-or-downhill">DRIVING ON AN UPHILL OR DOWNHILL</h3>
    <p>For any vehicle, it is important to understand the concept of gear changing.</p>
    <p><strong>Q:</strong> Why do we change gears?<br>
    <strong>A:</strong> We change gears so as to have enough power to suit the conditions of the road or to reach the desired speed.</p>
    <p>Effort should only be used when required - lower gears give a lot of effort, whilst high gears produce less effort. Lower gears are also known as heavy gears due to the fact that they are heavy on the engine and end up reducing engine speed.</p>
    <p>When starting to move off, the vehicle is heavy and therefore requires a lower gear, a gear that gives a lot of power. As the vehicle is moving less effort is needed, meaning higher gears are engaged into. When driving on an upgrade (uphill), a lot of effort is require so engage to lower gear.</p>
    <p>When driving on a downgrade (downhill) also engage a lower gear. As stated earlier lower gears reduce engine speed and therefore vehicle speed is also reduced on a downgrade.</p>`
  },
  p: {
    id: "p",
    title: "SPECIAL VEHICLES",
    description: "In Zimbabwe we have special vehicles that have a right of way above all vehicles. These vehicles can claim that right of way provided they are sounding their...",
    html: `<h3 id="special-vehicles">SPECIAL VEHICLES</h3>
    <p>In Zimbabwe we have special vehicles that have a right of way above all vehicles. These vehicles can claim that right of way provided they are sounding their siren. Examples Include ambulances, fire brigade trucks, police vehicles and state motor vehicles (motorcade).</p>
    <p>An animal drawn wagon is the only special vehicle that does not have a siren.</p>`
  },
  q: {
    id: "q",
    title: "HAZARDOUS CONDITIONS",
    description: "There are certain areas when driving that require extreme caution. These areas include:...",
    html: `<h3 id="hazardous-conditions">HAZARDOUS CONDITIONS</h3>
    <p>There are certain areas when driving that require extreme caution. These areas include:</p>
    <ul>
    <li>Sharp curves</li>
    <li>Changing lanes</li>
    <li>Parking at a corner</li>
    <li>Obstructions in the road</li>
    <li>Driving on a slippery road</li>
    <li>Dusty, wet, misty conditions</li>
    <li>Approaching an accident scene</li>
    <li>Driving with passengers onboard</li>
    <li>Driving on an upgrade or downgrade.</li>
    <li>Driving under extreme humid conditions</li>
    <li>Driving in front of vehicle sounding a siren.</li>
    <li>Driving past a vehicle on the side of the road</li>
    <li>Driving under the influence of alcohol or drugs</li>
    <li>Turning left or right at a robot controlled intersection</li>
    </ul>
    <h4 id="driving-past-a-vehicle-parked-on-the-side-of-the-road">Driving past a vehicle parked on the side of the road</h4>
    <p>When driving past a parked vehicle, reduce speed and exercise caution, the drivers might just venture into the road whilst you are passing and cause a collision</p>
    <h4 id="driving-on-a-slippery-road">Driving on a slippery road</h4>
    <p>Drive at a low speed when on a slippery road and use extreme left side of the road.</p>
    <h4 id="driving-under-extreme-humid-conditions">Driving under extreme humid conditions</h4>
    <p>In humid conditions, it is extremely difficult to see other road users. When driving under such conditions, make sure headlights are on at all times even after 06:30 am. When driving in foggy or misty conditions, fog lights should be on.</p>
    <h4 id="obstructions-in-the-road">Obstructions in the Road</h4>
    <p>In situations where a drier is faced with an obstruction, it is important that the assessment is done on the correct action to take. It is not necessary to stop unless your hazard perception requires you to. In such situations, move over to a clear and safe side and drive off. Give way to oncoming traffic when forced to use restricted space use restricted space.</p>
    <h4 id="approaching-an-accident-scene">Approaching an accident scene</h4>
    <p>Accident scenes should be treated with extreme caution when driving and if you come across an accident scene slow down and look out for obstructions. Obstructions could be:</p>
    <ul>
    <li>Car wreckage</li>
    <li>Injured people</li>
    <li>Dead Bodies</li>
    <li>Other vehicles assisting on the scene</li>
    <li></li>
    </ul>
    <p>If your intention is to stop and help then make sure your vehicle is parked in a very safe place and that it doesn’t obstruct traffic.You should a􀆩end to the injured immediately and protect the bodies of the deceased.</p>
    <p>If your vehicle had been involved in the accident it is mandatory that you report to the police within 24 hours. Submission of documents should be done within 7 days from the day of the accident.</p>
    <h4 id="duties-of-a-driver-after-an-accident">Duties of A Driver After An Accident</h4>
    <ul>
    <li>Guard the corpse in one is killed</li>
    <li>Furnish police with the required information</li>
    <li>Rendering possible help that can be needed</li>
    <li>Report to police within 24 hours</li>
    </ul>
    <h4 id="driving-under-the-influence-of-drugs-and-alcohol">Driving under the influence of drugs and alcohol</h4>
    <p>Drunk drivers are <strong>NOT</strong> allowed behind the steering wheel. If vision is impaired by the use of alcohol or any drug, stay off the road.</p>
    <h4 id="passengers">Passengers</h4>
    <p>When driving, passengers should be seated and loud noises should be avoided. Children of the age 5 and below are not allowed to sit in front. Goods trucks and vehicles are not permitted to carry passengers. Vasolex, tractors and caterpillars are also not permitted to carry passengers.</p>
    <h4 id="parking-at-a-corner">Parking at a corner</h4>
    <p>It is not permitted to park a vehicle at a corner, it is **mandatory **to park 7,5 m from a corner.</p>`
  }
}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  notesRequest: ['data'],
  notesSuccess: ['payload'],
  notesFailure: null
})

export const NotesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  htmlNotes,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) => {
  console.log("NotesRedux>reques>datat", data)
  return state.merge({ fetching: true, data, payload: null })
}

// successful api lookup
export const success = (state, action) => {
  console.log("NotesRedux>success>action", action)
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state => {
  console.log("NotesRedux>failure")
  return state.merge({ fetching: false, error: true, payload: null })
}

export const onRehydrate = (state, action) => {
  return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NOTES_REQUEST]: request,
  [Types.NOTES_SUCCESS]: success,
  [Types.NOTES_FAILURE]: failure,
  [REHYDRATE]: onRehydrate,
})

const selectNotesDomain = ({ notes }) => notes.asMutable()

export const makeSelectHtmlNotes = () => createSelector(
  selectNotesDomain,
  ({htmlNotes}) => {
    return Object.values(htmlNotes).sort(({ id: a}, {id: b}) => {
      if(a > b) return 1
      if(a < b) return -1
      return 0
    })
  })
