import { createStore, applyMiddleware, compose } from 'redux'
import { autoRehydrate } from 'redux-persist'
import Config from '../Config/DebugConfig'
import FirebaseConfig from '../Config/FirebaseConfig'
import ReactReduxFirebaseConfig from '../Config/ReactReduxFirebaseConfig'
import createSagaMiddleware from 'redux-saga'
import RehydrationServices from '../Services/RehydrationServices'
import ReduxPersist from '../Config/ReduxPersist'
import ScreenTracking from './ScreenTrackingMiddleware'
import { reactReduxFirebase } from 'react-redux-firebase'
import RNFirebase from 'react-native-firebase'
import devToolsEnhancer from 'remote-redux-devtools'
import OnStateChange from './OnStateChange'
import OnProfileIdChange from './OnProfileIdChange'

export const firebase = RNFirebase.initializeApp(FirebaseConfig)

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = []

  /* ------------- Analytics Middleware ------------- */
  middleware.push(ScreenTracking)

  /* ------------- Saga Middleware ------------- */

  const sagaMonitor = Config.useReactotron ? console.tron.createSagaMonitor() : null
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
  middleware.push(sagaMiddleware)

  middleware.push(OnStateChange(OnProfileIdChange))

  /* ------------- ReduxFirebase Enhancer ------------- */

  enhancers.push(reactReduxFirebase(firebase, ReactReduxFirebaseConfig))

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware))

  /* ------------- AutoRehydrate Enhancer ------------- */

  // add the autoRehydrate enhancer
  if (ReduxPersist.auto) {
    enhancers.push(autoRehydrate())
  }

  /* ------------- ReduxFirebase Enhancer ------------- */

  if (Config.useDevToolsEnhancer) {
    enhancers.push(devToolsEnhancer())
  }

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore = Config.useReactotron ? console.tron.createStore : createStore
  const store = createAppropriateStore(rootReducer, compose(...enhancers))

  // configure persistStore and check reducer version number
  if (ReduxPersist.active) {
    RehydrationServices.updateReducers(store)
  }

  // kick off root saga
  sagaMiddleware.run(rootSaga)

  return store
}
