import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { createSelector } from 'reselect'
import {REHYDRATE} from 'redux-persist/constants'
import {getDate} from '../Services/Utils'

/* ------------- Types and Action Creators ------------- */

const days = {
  0: 'Su',
  1: 'M',
  2: 'Tu',
  3: 'W',
  4: 'Th',
  5: 'F',
  6: 'Sa',
}

const { Types, Creators } = createActions({
  quizRequest: ['data'],
  quizSuccess: ['payload'],
  quizFailure: null,
  startQuizAction: ['quiz_type', 'questions'],
  answerQuestion: ['question', 'response'],
})

export const QuizTypes = Types

export const QuizFormats = {
  PRACTICE: 'PRACTICE',
  TEST: 'TEST',
}

export const QuizConstants = {
  IMAGE_RATIO: 0.6,
  QUESTION_SET_LENGTH: 25
}
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null,
  entities: {
    diagram: {},
    theory: {},
    current_set: null,
  },
  daily_progress: {},
})

const shuffle = (array) => {
  let currentIndex = array.length, temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

const loadQuestions = (questions) => {
  const entities = {
    diagram: {},
    theory: {}
  }

  for (let i = 0; i < questions.length; i++) {
    const question = questions[i]
    if (question.image)
      entities.diagram[question.id] = question
    else
      entities.theory[question.id] = question
  }
  return entities
}

const generateQuestionSet = (entities, type, questionSetLength = QuizConstants.QUESTION_SET_LENGTH) => {
  const current_set = []

  let digs = 0
  digs = Math.floor(questionSetLength * QuizConstants.IMAGE_RATIO)

  let max_attempts = 0
  let i = 0
  let set = entities.diagram
  let keys = Object.keys(set)

  while (current_set.length < digs) {
    if (i >= keys.length) {
      i = 0
      max_attempts++
      continue
    }

    const question = set[keys[i]]
    const attempts = question.correctly_answered + question.incorrectly_answered

    if (question.image && (max_attempts >= attempts)) {
      current_set.push(Object.assign({}, question, {response: null}))
    }


    i++
  }

  max_attempts = 0
  i = 0
  set = entities.theory
  keys = Object.keys(set)

  while (current_set.length < questionSetLength) {
    if (i >= keys.length) {
      i = 0
      max_attempts++
      continue
    }

    const question = set[keys[i]]
    const attempts = question.correctly_answered + question.incorrectly_answered

    if (max_attempts >= attempts)
      current_set.push(Object.assign({}, question, {response: null}))

    i++
  }
  return shuffle(current_set)
}

export const current_set = (state = INITIAL_STATE.current_set, action, entities) => {
  switch (action.type) {
    case Types.START_QUIZ_ACTION: {
      const questions = generateQuestionSet(entities, action.quiz_type, action.questions)
      const length = questions.length

      state = {
        questions,
        length,
        index: 0,
        row: 0,
        quiz_type: action.quiz_type
      }
    }
      break
    case Types.ANSWER_QUESTION: {
      const {questions, row, length} = state
      let key = null
      for (let i = 0; i < length; i++) {
        if (questions[i].id === action.question.id) {
          key = i
          break
        }
      }
      if (key !== null) {
        const questionAfter = Object.assign({}, question(questions[key], action), {response: action.response})
        state = state.merge({
          questions: [
            ...questions.slice(0, key),
            questionAfter,
            ...questions.slice(key + 1)
          ],
          index: key + 1,
          row: questionAfter.response === questionAfter.correct_option ? row + 1 : row
        })
      }
    }
      break
    default:
  }
  return state
}

export const entity_type = (state, type, action) => {
  state = state[type]
  switch (action.type) {
    case Types.ANSWER_QUESTION: {
      const {question, response} = action
      state = state.merge({
        [question.id]: {
          ...state[question.id]
        }
      })
      if (question.correct_option === response) {
        state = state.setIn([question.id, 'correctly_answered'], state[question.id].correctly_answered + 1)
      } else {
        state = state.setIn([question.id, 'incorrectly_answered'], state[question.id].incorrectly_answered + 1)
      }
    }
      break
  }
  return state
}

export const question = (state, action) => {
  switch (action.type) {
    case Types.ANSWER_QUESTION: {
      let {correctly_answered, incorrectly_answered} = state
      correctly_answered += state.correct_option === action.response ? 1 : 0
      incorrectly_answered += state.correct_option === action.response ? 0 : 1
      state = state.merge({correctly_answered, incorrectly_answered})
    }
      break
    default:
  }
  return state
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state, { data }) =>
  state.merge({ fetching: true, data, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { payload } = action
  return state.merge({ fetching: false, error: null, payload })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

export const onRehydrate = (state, action) => {
  let entities = {}
  try {
    entities = action.payload.quiz.entities
    console.log(entities)
    if (!Object.keys(entities.diagram).length) {
      throw new Error("load questions")
    }

    delete entities.diagram[26];
    delete entities.diagram[31];
    delete entities.diagram[11]
    delete entities.diagram[25]
    delete entities.diagram[108]
    state = state.merge({ entities })
  } catch (e) {
    const questions = require('../Data/questions.json')
    entities = Object.assign({}, entities, loadQuestions(questions))
    state = state.merge({ entities })
  }
  try {
    const { daily_progress } = action.payload.quiz
    if (daily_progress) {
      state = state.merge({ daily_progress })
    }
  } catch (e) {
    // console.log('not found')
  }
  return state
}

export const onStartQuiz = (state, action) => {
  const _current_set = current_set(state.entities.current_set, action, state.entities)
  const entities = state.entities.merge({ current_set: _current_set })
  return state.merge({ entities })
}

export const onAnswerQuestion = (state, action) => {
  const {question} = action
  const type = question.image ? 'diagram' : 'theory'

  const _current_set = current_set(state.entities.current_set, action, state.entities)
  const _entity_type = entity_type(state.entities, type, action)
  const entities = state.entities.merge({current_set: _current_set, [type]: _entity_type})
  state = state.merge({ entities })

  const key = getDate()
  if (!state.daily_progress[key]) {
    state = state.setIn(['daily_progress', key], {correctly_answered: 0, incorrectly_answered: 0,})
  }
  const option = action.question.correct_option === action.response ? 'correctly_answered' : 'incorrectly_answered'
  state = state.setIn(['daily_progress', key, option], state.daily_progress[key][option] + 1)
  return state
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.QUIZ_REQUEST]: request,
  [Types.QUIZ_SUCCESS]: success,
  [Types.QUIZ_FAILURE]: failure,
  [REHYDRATE]: onRehydrate,
  [Types.START_QUIZ_ACTION]: onStartQuiz,
  [Types.ANSWER_QUESTION]: onAnswerQuestion,
})

const selectQuizDomain = ({ quiz }) => quiz.asMutable()

export const makeSelectCurrentSet = () => createSelector(
  selectQuizDomain,
  (quizDomain) => {
    try {
      return quizDomain.entities.current_set
    } catch (e) {
      return {}
    }
  })

export const makeSelectProgress = () => createSelector(
  selectQuizDomain,
  (quizDomain) => {
    try {
      const daily_progress = quizDomain.daily_progress
      let weekly_progress = []
      let date = new Date()
      let active_days = 0
      let active_total = 0
      date.setDate(date.getDate() - 6)

      for (let i = 0; i < 7; i++, date.setDate(date.getDate() + 1)) {
        const key = getDate(date)
        let day = days[date.getDay()]
        let score = 0

        let correct = 0
        let total = 0

        if (daily_progress[key]) {
          correct = daily_progress[key]['correctly_answered']
          total = daily_progress[key]['correctly_answered'] + daily_progress[key]['incorrectly_answered']
        }

        if (total) {
          score = Math.floor(correct / total * 100)
          active_days++
          active_total += score
        }

        weekly_progress.push({
          day,
          score,
        })
      }

      let overall = 0
      if (active_days) {
        overall = Math.floor(active_total / active_days)
      }

      return {weekly_progress, overall}
    } catch (e) {
      return {weekly_progress: [], overall: 0}
    }
  })

export const makeSelectResults = () => createSelector(
  makeSelectCurrentSet(),
  (current_set) => {
    console.log(current_set)
    try {
      const {questions} = current_set
      const correct = questions.reduce((correct, question) => {
        if (question.response === question.correct_option)
          correct++
        return correct
      }, 0)

      return {
        current_set,
        correct
      }
    } catch (e) {
      return {}
    }
  })
