import PropTypes from 'prop-types'
import Image from './Image'

export default PropTypes.shape({
  isLoaded: PropTypes.bool.isRequired,
  isEmpty: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  downloadUrl: PropTypes.string.isRequired,
  location: PropTypes.oneOf(['bulawayo', 'harare', 'manicaland', 'mash_central', 'mash_east', 'mash_west', 'masvingo', 'mat_north', 'mat_south', 'midlands']).isRequired,
  createdAt: PropTypes.number.isRequired,
  modifiedAt: PropTypes.number.isRequired,
  profile_pic: Image.isRequired
})
