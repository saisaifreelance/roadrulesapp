import PropTypes from 'prop-types'
import Profile from './Profile'
import Image from './Image'
export default PropTypes.shape({
  isFetching: PropTypes.string,
  isAuthenticated: PropTypes.bool.isRequired,
  isHydrated: PropTypes.bool.isRequired,
  isSet: PropTypes.bool.isRequired,
  isReady: PropTypes.bool.isRequired,
  hasProfile: PropTypes.bool.isRequired,
  error: PropTypes.string,
  verificationId: PropTypes.string,
  form: PropTypes.objectOf(PropTypes.shape({
    value: PropTypes.string,
    error: PropTypes.string
  })),
  image: Image,
  data: Profile
})
