import PropTypes from 'prop-types'

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  downloadUrl: PropTypes.string.isRequired,
  totalBytes: PropTypes.number.isRequired,
  createdAt: PropTypes.number.isRequired,
  modifiedAt: PropTypes.number.isRequired
})
