import React, { Component } from 'react'
import {
  View,
  Text
} from 'react-native'
import {AnimatedCircularProgress} from 'react-native-circular-progress'
import Svg, {
  Circle,
  Polyline
} from 'react-native-svg'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { makeSelectProgress } from '../Redux/QuizRedux'

// Styles
import {Colors, Fonts, Metrics} from '../Themes'
import styles from './Styles/ProgressStyle'

const toolbarHeight = 56 * 2
const lineHeight = 40
const circleBorder = 4
const circleSize = 20
const chartWidth = Metrics.screenWidth - 32
const chartHeight = lineHeight * 6 + 32
const chartDivision = (chartWidth - circleSize) / 7
const meterSize = Metrics.screenHeight - chartHeight - toolbarHeight - 32 - 32 - 40
const yRange = lineHeight * 5
const xRange = chartDivision * 7
const yOffset = lineHeight + 16
const xOffset = chartDivision

class Progress extends Component {
  // constructor (props) {
  //   super(props)
  //   this.state = {}
  // }
  render() {
    const {weekly_progress, overall} = this.props

    const grid = []
    for (let i = 5; i >= 0; i--) {
      const percentage = `${i * 10 * 2}%`

      grid.push(
        <View
          key={i}
          style={{height: lineHeight, borderBottomWidth: 1, borderBottomColor: 'rgba(0,0,0,0.2)', justifyContent: 'flex-end'}}>
          <Text style={{}}>{percentage}</Text>
        </View>
      )
    }

    let points = ''
    let circles = []
    let labels = []
    for (let i = 0; i < weekly_progress.length; i++) {
      const x = Math.floor((i) * (xRange / 7) + xOffset)
      const y = Math.floor(((100 - weekly_progress[i].score) * yRange / 100) + yOffset)
      points += x + ',' + y + ' '
      circles.push(
        <Circle
          key={i}
          cx={x}
          cy={y}
          r={circleSize/2}
          stroke='none'
          strokeWidth={circleBorder}
          fill={Colors.red}
        />)
      labels.push(
        <AnimatedCircularProgress
          key={i}
          style={{borderRadius: 16, marginLeft: chartDivision - 32}}
          size={32}
          width={4}
          fill={weekly_progress[i].score}
          tintColor={Colors.amber}
          backgroundColor="#9E9E9E">
          {
            () => (
              <View style={styles.fill}>
                <Text style={{fontSize: Fonts.size.small}}>
                  { weekly_progress[i].day }
                </Text>
              </View>
            )

          }

        </AnimatedCircularProgress>
      )
    }

    const _meter = meterSize > 64 ? (
      <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
        <AnimatedCircularProgress
          style={{borderRadius: 150}}
          size={meterSize}
          width={8}
          fill={overall}
          tintColor={Colors.amber}
          backgroundColor="#9E9E9E">
          {
            (fill) => (
              <View style={styles.fill}>
                <Text style={styles.points}>
                  { `${Math.floor(fill)}%` }
                </Text>
              </View>
            )
          }
        </AnimatedCircularProgress>
        <Text style={{fontSize: Fonts.size.h6, textAlign: 'center', color: Colors.charcoal, marginTop: 8}}>Weekly Average Score!</Text>
      </View>
    ) : null
    return (
      <View style={styles.container}>
        {_meter}
        <View style={{height: chartHeight, width: chartWidth,}}>
          <View style={{position: 'absolute', left: 0, right: 0, top: 16, bottom: 16}}>
            {grid}
          </View>
          <Svg
            height={chartHeight}
            width={chartWidth}
            style={{}}
          >
            <Polyline
              points={points}
              stroke={Colors.blue}
              strokeWidth="4"
              fill="none"
            />
            {circles}
          </Svg>
        </View>
        <View style={{flexDirection: 'row', paddingLeft: 16,marginTop: 8,}}>{labels}</View>
      </View>
    )
  }
}

const mapStateToProps = makeSelectProgress()

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Progress)
