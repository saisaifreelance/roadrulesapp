import React from 'react'
import { DrawerNavigator } from 'react-navigation'
import DonutScreen from './DonutScreen'
import DrawerContainer from './DrawerContainer'

export default DrawerNavigator({
  Home: {
    screen: DonutScreen
  }
}, {
  contentComponent: DrawerContainer
})
