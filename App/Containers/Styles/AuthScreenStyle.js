import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.logo,
  logoContainer: {
    ...ApplicationStyles.logo.logoContainer,
    height: Metrics.screenHeight / 3
  },
  container: {
    ...ApplicationStyles.screen.container,
    backgroundColor: Colors.snow,
    paddingTop: Metrics.toolbarPaddingTop
  },
  formErrorContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: Metrics.smallMargin
  },
  ...ApplicationStyles.form
})
