import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,

  container: {
    flex: 1,
    backgroundColor: Colors.snow,
  },
  toolbar: {
    alignItems: 'center',
    padding: 16,
    flexDirection: 'row',
    backgroundColor: 'transparent',
  },
  toolbar_title: {
    fontSize: Fonts.size.h6,
    color: Colors.panther,
  },
  points: {
    backgroundColor: 'transparent',
  },
  progress: {},
  fill: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  buttons: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderTopColor: 'rgba(0,0,0,0.1)',
    borderTopWidth: 1,
  },
  button: {},
  msg: {
    flex: 1,
    padding: 16,
  },
  flex_image: {
    width: Metrics.screenWidth,
    height: undefined,
    flex: 1,
  }
})
