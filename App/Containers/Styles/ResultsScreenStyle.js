import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    flex: 1,
  },
  content: {
    padding: 16,
    flex: 1,
  },
  fill: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  points: {
    fontSize: Fonts.size.h5,
    textAlign: 'center'
  },

  msg_container_pass: {
    backgroundColor: Colors.green,
    borderRadius: 8,
    padding: 16,
  },
  msg_container_fail: {
    backgroundColor: Colors.red,
    borderRadius: 8,
    padding: 16,
  },
  msg_pass: {
    fontSize: Fonts.size.h6,
    color: Colors.snow,
  },
  msg_fail: {
    fontSize: Fonts.size.h6,
    color: Colors.snow,
  },
})
