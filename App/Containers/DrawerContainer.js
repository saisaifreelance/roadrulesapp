import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ScrollView, View, Text, TouchableOpacity, Modal, Alert } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import Actions, { makeSelectProfile } from '../Redux/ProfileRedux'
import ProfileReducer from '../Types/ProfileReducer'
import IconButton from '../Components/IconButton'
import DrawerHeader from '../Components/DrawerHeader'
import {ModalNavigator as ModalConnect} from '../Components/ModalConnect'
import Icon from 'react-native-vector-icons/MaterialIcons'
import EvilIcon from 'react-native-vector-icons/EvilIcons'
import { Colors, Fonts } from '../Themes'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import FacebookActions from '../Redux/FacebookRedux'
import Communication from '../Services/Communication'

const communication = Communication.create()

// Styles
import styles from './Styles/DrawerContainerStyle'

class DrawerContainer extends Component {
  static propTypes = {
    profile: ProfileReducer.isRequired,
    updateProfile: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
  }

  constructor (props) {
    super(props)
    this.goToProfile = this.goToProfile.bind(this)
    this.onFacebookInvite = this.onFacebookInvite.bind(this)
    this.onGoogleInvite = this.onGoogleInvite.bind(this)
    this.onShare = this.onShare.bind(this)
    this.onConnect = this.onConnect.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.state = {
        modal: false,
    }
  }

  modalAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ModalConnect',
    params: {
      title: 'Request Assistance',
      description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
      message: 'Hi, I\'m a Road Rules App user, requesting assistance',
      close: this.closeModal.bind(this),
      contact: {
        name: 'Road Rules App',
        phone: {
          office: {
            label: 'office',
            value: '+263718384668'
          }
        },
        whatsapp: {
          office: {
            label: 'office',
            value: '+263718384668'
          }
        },
        messenger: {
          office: {
            label: 'office',
            value: 'RoadRulesApp'
          }
        },
      }
    }
  }

  static navigationOptions = {
    drawerLabel: 'Notifications',
    drawerIcon: ({tintColor}) => (
      <IconButton
        backgroundColor={tintColor}
        color={Colors.snow}
        icon='menu'
      />
    )
  }

  profileAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ProfileScreen',
    params: {}
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const {state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalConnect') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalConnect') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  goToProfile () {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        this.profileAction
      ]
    }))
  }

  onFacebookInvite () {
    communication.facebookInvite()
  }

  onGoogleInvite () {
    communication.firebaseInvite('Click this link http://bit.ly/1QAo1Gs to Zimbabwe\'s Driving Test App from Google Play', 'Road Rules App', 'http://bit.ly/1QAo1Gs')
  }

  onShare () {
    communication.share('Click this link http://bit.ly/1QAo1Gs to download the Road Rules App, Zimbabwe\'s Provisional Driver\'s License Test App FREE from Google Play store today', 'Tell a Friend', 'http://bit.ly/1QAo1Gs', {dialogTitle: 'Tell a Friend'})
  }

  onConnect () {
    this.openModal()
  }

  onLogout () {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout of the Road Rules App?',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'Logout', onPress: () => { this.props.logout() }},
      ],
      { cancelable: false }
    )
  }

  render () {
    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalConnect ref={this.modalRef} />
      </Modal>
    ) : null
    return (
      <ScrollView style={styles.container}>
        <DrawerHeader profile={this.props.profile.data} logout={this.onLogout.bind(this)} updateProfile={this.goToProfile} />
        <View style={styles.optionsContainer}>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={this.onShare}>
            <View style={styles.optionIconContainer}>
              <Icon name='share' size={Fonts.size.h4} color={Colors.green} />
            </View>
            <Text style={styles.option}>Tell a Friend</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={this.onFacebookInvite}>
            <View style={styles.optionIconContainer}>
              <EvilIcon name='sc-facebook' size={Fonts.size.h3} color={Colors.facebook} />
            </View>
            <Text style={styles.option}>Invite Friends via Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={this.onGoogleInvite}>
            <View style={styles.optionIconContainer}>
              <EvilIcon name='sc-google-plus' size={Fonts.size.h3} color={Colors.google} />
            </View>
            <Text style={styles.option}>Invite Phone Book Contacts</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.optionContainer}
            onPress={this.onConnect}>
            <View style={styles.optionIconContainer}>
              <Icon name='help-outline' size={Fonts.size.h4} color={Colors.charcoal} />
            </View>
            <Text style={styles.option}>Request Assistance</Text>
          </TouchableOpacity>
        </View>
        {modal}
      </ScrollView>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(Actions.logoutRequestAction()),
    updateProfile: () => dispatch(Actions.logoutRequestAction()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContainer)
