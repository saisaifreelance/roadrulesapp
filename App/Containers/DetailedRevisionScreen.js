import React, { Component } from 'react'
import {
  View,
  FlatList,
  Image,
  Text,
  Modal,
  BackHandler,
  Platform,
  TouchableOpacity
} from 'react-native'
import ViewPager from 'react-native-viewpager'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, { makeSelectResults } from '../Redux/QuizRedux'

import {Colors, Metrics, Fonts} from '../Themes'
import NavBar from '../Components/NavBar'
import ModalMenu from '../Components/ModalMenu'
import QuestionContainer from '../Components/QuestionContainer'
import {Toolbar} from './QuizScreen'

const _rowHasChanged = (r1, r2) => {
  return r1 !== r2
}

// Styles
import styles from './Styles/DetailedRevisionScreenStyle'

class DetailedRevisionScreen extends Component {
  constructor (props) {
    super(props)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      ds: new ViewPager.DataSource({rowHasChanged: _rowHasChanged}),
      index: 0,
      modal: false
    }
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    this.props.navigation.goBack()
    return true
  }

  modalAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ModalMenu',
    params: {
      actions: [
        {
          title: 'Rate Us',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalRate',
              params: {}
            })
          }
        },
        {
          title: 'Request Assistance',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalConnect',
              params: {
                title: 'Request Assistance',
                description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
                message: 'Hi, I\'m a Road Rules App user, requesting assistance',
                contact: {
                  name: 'Road Rules App',
                  phone: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  whatsapp: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  messenger: {
                    office: {
                      label: 'office',
                      value: 'RoadRulesApp'
                    }
                  },
                }
              }
            })
          }
        }
      ],
      title: 'Select an option below',
      close: this.closeModal.bind(this)
    }
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const { state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
      // modal.dispatch(this.modalAction)
      /*
      modal.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          this.modalAction
        ]
      }))
      */
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  renderPage (question, pageID) {
    return (
      <QuestionContainer
        isReview={"detailed"}
        explanationProps={{
          icon_close: "none",
        }}
        openModal={this.openModal}
        selected={question.response}
        question={question}
        onSelect={() => {}}
        modal={true}
      />
    )
  }

  renderPageIndicator () {
    return <View />
  }

  render () {
    const {current_set} = this.props
    if (!current_set) {
      return <View style={{backgroundColor: 'red', flex: 1}} />
    }
    const {index, ds} = this.state
    const {length, questions} = current_set
    const toolbarProps = {
      index,
      length,
      msg: "Detailed Revision",
      leftAction: () => this.props.navigation.goBack()
    }
    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalMenu ref={this.modalRef} />
      </Modal>
    ) : null
    return (
      <View style={styles.container}>
        <Toolbar {...toolbarProps}/>
        <ViewPager
          style={styles.container}
          dataSource={ds.cloneWithPages(questions)}
          renderPage={this.renderPage.bind(this)}
          onChangePage={(index) => this.setState({index}) }
          renderPageIndicator={this.renderPageIndicator.bind(this)}
        />
        {modal}
      </View>
    )
  }
}

const mapStateToProps = makeSelectResults()

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailedRevisionScreen)
