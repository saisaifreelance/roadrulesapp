import React, { Component } from 'react'
import {
  View,
  FlatList,
  Image,
  Text,
  Modal,
  BackHandler,
  Platform,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { makeSelectResults } from '../Redux/QuizRedux'

import {Colors, Metrics, Fonts} from '../Themes'
import NavBar from '../Components/NavBar'
import ModalMenu from '../Components/ModalMenu'
import QuestionContainer from '../Components/QuestionContainer'

// Styles
import styles from './Styles/QuickRevisionScreenStyle'

class QuickRevisionScreen extends Component {
  constructor (props) {
    super(props)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.state = {
      id: null,
      modal: false
    }
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction.bind(this))
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction.bind(this))
    }
  }

  backAction () {
    const {id} = this.state
    if (id) {
      this.setState({id: null})
    } else {
      this.props.navigation.goBack()
    }

    return true
  }

  modalAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ModalMenu',
    params: {
      actions: [
        {
          title: 'Rate Us',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalRate',
              params: {}
            })
          }
        },
        {
          title: 'Request Assistance',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalConnect',
              params: {
                title: 'Request Assistance',
                description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
                message: 'Hi, I\'m a Road Rules App user, requesting assistance',
                contact: {
                  name: 'Road Rules App',
                  phone: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  whatsapp: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  messenger: {
                    office: {
                      label: 'office',
                      value: 'RoadRulesApp'
                    }
                  },
                }
              }
            })
          }
        }
      ],
      title: 'Select an option below',
      close: this.closeModal.bind(this)
    }
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const { state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
      // modal.dispatch(this.modalAction)
      /*
      modal.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          this.modalAction
        ]
      }))
      */
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  renderRow ({item: question, index, separators}, rowID) {
    console.log('DATA!!!')
    console.log(question)
    const pass = question.response === question.correct_option
    const {id} = question

    const options = {
      option_1: 'A. ',
      option_2: 'B. ',
      option_3: 'C. ',
    }

    const iconProps = {
      name: pass ? "done" : "close",
      color: pass ? Colors.green : Colors.red,
      size: 36,
      style: {margin: 8}
    }
    const image = question.image ?
      <Image source={{uri: question.image}} resizeMode={"contain"} style={{width: 100, height: 100}}/> : null

    const wrong_answer = pass ? null : (
      <Text style={{
        color: 'red',
        textDecorationLine: 'line-through',
        textDecorationColor: 'red'
      }}>
        <Text style={{
          color: 'black',
          fontSize: Fonts.size.regular
        }}>{options[question.response]}{question[question.response]}</Text>
      </Text>
    )
    return (
      <TouchableOpacity
        style={{
          padding: 16,
          flexDirection: 'row',
          backgroundColor: Colors.snow,
          margin: 4,
          elevation: 1,
          alignItems: 'center'
        }}
        onPress={() => this.setState({id})}
      >
        <Icon {...iconProps} />
        <View style={{flex: 1}}>
          <Text style={{fontSize: Fonts.size.h6, marginBottom: 8}}>{question.text}</Text>
          {wrong_answer}
          <Text style={{fontSize: Fonts.size.regular}}>{options[question.correct_option]}{question[question.correct_option]}</Text>
        </View>
        {image}
      </TouchableOpacity>
    )
  }

  render () {
    const {current_set} = this.props
    if (!current_set) {
      return <View style={{backgroundColor: 'red', flex: 1}} />
    }
    const {questions} = current_set
    const {id} = this.state
    let content = null
    if (id !== null) {
      let question = null
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].id === id) {
          question = questions[i]
          break
        }
      }

      if (!question)
        return null

      content = (
        <QuestionContainer
          isReview={"quick"}
          openModal={this.openModal}
          selected={question.response}
          explanationProps={{
            icon_close: "arrow-back",
            toggleShowExplanation: this.backAction.bind(this)
          }}
          question={question} onSelect={() => {
        }}
          modal={true}
        />
      )
    } else {
      content = (
        <FlatList
          style={styles.container}
          data={questions}
          renderItem={this.renderRow.bind(this)}
        />
      )
    }

    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalMenu ref={this.modalRef} />
      </Modal>
    ) : null

    return (
      <View style={styles.container}>
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.panther,
            }
          }}
          leftElement='arrow-back'
          rightElement='more-vert'
          onLeftElementPress={() => this.props.navigation.goBack()}
          onRightElementPress={this.openModal}
          centerElement='Quick Revision'
        />
        {content}
        {modal}
      </View>
    )
  }
}

const mapStateToProps = makeSelectResults()

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuickRevisionScreen)
