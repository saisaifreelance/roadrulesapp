import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
import { makeSelectProfile } from '../Redux/ProfileRedux'
import ProfileReducer from '../Types/ProfileReducer'

// Styles
import styles from './Styles/LaunchScreenStyles'

class LaunchScreen extends Component {
  static propTypes = {
    profile: ProfileReducer.isRequired,
  }
  homeAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'HomeScreen',
    params: {}
  }

  authAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'AuthScreen',
    params: {}
  }

  profileAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ProfileScreen',
    params: {}
  }

  constructor (props) {
    super(props)
    console.log('construct')
    this.state = {
      animationComplete: false
    }
    this.onBack = this.onBack.bind(this)
  }

  componentDidMount () {
    console.log('mount')
    this.timeout = setTimeout(() => {
      this.setState({animationComplete: true})
    }, 1500)
  }

  onBack () {

  }

  componentDidUpdate (prevProps, prevState) {
    const { isAuthenticated, hasProfile, isReady } = this.props.profile
    const { isReady: prevIsReady } = prevProps.profile
    const { animationComplete } = this.state
    const { animationComplete: prevAnimationComplete } = prevState

    const redirect = (isReady && animationComplete && !prevAnimationComplete) || (animationComplete && isReady && !prevIsReady)
    if (redirect) {
      clearTimeout(this.timeout)
      if (isAuthenticated && hasProfile) {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.homeAction
          ]
        }))
      } else if (isAuthenticated && !hasProfile) {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.profileAction
          ]
        }))
      } else {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.authAction
          ]
        }))
      }
    }
  }

  render () {
    /*
        return (
          <View style={[styles.container, {backgroundColor: 'red'}]}>
          </View>
        );
    */
    return (
      <View style={styles.container}>
        <Image
          source={{uri: 'screen'}}
          resizeMode='contain'
          style={styles.logo} />
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
