import React, { Component } from 'react'
import {
  View,
  Image,
  Text,
  TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
import {Button as MKButton} from 'react-native-material-ui'
import Icon from 'react-native-vector-icons/MaterialIcons'
import {AnimatedCircularProgress} from 'react-native-circular-progress'
import {createTransition, SlideUp} from 'react-native-transition'
import TimerFormatter from 'minutes-seconds-milliseconds'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, {QuizFormats, makeSelectCurrentSet} from '../Redux/QuizRedux'

// Styles
import styles from './Styles/QuizScreenStyle'
import {Colors, Fonts, Metrics} from '../Themes/'

import {ModalPay} from '../Components/Modal'
import QuestionContainer from '../Components/QuestionContainer'
import Button from '../Components/Button'

const TEST_DURATION = 480000
const { screenWidth: width} = Metrics
const BUTTON_TEXT = {
  SUBMIT: 'SUBMIT',
  CONTINUE: 'CONTINUE'
}
const Transition = createTransition(SlideUp)

export const Toolbar = (props) => {
  const {index, length, quiz_type, time, duration, msg, leftAction} = props

  const cursor = index + 1
  const fill = Math.floor(cursor / length * 100)

  let timer = null
  if (quiz_type === QuizFormats.TEST) {
    const time_elapsed = TimerFormatter(time)
    const fill = time / duration * 100
    timer = (
      <AnimatedCircularProgress
        style={{backgroundColor: Colors.snow, borderRadius: 32}}
        size={90}
        width={3}
        fill={fill}
        tintColor={Colors.amber}
        backgroundColor="#9E9E9E">
        {
          (fill) => (
            <View style={styles.fill}>
              <Text style={styles.points}>
                { time_elapsed }
              </Text>
            </View>
          )
        }
      </AnimatedCircularProgress>
    )
  }

  let title = null
  if (msg) {
    title = (<Text style={styles.toolbar_title}>{msg}</Text>)
  } else {
    title = quiz_type === QuizFormats.TEST ?
      (<Text style={styles.toolbar_title}>Test</Text>) :
      (<Text style={styles.toolbar_title}>Practice</Text>)
  }

  return (
    <View style={styles.toolbar}>
      <TouchableOpacity style={{borderRadius: 16}} onPress={leftAction}>
        <Icon name="close" size={32} color={Colors.charcoal}/>
      </TouchableOpacity>
      <View style={{flex: 1, padding: 16}}>
        {title}
      </View>
      <View style={{flexDirection: 'row', alignItems: 'center'}}>
        <AnimatedCircularProgress
          style={{backgroundColor: Colors.snow, elevation: 1, borderRadius: 16,}}
          size={32}
          width={3}
          fill={fill}
          tintColor={Colors.amber}
          backgroundColor="#9E9E9E">
          {
            (fill) => (
              <View style={styles.fill}>
                <Text style={styles.points}>
                  { cursor }
                </Text>
              </View>
            )
          }
        </AnimatedCircularProgress>
        {timer}
      </View>
    </View>
  )
}

class QuizScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      start: null,
      time: null,
      running: true,
      selected: null,
      buttonText: null,
    }

    this.renderMessage = this.renderMessage.bind(this)
    this.renderQuestion = this.renderQuestion.bind(this)
    this.renderButton = this.renderButton.bind(this)
    this.renderToolbar = this.renderToolbar.bind(this)
  }

  componentDidMount () {
    this.setState({start: Date.now()})
    if (this.props.current_set && this.props.current_set.quiz_type === QuizFormats.TEST)
      this.interval = setInterval(() => {
        const time = Date.now() - this.state.start
        if (time < TEST_DURATION)
          return this.setState({
            time: Date.now() - this.state.start
          })

        this.setState({running: false, time: TEST_DURATION})
        clearInterval(this.interval)
      }, 100)
  }

  componentDidUpdate (prevProps, prevState) {
    const content = this.state.running ? this.renderQuestion() : this.renderMessage()
    if (this.props.current_set.index !== prevProps.current_set.index) {
      console.log(this.state.running)
      console.log(content)
      Transition.show(content)
    }
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  renderMessage () {
    const {active, current_set} = this.props
    const { quiz_type } = current_set
    const {time, showRow, running} = this.state
    let title, content, button, instruction = null

    // time up
    if (quiz_type === QuizFormats.TEST && time >= TEST_DURATION) {
      title = "Time Up!"
      content = (

        <View style={{flex: 1, alignItems: 'center'}}>
          <Image source={require('../Images/splash_time.png')}
                 style={{width: width - 64, height: undefined, flex: 1}}
                 resizeMode={"contain"}/>
        </View>
      )
      instruction = "You have run out of time, click continue to see results"
      button = this.props.goResults
    }

    // practice last question during trial
    if (!active && (current_set.index + 1) >= current_set.length) {
      title = "Trial mode"
      content = (
        <View style={{flex: 1}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image source={require('../Images/splash_stop.png')}
                   style={{width: width - 64, height: undefined, flex: 1}}
                   resizeMode={"contain"}/>
          </View>
          <Text style={{fontSize: Fonts.size.regular, textAlign: 'center', margin: 16,}}>
            Pay Now For Full Access To The Learner & Driving Schools Sections For (12) months.
          </Text>
          <Button style={{}} onPress={() => this.props.openModal(<ModalPay />)}>PAY NOW</Button>
        </View>
      )
      button = this.onSubmit.bind(this)
    }

    // on a roll
    if (showRow) {
      title = "You are on a roll!"
      content = (
        <View style={{flex: 1}}>

        </View>
      )
      button = this.onSelect.bind(this)
    }

    if (title && content && button) {
      const _instruction = <Text
        style={{fontSize: Fonts.size.h6, textAlign: 'center', margin: 16,}}>{instruction}</Text>
      return (
        <View style={styles.container}>
          <View style={styles.msg}>
            <Text style={{fontSize: Fonts.size.h5, textAlign: 'center', margin: 16,}}>{title}</Text>
            {content}
            {instruction ? _instruction : null}
          </View>
          <View style={styles.buttons}>
            <Button style={styles.button} onPress={button}>{BUTTON_TEXT.CONTINUE}</Button>
          </View>
        </View>
      )
    }

    return null
  }

  renderQuestion () {
    const {current_set, openModal} = this.props
    const { quiz_type } = current_set
    const {selected, buttonText} = this.state
    const question = current_set.questions[current_set.index]
    if (!question) return null

    const modal = quiz_type !== QuizFormats.TEST && buttonText === BUTTON_TEXT.CONTINUE
    const q = (
      <QuestionContainer
        question={question}
        selected={selected}
        onSelect={this.onSelect.bind(this)}
        openModal={openModal}
        modal={modal}
      />
    )

    if (!selected || buttonText === BUTTON_TEXT.CONTINUE || selected && quiz_type === QuizFormats.TEST) {
      return (
        <Transition style={styles.container}>{q}</Transition>
      )
    }

    return q
  }

  renderToolbar () {
    const {current_set, backAction} = this.props
    const {time} = this.state
    return (
      <Toolbar time={time}
               quiz_type={current_set.quiz_type}
               length={current_set.length}
               index={current_set.index}
               duration={TEST_DURATION}
               leftAction={backAction}
      />
    )
  }

  renderButton () {
    const {buttonText} = this.state
    return (
      <View style={styles.buttons}>
        <Button style={styles.button} onPress={this.onSubmit.bind(this)}>{buttonText}</Button>
      </View>
    )
  }

  onSelect (selected) {
    this.setState({selected, buttonText: BUTTON_TEXT.SUBMIT})
  }

  onSubmit () {
    const {buttonText, selected, running} = this.state
    const {current_set, active} = this.props
    const {quiz_type} = current_set

    if (quiz_type !== QuizFormats.TEST && buttonText === BUTTON_TEXT.SUBMIT) {
      return this.setState({buttonText: BUTTON_TEXT.CONTINUE})
    }

    // final question
    if (
      !active && running &&
      quiz_type !== QuizFormats.TEST &&
      buttonText === BUTTON_TEXT.CONTINUE &&
      (current_set.index + 1) >= current_set.length
    ) {
      return this.setState({running: false})
    }

    this.setState({selected: null, buttonText: null})
    this.props.doAnswerQuestion(current_set.questions[current_set.index], selected, (current_set.index + 1) >= current_set.length)
  }

  render () {
    const {current_set} = this.props
    console.log(current_set)
    if (!current_set) {
      return <View style={{backgroundColor: 'green', flex: 1}} />
    }
    const {selected, running} = this.state
    const question = current_set.questions[current_set.index]
    if (!question) return null

    let content = null, toolbar = null, button = null;

    content = running ? this.renderQuestion() : this.renderMessage();
    toolbar = running ? this.renderToolbar() : null
    button = selected && running ? this.renderButton() : null

    return (
      <View style={styles.container}>
        {toolbar}
        {content}
        {button}
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  current_set: makeSelectCurrentSet()
})

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    doAnswerQuestion (question, response, lastQuestion) {
      dispatch(Actions.answerQuestion(question, response))
      if (lastQuestion) {
        ownProps.navigation.navigate('ResultsScreen')
      }
    },
    goResults () {
      // return dispatch(goResults())
    },
    backAction () {
      // return dispatch(navigatePop())
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizScreen)
