import React, { Component } from 'react'
import { LayoutAnimation, RefreshControl, Animated, ScrollView, View, KeyboardAvoidingView, UIManager, Modal, BackHandler, Platform } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { makeSelectDrivingSchools } from '../Redux/DrivingSchoolsRedux'

import { Images, Fonts, Colors } from '../Themes'

import DrivingSchoolSearch from '../Components/DrivingSchoolSearch'
import DrivingSchoolCard from '../Components/DrivingSchoolCard'
import OverlayActions from '../Components/OverlayActions'
import ModalFilter from '../Components/ModalFilter'
import ModalMap from '../Components/ModalMap'
import ModalLocation from '../Components/ModalLocation'
import ModalTime from '../Components/ModalTime'

// Styles
import styles from './Styles/DrivingSchoolsScreenStyle'

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView)

const {Types, Properties} = LayoutAnimation
const TRANSITION_DURATION = 300

class DrivingSchoolsScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showSearch: true,
      height: 0,
      modal: null,
    }
    this.openModal = this.openModal.bind(this)
    this.onPress = this.onPress.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.onRefresh = this.onRefresh.bind(this)
    this.onScroll = this.onScroll.bind(this)
    this.renderModal = this.renderModal.bind(this)
    this.backAction = this.backAction.bind(this)
    this.scrollOffset = 0
    this._PRESET = LayoutAnimation.create(
      TRANSITION_DURATION, Types.easeInEaseOut, Properties.opacity
    )

    // UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction.bind(this))
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction.bind(this))
    }
  }

  backAction () {
    this.props.navigation.goBack()
    return true
  }

  // onSelect
  onRefresh () {
    this.setState({showSearch: true})
  }
  onScroll (event) {
    // const currentOffset = event.nativeEvent.contentOffset.y
    // if (this.state.showSearch && currentOffset > this.offset) {
    //   this.setState({showSearch: false})
    // }
    // this.offset = currentOffset
  }
  onLayout ({ nativeEvent: { layout: { width, height, x, y } } }) {
    this.setState({ width, height })
  }
  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const {state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }
  openModal (modal) {
    this.setState({modal})
  }
  closeModal () {
    this.setState({modal: null})
  }
  componentWillUpdate (nextProps, nextState) {
    if (nextState.height && nextState.height !== this.state.height) {
      // LayoutAnimation.configureNext(this._PRESET, () => {
      //   this.setState({animating: false})
      // })
    }
  }
  renderModal () {
    const {modal} = this.state
    switch (modal) {
      case 'location':
        return (<ModalLocation closeModal={this.closeModal} />)
      case 'time':
        return (<ModalTime closeModal={this.closeModal} />)
      case 'map':
        return (<ModalMap closeModal={this.closeModal} drivingSchools={this.props.drivingSchools} />)
      case 'filter':
        return (<ModalFilter closeModal={this.closeModal} />)
      default:
        return null
    }
  }
  onPress () {
    this.props.navigation.navigate('DrivingSchoolScreen')
  }
  render () {
    const { showSearch } = this.state
    const modal = this.renderModal()

    const views = []
    const group = this.props.drivingSchools.reduce((accumulator, drivingSchool) => {
      if (accumulator.length < 2) {
        accumulator.push(drivingSchool)
        return accumulator
      }
      views.push(
        <View style={styles.row} key={views.length}>
          <DrivingSchoolCard onPress={this.onPress} description={accumulator[0].cost} title={accumulator[0].title} image={{uri: 'image_22'}} location={accumulator[0].city} rating={accumulator[0].rating} />
          <DrivingSchoolCard onPress={this.onPress} description={accumulator[1].cost} title={accumulator[1].title} image={{uri: 'image_22'}} location={accumulator[1].city} rating={accumulator[1].rating} />
        </View>
      )
      return []
    }, [])

    if (group.length > 0) {
      views.push(
        <View style={styles.row} key={views.length}>
        {group.map((drivingSchool, index) => (<DrivingSchoolCard key={index} onPress={this.onPress} description={drivingSchool.cost} title={drivingSchool.title} image={{uri: 'image_22'}} location={drivingSchool.city} rating={drivingSchool.rating} />))}
        </View>
      )
    }

    console.log(views)
    console.log(group)


    return (
      <View style={styles.mainContainer}>
        <ScrollView
          scrollEventThrottle={1}
          bounces={false}
          onScroll={this.onScroll}
          style={[styles.mainContainer, {paddingTop: this.state.height}]}
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={this.onRefresh}
            />
          }
        >
          <KeyboardAvoidingView behavior='position'>
            {views}
          </KeyboardAvoidingView>
        </ScrollView>
        <DrivingSchoolSearch showSearch={showSearch} onLayout={this.onLayout.bind(this)} openModal={this.openModal} closeModal={this.closeModal} />
        <OverlayActions actions={[
          {
            label: 'Map',
            icon: 'map',
            onPress: () => {
              this.openModal('map')
            }
          },
          {
            label: 'Filter',
            icon: 'tune',
            onPress: () => {
              this.openModal('filter')
            }
          },
        ]} />
        <Modal
          animationType='slide'
          transparent
          visible={!!modal}
          onRequestClose={() => {alert('Modal has been closed.')}}
        >
          {modal}
        </Modal>
      </View>
    )
  }
}

// const mapStateToProps = makeSelectDrivingSchools()
const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DrivingSchoolsScreen)
