import React, { Component } from 'react'
import { SectionList, LayoutAnimation, View, Text, Modal, TouchableOpacity, BackHandler, Platform } from 'react-native'
import ParallaxScrollView from 'react-native-parallax-scroll-view'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { makeSelectProfile } from '../Redux/ProfileRedux'
import { makeSelectDrivingSchools } from '../Redux/DrivingSchoolsRedux'

// import ModalPay from '../Components/ModalPay'
import ModalConnect from '../Components/ModalConnect'
import Header from '../Components/Header'
import { Images, Colors, Metrics } from '../Themes'
import ListItemDrivingSchoolsCategory from '../Components/ListItemDrivingSchoolsCategory'
import NavBar from '../Components/NavBar'

// Styles
import styles from './Styles/DrivingSchoolsScreenStyle'

const {Types, Properties} = LayoutAnimation

const TRANSITION_DURATION = 300

class DrivingSchoolsScreen extends Component {
  constructor (props) {
    super(props)
    this.backAction = this.backAction.bind(this)
    this.handlePress = this.handlePress.bind(this)
    this.handleLongPress = this.handleLongPress.bind(this)
    this.handleItemPress = this.handleItemPress.bind(this)
    this.handleItemLongPress = this.handleItemLongPress.bind(this)
    this.renderItem = this.renderItem.bind(this)
    this.renderSectionHeader = this.renderSectionHeader.bind(this)
    this.getItems = this.getItems.bind(this)

    this.state = {
      selectedCardIndex: {
        cat: null,
        item: null,
      },
      hoveredCardIndex: {
        cat: null,
        item: null,
      },
      animating: false
    }

    this._PRESET = LayoutAnimation.create(
      TRANSITION_DURATION, Types.easeInEaseOut, Properties.opacity
    )
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  handleItemPress (cardId, id) {}

  handleItemLongPress (cardId, id) {}

  handlePress (cardId) {
    if (this.state.selectedCardIndex.cat === cardId && this.state.hoveredCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
    })
    // this.list.scrollTo({y: 0, x: 0, animated: true})
    this.setState({
      selectedCardIndex: {
        cat: cardId,
        item: null
      },
      hoveredCardIndex: {
        cat: null,
        item: null
      },
      animating: true
    })
    if (this.props.onPress) this.props.onPress()
  }

  handleLongPress (cardId) {
    if (this.state.hoveredCardIndex.cat === cardId && this.state.selectedCardIndex.cat === null) {
      return
    }
    LayoutAnimation.configureNext(this._PRESET, () => {
      this.setState({animating: false})
    })
    this.setState({
      selectedCardIndex: {
        cat: null,
        item: null
      },
      hoveredCardIndex: {
        cat: cardId,
        item: null
      },
      animating: true,
    })
  }

  renderSectionHeader ({section: {data, title}}) {
    return (
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
      </View>
    )
  }

  renderItem ({item: category, index: cardIndex, sepators}) {
    return (
      <ListItemDrivingSchoolsCategory
        category={category}
        cardIndex={cardIndex}
        selected={category.selected ? cardIndex : this.state.selectedCardIndex.cat}
        hovered={category.hovered ? cardIndex : this.state.hoveredCardIndex.cat}
        onPress={this.handlePress}
        onLongPress={this.handleLongPress}
      />
    )
  }

  getItems () {
    const {selectedCardIndex, animating} = this.state
    if (!this.props.drivingSchools.categories) {
      return []
    }
    let title = 'Driving Schools'
    if (!animating && (selectedCardIndex.cat || selectedCardIndex.cat === 0)) {
      const category = this.props.drivingSchools.categories[selectedCardIndex.cat]
      return [
        {
          data: [{
            selected: true,
            hovered: false,
            ...category
          }],
          title
        }
      ]
    }
    const data = Array.isArray(this.props.drivingSchools.categories) ? this.props.drivingSchools.categories : []
    return [
      {
        data,
        title
      }
    ]
  }

  backAction () {
    if (this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0) {
      this.setState({selectedCardIndex: {cat: null, item: null}})
      return true
    }
    this.props.navigation.goBack()
    return true
  }

  render () {
    const hasIndex = this.state.selectedCardIndex.cat || this.state.selectedCardIndex.cat === 0
    const smallHeader = hasIndex
    const sections = this.getItems()

    return (
      <View style={styles.mainContainer}>
        <SectionList
          ref={(list) => { this.list = list }}
          data={[]}
          sections={sections}
          renderItem={this.renderItem}
          renderSectionHeader={this.renderSectionHeader}
          renderScrollComponent={(props) => {
            delete props.renderScrollComponent
            return (
              <ParallaxScrollView
                {...props}
                contentContainerStyle={styles.mainContainer}
                headerBackgroundColor={Colors.charcoal}
                contentBackgroundColor={Colors.steel}
                parallaxHeaderHeight={hasIndex ? Metrics.toolbarHeight : Metrics.parallaxHeaderHeight}
                stickyHeaderHeight={Metrics.toolbarHeight}
                renderForeground={() => (<Header background={Images.backgroundDrivingSchools} title=''/>)}
                renderStickyHeader={() => (
                  <View style={{height: Metrics.toolbarHeight, backgroundColor: 'black'}}/>
                )}
                renderFixedHeader={() => (
                  <NavBar
                    containerStyle={{
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      right: 0
                    }}
                    style={{
                      container: {
                        marginTop: Metrics.toolbarPaddingTop,
                        backgroundColor: Colors.transparent,
                      }
                    }}
                    leftElement='arrow-back'
                    onLeftElementPress={this.backAction}
                    centerElement={hasIndex ? 'Driving Schools' : 'Driving Schools Listing'}
                    searchable={null}
                    isSearchActive={false}
                  />
                )}
              />
            )
          }}
        />
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  drivingSchools: makeSelectDrivingSchools()
})

const mapDispatchToProps = (dispatch) => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(DrivingSchoolsScreen)
