import React, { Component } from 'react'
import {
  View,
  Text,
  BackHandler,
  Platform,
  Modal
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import Actions, { QuizFormats, makeSelectResults } from '../Redux/QuizRedux'

// Styles
import styles from './Styles/ResultsScreenStyle'

import {AnimatedCircularProgress} from 'react-native-circular-progress'
import Button from '../Components/Button'
import {Colors, Metrics} from '../Themes'
import NavBar from '../Components/NavBar'
import ModalMenu from '../Components/ModalMenu'

const PASS_PERCENTAGE = 88

class ResultsScreen extends Component {
  constructor (props) {
    super(props)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      modal: false,
      size: 250
    }
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    this.props.navigation.dispatch(NavigationActions.reset({
      index: 1,
      actions: [
        {
          type: NavigationActions.NAVIGATE,
          routeName: 'HomeScreen',
          params: {}
        },
        {
          type: NavigationActions.NAVIGATE,
          routeName: 'LearnScreen',
          params: {}
        },
      ]
    }))
    return true
  }

  modalAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ModalMenu',
    params: {
      actions: [
        {
          title: 'Rate Us',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalRate',
              params: {}
            })
          }
        },
        {
          title: 'Request Assistance',
          onPress: () => {
            this.modal.dispatch({
              type: NavigationActions.NAVIGATE,
              routeName: 'ModalConnect',
              params: {
                title: 'Request Assistance',
                description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
                message: 'Hi, I\'m a Road Rules App user, requesting assistance',
                contact: {
                  name: 'Road Rules App',
                  phone: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  whatsapp: {
                    office: {
                      label: 'office',
                      value: '+263718384668'
                    }
                  },
                  messenger: {
                    office: {
                      label: 'office',
                      value: 'RoadRulesApp'
                    }
                  },
                }
              }
            })
          }
        }
      ],
      title: 'Select an option below',
      close: this.closeModal.bind(this)
    }
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const { state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
      // modal.dispatch(this.modalAction)
      /*
      modal.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          this.modalAction
        ]
      }))
      */
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  render () {
    const {correct, current_set, rightAction, actions, openModal} = this.props
    if (!current_set) {
      return <View style={{backgroundColor: 'red', flex: 1}} />
    }
    const percentage = correct / current_set.length * 100
    const fill = Math.floor(percentage)
    const pass = percentage >= PASS_PERCENTAGE

    const text = pass ?
      `Congratulations, you passed with a score of ${fill}%` :
      `Sorry, you missed the minimum pass score of ${PASS_PERCENTAGE}%, you scored ${fill}%`

    const message_container_style = pass ? styles.msg_container_pass : styles.msg_container_fail
    const message_style = pass ? styles.msg_pass : styles.msg_fail

    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalMenu ref={this.modalRef} />
      </Modal>
    ) : null

    return (
      <View style={styles.container}>
        <NavBar
          style={{
            container: {
              marginTop: Metrics.toolbarPaddingTop,
              backgroundColor: Colors.panther,
            }
          }}
          leftElement='arrow-back'
          rightElement='more-vert'
          onLeftElementPress={this.backAction}
          onRightElementPress={this.openModal}
          centerElement='Test Results'
        />
        <View style={styles.content}>
          <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
            <AnimatedCircularProgress
              style={{borderRadius: 150}}
              size={this.state.size}
              width={8}
              fill={fill}
              tintColor={ pass ? Colors.green : Colors.green}
              backgroundColor="#9E9E9E">
              {
                (fill) => (
                  <View style={styles.fill}>
                    <Text style={styles.points}>
                      { `${Math.floor(fill)}%` }
                    </Text>
                  </View>
                )
              }
            </AnimatedCircularProgress>
          </View>
          <View>
            <View style={message_container_style}>
              <Text style={message_style}>{text}</Text>
            </View>
            <Button text="" onPress={() => { this.props.navigation.navigate('QuickRevisionScreen') }} >Quick Revision</Button>
            <Button text="" onPress={() => { this.props.navigation.navigate('DetailedRevisionScreen') }} >Detailed Revision</Button>
          </View>
        </View>
        {modal}
      </View>
    )
  }
}

const mapStateToProps = makeSelectResults()

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ResultsScreen)
