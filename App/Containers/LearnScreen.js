import React, { Component } from 'react'
import { View, Modal, BackHandler, Platform } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
import { makeSelectProfile } from '../Redux/ProfileRedux'
import Actions, { QuizTypes, QuizFormats } from '../Redux/QuizRedux'

// Styles
import styles from './Styles/LearnScreenStyle'
import {Metrics, Colors, Fonts} from '../Themes'

import {LEARNER} from '../Services/Utils'
import NavBar from '../Components/NavBar'
import ScrollableTabView from 'react-native-scrollable-tab-view'
import StartQuiz from '../Components/StartQuiz'
import ModalMenu from '../Components/ModalMenu'
import Progress from './Progress'
import Notes from '../Components/Notes'
import TabBar from '../Components/TabBar'

class LearnScreen extends Component {
  constructor (props) {
    super(props)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      modal: false,
      screen: null
    }

    const actions = []
    if (!props.profile.hasRated) {
      actions.push({
        title: 'Rate Us',
        onPress: () => {
          this.modal.dispatch({
            type: NavigationActions.NAVIGATE,
            routeName: 'ModalRate',
            params: {}
          })
        }
      })
    }
    actions.push({
      title: 'Request Assistance',
      onPress: () => {
        this.modal.dispatch({
          type: NavigationActions.NAVIGATE,
          routeName: 'ModalConnect',
          params: {
            title: 'Request Assistance',
            description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
            message: 'Hi, I\'m a Road Rules App user, requesting assistance',
            contact: {
              name: 'Road Rules App',
              phone: {
                office: {
                  label: 'office',
                  value: '+263718384668'
                }
              },
              whatsapp: {
                office: {
                  label: 'office',
                  value: '+263718384668'
                }
              },
              messenger: {
                office: {
                  label: 'office',
                  value: 'RoadRulesApp'
                }
              },
            }
          }
        })
      }
    })
    this.modalAction = {
      type: NavigationActions.NAVIGATE,
      routeName: 'ModalMenu',
      params: {
        actions,
        title: 'Select an option below',
        close: this.closeModal
      }
    }
  }

  componentDidMount () {
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentWillUnmount () {
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    this.props.navigation.goBack()
    return true
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const { state: { nav: { routes, index } } } = modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal (modal) {
    this.setState({modal: true})
    if (this.modal) {
      const {state: { nav: { routes, index } } } = this.modal
      const { key, routeName } = routes[index]
      if (routeName === 'ModalMenu') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
      // modal.dispatch(this.modalAction)
      /*
      modal.dispatch(NavigationActions.reset({
        index: 0,
        actions: [
          this.modalAction
        ]
      }))
      */
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  render () {
    const modal = this.state.modal ? (
      <Modal
        animationType={'slide'}
        transparent
        visible
        onRequestClose={this.closeModal}>
        <ModalMenu ref={this.modalRef} />
      </Modal>
    ) : null
    return (
      <View style={styles.container}>
        <ScrollableTabView style={styles.container} renderTabBar={({ activeTab, tabs, containerWidth, goToPage, scrollValue }) => {
          return (
            <View style={{elevation: 1}}>
              <NavBar
                style={{
                  container: {
                    marginTop: Metrics.toolbarPaddingTop,
                    backgroundColor: Colors.black,
                  }
                }}
                leftElement='arrow-back'
                rightElement='more-vert'
                onLeftElementPress={this.props.navigation.goBack}
                onRightElementPress={this.openModal}
                centerElement='Learn'
              />
              <TabBar
                index={activeTab}
                tabs={tabs}
                onIndexChange={goToPage}
                backgroundColor={Colors.black}
                activeTextColor={Colors.snow}
                inactiveTextColor={'rgba(255,255,255,0.75)'}
                textStyle={Fonts.style.normal}
                underlineStyle={{
                  height: 4,
                  backgroundColor: Colors.snow,
                  bottom: 0,
                }}

                scrollValue={scrollValue}
                containerWidth={containerWidth}
              />
            </View>
          )
        }}>
          <StartQuiz
            tabLabel='Practice'
            message='START PRACTICE'
            instruction='Hi, you can select the number of questions below.'
            max={25}
            maxMessage='The maximum number of questions in a Practice set is [25].'
            min={3}
            startQuiz={this.props.startPracticeQuiz.bind(undefined)}
          />
          <StartQuiz
            practice={false}
            tabLabel='Test'
            message='start timed test'
            instruction='Hi, click the button below to start your timed test, you have (8) minutes to answer (25) questions.'
            max={25}
            maxMessage='The maximum number of questions in a Test is [25]'
            min={3}
            startQuiz={this.props.startTestQuiz.bind(undefined)}
          />
          <Progress tabLabel='Progress' />
          <Notes tabLabel='Notes' startNotes={() => this.props.navigation.navigate('NotesScreen')} />
        </ScrollableTabView>
        {modal}
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  startPracticeQuiz (questions) {
    dispatch(Actions.startQuizAction(QuizFormats.PRACTICE, questions))
    ownProps.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'QuizScreen', params: {} })
      ],
    }))
  },
  startTestQuiz (questions) {
    dispatch(Actions.startQuizAction(QuizFormats.TEST, questions))
    ownProps.navigation.dispatch(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'QuizScreen', params: {} })
      ],
    }))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(LearnScreen)
