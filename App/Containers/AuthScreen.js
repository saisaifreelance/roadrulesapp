import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Keyboard, KeyboardAvoidingView, View, NativeEventEmitter, NativeModules, Modal, TouchableOpacity, Text, BackHandler, Alert, Platform } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { createStructuredSelector } from 'reselect'
// local components
import Form from '../Components/Form'
import Button from '../Components/Button'
import ProgressModal from '../Components/ProgressModal'
import { ModalNavigator } from '../Components/ModalConnect'
import HeaderLogo from '../Components/HeaderLogo'
import KeyboardDismiss from '../Components/KeyboardDismiss'
// Actions
import Actions, { makeSelectProfile } from '../Redux/ProfileRedux'
import { NAV_TEST } from '../Redux/NavigationRedux'
import { fields as PhoneNumberLoginFields } from '../Schema/PhoneNumberLogin'
import { fields as PhoneNumberVerify } from '../Schema/PhoneNumberVerify'
import ProfileReducer from '../Types/ProfileReducer'
// Styles
import styles from './Styles/AuthScreenStyle'
import { Images, Metrics, Colors } from '../Themes'

const EventEmitter = new NativeEventEmitter(NativeModules.FirebasePhoneAuth)
const {FirebasePhoneAuth} = NativeModules

class AuthScreen extends Component {
  static propTypes = {
    profile: ProfileReducer.isRequired,
    changeField: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    verify: PropTypes.func.isRequired,
    clearVerificationId: PropTypes.func.isRequired,
    clearError: PropTypes.func.isRequired,
    onVerificationCompleted: PropTypes.func.isRequired,
    onCodeAutoRetrievalTimeout: PropTypes.func.isRequired,
  }

  modalAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ModalConnect',
    params: {
      title: 'Request Assistance',
      description: 'Request Assistance from a Road Rules App representative via Phone call, WhatsApp or Facebook Messenger.',
      message: 'Hi, I\'m a Road Rules App user, requesting assistance in the login process',
      contact: {
        name: 'Road Rules App',
        phone: {
          office: {
            label: 'office',
            value: '+263718384668'
          }
        },
        whatsapp: {
          office: {
            label: 'office',
            value: '+263718384668'
          }
        },
        messenger: {
          office: {
            label: 'office',
            value: 'RoadRulesApp'
          }
        }
      },
      close: this.closeModal.bind(this)
    }
  }

  homeAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'HomeScreen',
    params: {}
  }

  profileAction = {
    type: NavigationActions.NAVIGATE,
    routeName: 'ProfileScreen',
    params: {}
  }

  constructor (props) {
    super(props)
    this.changeField = this.changeField.bind(this)
    this.modalRef = this.modalRef.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.backAction = this.backAction.bind(this)
    this.state = {
      modal: false
    }
  }

  changeField (field, value) {
    this.props.changeField({[field]: value})
  }

  modalRef (modal) {
    if (!this.modal && this.state.modal) {
      const {state: {nav: {routes, index}}} = modal
      const {key, routeName} = routes[index]
      if (routeName === 'ModalConnect') {
        modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
    this.modal = modal
  }

  openModal () {
    Keyboard.dismiss()
    this.setState({modal: true})
    if (this.modal) {
      const {state: {nav: {routes, index}}} = this.modal
      const {key, routeName} = routes[index]
      if (routeName === 'ModalConnect') {
        this.modal.dispatch(NavigationActions.setParams({
          params: this.modalAction.params,
          key
        }))
      }
    }
  }

  closeModal () {
    this.setState({modal: null})
  }

  componentDidMount () {
    const {isAuthenticated, hasProfile} = this.props.profile
    const redirect = isAuthenticated
    if (redirect) {
      if (hasProfile) {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.homeAction
          ]
        }))
      } else {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.profileAction
          ]
        }))
      }
      return
    }
    EventEmitter.addListener(FirebasePhoneAuth.ON_VERIFICATION_COMPLETED, this.props.onVerificationCompleted)
    EventEmitter.addListener(FirebasePhoneAuth.ON_CODE_AUTO_RETRIEVAL_TIMEOUT, this.props.onCodeAutoRetrievalTimeout)
    if (Platform.OS !== 'ios') {
      BackHandler.addEventListener('hardwareBackPress', this.backAction)
    }
  }

  componentDidUpdate (prevProps) {
    const {isAuthenticated, hasProfile} = this.props.profile
    const {isAuthenticated: prevIsAuthenticated} = prevProps.profile

    const redirect = isAuthenticated && !prevIsAuthenticated
    if (redirect) {
      if (hasProfile) {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.homeAction
          ]
        }))
      } else {
        this.props.navigation.dispatch(NavigationActions.reset({
          index: 0,
          actions: [
            this.profileAction
          ]
        }))
      }
    }
  }

  componentWillUnmount () {
    if (EventEmitter.removeAllListeners) EventEmitter.removeAllListeners(FirebasePhoneAuth.ON_VERIFICATION_COMPLETED)
    if (EventEmitter.removeAllListeners) EventEmitter.removeAllListeners(FirebasePhoneAuth.ON_CODE_AUTO_RETRIEVAL_TIMEOUT)
    if (Platform.OS !== 'ios') {
      BackHandler.removeEventListener('hardwareBackPress', this.backAction)
    }
  }

  backAction () {
    Alert.alert(
      'Exit App',
      'Are you sure you want to exit the Road Rules App before logging in? You will be asked to do so next time.',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'Exit', onPress: () => { BackHandler.exitApp() }},
      ],
      { cancelable: false }
    )
    return true
  }

  render () {
    let form = null
    let headerLogo = null
    let legend = null
    let modal = null
    let error = null

    if (this.state.modal) {
      modal = (
        <Modal
          animationType={'slide'}
          transparent
          visible
          onRequestClose={this.closeModal}>
          <ModalNavigator ref={this.modalRef}/>
        </Modal>
      )
    }

    if (this.props.profile.verificationId) {
      if (!this.props.profile.error) {
        headerLogo = (<HeaderLogo />)
        legend = 'Please enter the verification code you received via SMS below and click Verify.'
        form = (
          <View>
            <Form
              fields={PhoneNumberVerify}
              data={this.props.profile.form}
              error={this.props.profile.error}
              clearError={this.props.clearError}
              submitText='Verify'
              onChangeField={this.changeField}
              onSubmit={(...args) => {
                Keyboard.dismiss()
                this.props.verify(...args)
              }}
              legend={legend}
            />
            <Button
              style={{marginHorizontal: Metrics.baseMargin, marginBottom: Metrics.baseMargin}}
              onPress={() => {
                Keyboard.dismiss()
                this.props.clearVerificationId()
              }}>Resend code</Button>
            <Button
              style={{marginHorizontal: Metrics.baseMargin, marginBottom: Metrics.baseMargin}}
              onPress={this.openModal}>Request assistance</Button>
          </View>
        )
      } else {
        headerLogo = (<HeaderLogo />)
        error = (
          <TouchableOpacity style={styles.formErrorContainer} onPress={() => { this.props.clearError() }}>
            <Text style={styles.formError}>{this.props.profile.error}</Text>
          </TouchableOpacity>
        )
        form = (
          <View>
            <Form
              fields={PhoneNumberVerify}
              data={this.props.profile.form}
              error={null}
              clearError={this.props.clearError}
              submitText='Verify'
              onChangeField={this.changeField}
              onSubmit={(...args) => {
                Keyboard.dismiss()
                this.props.verify(...args)
              }}
              legend={legend}
            />
            <Button
              style={{marginHorizontal: Metrics.baseMargin, marginBottom: Metrics.baseMargin}}
              onPress={() => {
                Keyboard.dismiss()
                this.props.clearVerificationId()
              }}>Resend code</Button>
            <Button
              style={{marginHorizontal: Metrics.baseMargin, marginBottom: Metrics.baseMargin}}
              onPress={this.openModal}>Request assistance</Button>
          </View>
        )
      }
    }

    if (!this.props.profile.verificationId) {
      headerLogo = (<HeaderLogo />)
      legend = 'Please enter your current phone number below and click Send Verification Code, a verification code will be sent to your phone.'
      form = (
        <Form
          fields={PhoneNumberLoginFields}
          data={this.props.profile.form}
          error={this.props.profile.error}
          clearError={this.props.clearError}
          submitText='Send Verification Code'
          onChangeField={this.changeField}
          onSubmit={(...args) => {
            Keyboard.dismiss()
            this.props.login(...args)
          }}
          legend={legend}
        />
      )
    }
    const isFetching = this.props.profile.isFetching ? (<ProgressModal message={this.props.profile.isFetching}/>) : null
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          contentContainerStyle={{flex: 1}}
          behavior='position'
          keyboardVerticalOffset={-300}>
          {headerLogo}
          {error}
          <KeyboardDismiss>
            {form}
          </KeyboardDismiss>
        </KeyboardAvoidingView>
        {isFetching}
        {modal}
      </View>
    )
  }
}

const mapStateToProps = createStructuredSelector({
  profile: makeSelectProfile()
})

const mapDispatchToProps = (dispatch) => {
  return {
    changeField: (fields) => dispatch(Actions.fieldChangeAction(fields)),
    login: (fields, form) => dispatch(Actions.loginRequestAction(fields, form)),
    // login: () => dispatch(Actions.signInAnonymouslyAction()),
    // login: () => dispatch({type: NAV_TEST}),
    logout: () => dispatch(Actions.logoutRequestAction()),
    verify: (fields, form) => dispatch(Actions.verifyRequestAction(fields, form)),
    clearVerificationId: () => dispatch(Actions.clearVerificationIdAction()),
    clearError: () => dispatch(Actions.clearErrorAction()),
    onVerificationCompleted: (verificationCode) => dispatch(Actions.onVerificationCompletedAction(verificationCode)),
    onCodeAutoRetrievalTimeout: (verificationId) => dispatch(Actions.onCodeAutoRetrievalTimeoutAction(verificationId))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen)
